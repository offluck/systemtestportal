# Systemtestportal

[pipeline-badge]: https://gitlab.com/offluck/systemtestportal/badges/main/pipeline.svg
[coverage-badge]: https://codecov.io/gl/offluck/systemtestportal/badge.svg
[coverage-link]: https://codecov.io/gl/offluck/systemtestportal/badge.svg

![PipeLine][pipeline-badge]

[![Coverage][coverage-badge]][coverage-link]

## Prerequisites

If not started via Docker-Compose, a database named `systemtestportal_db` has to be manually created in the user's local DBMS specified in the `.env` file of the `stp-backend`-folder. Otherwise, the application will crash with an error message on startup.

## Documentation
General documentation can be found in `documentation`

Develoment, backend-specific and frontend-specific documentation can be found in `stp-backend/DEVELOP.md`, `stp-backend/README.md` and `stp-frontend/README.md`

## Running the project via docker-compose

```shell
docker-compose up --build -d
```
