# PipeLine

GitLab CI/CD pipeline consists of 5 main parts:
- `build`:
  - `debug`: debug build of backend with `cargo build --verbose --verbose`
  - `release`: release build of backend `cargo build --release --verbose --verbose`
- `test`:
  - `coverage`: runs backend test coverage with `cargo tarpaulin --verbose --all-features --workspace --timeout 120 --out Xml`
  - `test`: runs backend tests with `cargo test --verbose --verbose`
- `codestyle`:
  - `check`: basic code check with `cargo check --verbose --verbose`
  - `formatting`: checks code formatting with `cargo fmt --all -- --check`
  - `warnings`: checks that project has no warnings with `cargo clippy -- -D warnings`
- `docker`:
  - `compose`: builds release docker images with `docker-compose -f docker/compose/ci.docker-compose.yml up --build -d`
- `external`: delivers coverage results to CodeCov
