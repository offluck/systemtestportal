# setup

[https://vuejs.org/guide/typescript/overview.html](https://vuejs.org/guide/typescript/overview.html)

## VS Code

### Vue Language Features (Volar)

[https://marketplace.visualstudio.com/items?itemName=Vue.volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

in VS Code:

- CTRL + P
- ext install Vue.volar
- Eingabe

### Typescript Vue Plugin (Volar)

[https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin)

nochmal dasselbe mit:

- ext install Vue.vscode-typescript-vue-plugin

### Volar Takeover Mode

normally we would have to typsecript language service instances running in VS Code, 1 from Volar and 1 from VSC.

To enable Takeover Mode, you need to disable VSCode's built-in TS language service in your project's workspace only by following these steps:

    In your project workspace, bring up the command palette with Ctrl + Shift + P (macOS: Cmd + Shift + P).
    Type built and select "Extensions: Show Built-in Extensions".
    Type typescript in the extension search box (do not remove @builtin prefix).
    Click the little gear icon of "TypeScript and JavaScript Language Features", and select "Disable (Workspace)".
    Reload the workspace. Takeover mode will be enabled when you open a Vue or TS file.

### Additional VSCode Extensions

Prettier - Code formatter

# Functionality

## Pages

### Login Page

The login page is accessible at `/login`. It features an email and password input field. Below the password input field,
there is a "Forgot Password" link that redirects to the `/forgot-password` page. Additionally, there are the "REGISTER" and "LOGIN" buttons.

#### Login Process

Upon entering credentials and clicking the "LOGIN" button (or pressing "Enter" in the password field),
the provided password will be hashed via SHA-256 and a salt.
Then you will either receive an info notification indicating that your credentials were correct and be redirected to the `/product` page,
or an error notification stating that your credentials were wrong. For security reasons, the specific incorrect input is not revealed.
The backend also returns your current user information, such as username, userid, biography, etc.,
which is stored in the frontend's authentication store along with an HTTP-only cookie containing a JWT token with a limited lifetime.

#### Logout Process

While logged in, a user menu icon appears in the toolbar. Clicking it reveals a logout button. Upon clicking the logout button, two actions occur:

1. You are logged out of the frontend, clearing your user information, and redirected to the `/login` page.
2. A call is made to the `/auth/logout` API endpoint in the backend, which returns an empty cookie.

### Register Page

The register page contains four input fields:

1. Name
2. Email
3. Password
4. Password (Again)

All fields must be correctly filled out before the "REGISTER" button can be pressed. To ensure this, specific rules apply:

- The "Name" field cannot be empty.
- The provided email must be valid (validated using RegEx).
- The email must not already be taken. The backend handles this validation, so attempting to register with a taken email triggers an error notification.
- The password must meet a minimum strength requirement of "Ok."
- The "Password" and "Password (Again)" fields must match.

#### Password Strengths

There are five levels of password strength:

- Short: password is shorter than 8 characters
- Weak: password only contains letters
- Common: password contains sequences such as "password" or "1234"
- Ok: password contains both letters and numbers
- Strong: password contains letters, numbers, and special characters

After providing valid input and clicking the "REGISTER" button, the password will be hashed and your data will be sent to the backend.
If successfull a screen appears informing you that an email has been sent to the provided email address for email verification.
There is also a "BACK TO LOGIN" button to redirect you pack the the `/login` page.

### Activation Page

Clicking the link in the sent email redirects you to the `/activate` page. The provided token query parameter is sent to the backend.
If the backend responds with an OK status code, the token is valid, and a success screen is displayed.
If the token has already been used or is invalid, a failure screen is shown. Both screens include a "BACK TO LOGIN" button, which redirects to the `/login` page.

### Account Settings Page

While logged in, you can access the account settings via the user menu. The account settings page displays five fields:

1. Full name
2. Email
3. Biography
4. Current password
5. New password
6. New password (again)

Currently, you can change your name, biography, and password. If you make any changes that you wish to save,
you must provide your current password (which will be hashed before being sent to the backend).
Failure to do so will prevent the changes from being saved, and an error notification will be displayed.
When changing your password, the same rules from the register page apply: a minimum strength of "Ok" and both password fields must match.

Upon providing correct input and clicking the "UPDATING ACCOUNT INFORMATION" button, a notification appears indicating the success of the update process.

### Forgot Password Page

The forgot password page can be accessed from the login page. If a user forgot their password they may request a reset link
with token. The validity of that token can be set in the .env file in the backend.

Please note that this requires a valid email config and the email config to be associated with the reset password mail template.

If the email is not known to the backend it will not send an email.

### Reset password page

The link to this page will be provided in the 'reset password mail'. The reset password page allows the user to set a new password.
The input fields are:

1. New password
2. New password again

The password must cohere to the rules written in the 'password strength' chapter. If the token is valid an the password abides
to the rules the new password is accepted. The user will be routed to the login page.

### Email Config Page

Users can configure SMTP for emails. All emails must use the TLS SMTP port (587).
Users enter following information:

1. Email Config Title
2. Email
3. Email App Password
4. SMTP Server Address

### Notes Email sending:

Note that depending on the SMTP provider, emails may be sent to spam. Google SMTP for example will test credibility of emails
by randomly marking them as spam. If those are retrieved the email will be more credible in the future. Other factors influence
the sending of emails as well. Most providers require an activation to use SMTP. The provider will then allow you to assign an
app password. Do not insert your personal password as app password.

Many providers have a fixed daily limit for emails sent via SMTP. If you require more emails to be sent you may have to pay
for the service.

### Email Template Page

Users can edit the email templates based on their needs. The placeholders in the squared brackets however must not be removed.
Following fields may be changed:

1. Email Template
2. Assigned Email Config

The assets folder in the backend also includes HTML templates which you may use if you want to. You can do this by changing the
migration script 'm20230101_000004_seed_data_email_templates' to use the .html files instead of the .txt ones.

### Test Case Page

This page has been appended to allow the user to subscribe to a test case. All subscribed users will be shown in a list.
Two buttons allow the user to subscribe and unsubscribe. This subscription can be seen by all users.

## Route Guarding

If you are not logged in on the frontend (meaning the "user" in the authentication store is "null") and attempt to access guarded routes,
you will be redirected back to the login page and see an error notification stating "You are currently not logged in."
On the JavaScript side, the `logoutUser()` function of the authentication store is called,
ensuring that the user information is cleared and an invalid token is requested from the backend.

Due to the HTTP-only JWT, the frontend cannot directly check the token's validity. This can lead to issues when the JWT expires after an hour.
To check if the session has expired, the backend returns an HTTP 401 status code.
In order to handle this without implementing an HTTP status check for 401 in every endpoint function, Axios provides interceptors.
These interceptors allow us to check if the backend returns a 401 code and handle it before reaching the function that made the request.

For error handling, the following steps are taken:

### Check the API Route

The `/auth/login` POST endpoint is allowed to return a 401 error when the provided credentials are invalid, so it can be handled by the login method.
The `/users` PUT endpoint is also permitted to return "Unauthorized" when attempting to change account information with an incorrect password.
For any other endpoint returning a 401 status code, the following actions are taken:

### Detect Error

The current user is cleared, and the "authenticationFail" flag of the authentication store is set to `true`.
Then, the user is redirected to the `/login` page and presented with a notification indicating that their session has expired.
Since we always save the last guarded route the user attempted to access, if the "authenticationFail" flag is set,
the user will be redirected to that page after logging in instead of being redirected to the `/products` page.
After the redirect, the "authenticationFail" flag is set back to false.

## Testing

### Unit Tests with Jest

Jest is a JavaScript testing framework commonly used to test applications built with frameworks like React, Vue, and Angular.
It provides a simple and intuitive way to write and execute tests, offering features such as test assertions, mocking, and code coverage analysis.

In our case, we primarily used Jest to write unit tests for utility functions and to check Quasar form-validation rules.
These tests can be found under `/stp-frontend/tests` and run with `yarn run test`.

### End-to-End Tests with Cypress

Cypress is an end-to-end testing framework for web applications.
It allows you to write tests that simulate user interactions and behavior,
providing a reliable way to test the functionality and performance of web applications.
Cypress offers features such as automatic waiting, real-time reloading, and a GUI for writing and debugging tests.

With Cypress, we tested the functionality of the `/login`, `/register`, `/activate`, and `/account` pages.
The tests themselves can be found under `/stp-frontend/cypress/e2e` and run with `yarn run cypress open`.
