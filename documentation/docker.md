# Images

- `docker-compose.yaml` - main compose, contains release images of backend, frontend and db
- `ci.docker-compose.yaml` - CI compose, contains release images of backend, frontend and db, but with different syntax to be run in GitLab CI/CD
- `db.docker-compose.yaml`- DB compose, use it to run release version of application with locally launched frontend and backend
- `dev/docker-compose.yaml` - development compose, quite similar to previous

The build of `docker-compose.yaml` and `ci.docker-compose.yaml` can take quite a long time!
