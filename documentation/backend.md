# Rust configuration

Install rust for your system as stated on the official website:

https://www.rust-lang.org/tools/install

# Rust Language Server

Install `rust-analyzer`, e. g. as extension in Visual Studio Code

# Rust Clippy

Run `cargo clippy` periodically to get recommendations for code improvements.
Depending on your IDE, it can also be configured to run automatically on every file save.

# Rust Format

Run `cargo fmt` to format the project in a coherent style.

# Extend the project

Install `sea-orm cli` with cargo:

```bash
cargo install sea-orm-cli
```

## Create a new migration

To create a new migration, copy and change one of the files in the migration directory.

Register it afterwards in the mod.rs file in the migration directory.

Then run `cargo run` to execute the new migration.

## Generate corresponding entity

To generate an entity file from the newly database table and to update the existing entities run:

```bash
sea-orm-cli generate entity --date-time-crate time -o src/entity/
```

Be careful! This replaces all existing entity files!
Therefore, any manual changes to the entity files will be lost whenever the entities and their relations are generated with the CLI command.
A good practice is to not edit anything in the entity folder manually.
Instead, write and execute migrations and regenerate the entities afterwards with the CLI command.

`--date-time-crate time` generates entity files that use the `time` crate for date/time data instead of the default `chrono`

## Create DTO (data transfer object) for the new entity

A dto is a minimized copy of the entity that only contains the data that enters or leaves the API.

It should be created in the dto directory and registered in its `mod.rs` file.

## Create Service

All the database operations happen in the services.
Functions to access the database should be created there.
The new service has to be registered in the `mod.rs` of the service directory.

## Create Route

All route handlers that call the service functions should be placed in the route folder.

## Register Route

Don't forget to register the route handler in the `mod.rs` file of the route directory.


# Cache

In `cache.rs` and in `main.rs` you can see the implementation and usage of cache. It mainly is used to store temporary users' information such as number of failed attempts to login

It is thread-safe due to mutexes

It is running in a separate coroutine (almost always even in a separate thread)
