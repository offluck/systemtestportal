# Start scripts for development

## On Windows

You need to have npm, docker and cargo installed.  
To execute the script just parse `.\dev\start-services.ps1` in the powershell window.
(Or call the powershell script form a different path).  
If facing issues calling the script, you probably haven' allowed script execution.  
For this open powershell as admin and run `Set-ExecutionPolicy RemoteSigned`.

## On Linux and Mac

You need to have tmux, yarn, docker and cargo installed.  
To run the startup-script run `dev/start-services.sh` from the project root.  
To switch between windows you need to press the prefix (`ctrl+b`) and `n` to go to
the next window or `p` to see the previous one.
