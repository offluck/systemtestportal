#!/bin/bash

set -e
SESSION_NAME="stp-dev"

# requires installation of tmux, yarn, cargo and docker

tmux new-session -d -s $SESSION_NAME -n "default" 
tmux new-window -t $SESSION_NAME -n "stp-db" "cd dev && docker-compose up"
tmux new-window -t $SESSION_NAME -n "stp-backend" "cd stp-backend && cargo run"
tmux new-window -t $SESSION_NAME -n "stp-frontend" "cd stp-frontend && yarn install && yarn run dev"

tmux attach-session -t $SESSION_NAME
