# start-script:
Start-Process -FilePath docker-compose up -WorkingDirectory .\dev -PassThru 

Start-Process -FilePath cargo run -WorkingDirectory .\stp-backend -PassThru
# it's just easier to use npm instead of yarn on windows
Start-Process -FilePath cmd -WorkingDirectory .\stp-frontend -ArgumentList "/c yarn install && yarn run dev" -PassThru
