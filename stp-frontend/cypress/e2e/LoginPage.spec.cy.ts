/* eslint-disable @typescript-eslint/no-explicit-any */
// used to supress errors by typsecript for assuming wrong type of credentials
describe('Login page test', () => {
    // Requires Testuser to be added, see testuser.json
    beforeEach(() => cy.fixture('testuser.json').as('userCredentials'));

    const modifier = 'falskjföasljfd';

    it('should not login if the input is invalid', () => {
        cy.visit('/login');

        cy.get('input[name="email-input"]').type(modifier);
        cy.get('input[name="password-input"]').type(modifier);

        cy.get('button[name="login-button"]').click();
        cy.wait(1000);

        cy.contains('Wrong email or password.').should('be.visible');
        cy.url().should('include', 'login');
    });

    it('should login', () => {
        cy.visit('/login');
        cy.get('@userCredentials').then((credentials: any) => {
            cy.get('input[name="email-input"]').type(credentials.email);
            cy.get('input[name="password-input"]').type(credentials.password);
        });

        cy.get('button[name="login-button"]').click();
        cy.wait(1000);

        cy.contains('Login successfull.').should('be.visible');
        cy.url().should('include', 'products');
        cy.getCookie('token').should('exist');
    });

    it('should logout', () => {
        cy.login();

        cy.get('button[name="account-menu"]').click();
        cy.get('#logout-item').click();
        cy.url().should('include', 'login');
    });

    // test with caution because it blocks your ip for 24 h or until you restart the backend

    it('should block the user', () => {
        const numberOfTries = 10;
        cy.visit('/login');

        cy.get('@userCredentials').then((credentials: any) => {
            const emailInput = cy.get('input[name="email-input"]');
            const passwordInput = cy.get('input[name="password-input"]');

            emailInput.type(credentials.email + modifier);
            passwordInput.type(credentials.password + modifier);

            for (let i = 0; i < numberOfTries; i++) {
                cy.get('button[name="login-button"]').click();
                cy.wait(300);
            }

            emailInput.clear();
            passwordInput.clear();
            cy.wait(4000);

            emailInput.type(credentials.email);
            passwordInput.type(credentials.password);
        });

        cy.contains('Wrong email or password.').should('not.exist');
        cy.contains('Login successfull.').should('not.exist');
        cy.url().should('include', 'login');
    });
});
