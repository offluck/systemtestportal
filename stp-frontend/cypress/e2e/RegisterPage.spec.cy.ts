import { checkPasswordStrenght } from '../support/utils';

describe('Registration Page test', () => {
    const name = 'Test Name';
    const email = 'testmail@hallo.com';
    const password = 'V3Ry_S3cRet-P4ssW0rd!';
    const modifier = 'falskjföasljfd';

    beforeEach(() => {
        cy.visit('/');
        cy.get('button[name="register-button"]').click();
        cy.url().should('include', 'register');
    });

    it('should be able to detect a wrong email input', () => {
        const emailInput = cy.get('input[name="email-input"]');
        const invalidEmails = [
            'invalid',
            'invalid@',
            '@example.com',
            'invalid@example',
        ];

        invalidEmails.forEach((email) => {
            emailInput.type(email);
            emailInput.blur();
            cy.contains('Please give a valid email.').should('be.visible');
            emailInput.clear();
        });
    });

    it('should be able to detect non matching password inputs', () => {
        cy.get('input[name="password-input"]').type(password);
        cy.get('input[name="password-check-input"]')
            .type(password + modifier)
            .blur();
        cy.contains("Passwords don't match.").should('be.visible');
    });

    it('should be able to detect different password strength', () => {
        const passwordFieldName = 'password-input';
        checkPasswordStrenght(passwordFieldName);
    });

    // requires
    it('should be detect taken mail', () => {
        cy.fixture('testuser.json').as('userCredentials');
        cy.get('@userCredentials').then((credentials: any) => {
          cy.get('input[name="email-input"]').type(credentials.email);
        });

        cy.get('input[name="name-input"]').type(name);
        cy.get('input[name="password-input"]').type(password);
        cy.get('input[name="password-check-input"]').type(password);
        cy.contains('Password strength: Strong').should('be.visible');

        cy.get('button[name="register-button"]').click();

        cy.contains('Given mail is not available').should('be.visible');
        cy.url().should('include', 'register');
    });

    // can currently only run once before deleting the user or comment out the put call in RegisterPage.vue
    it('should be able to register a new user', () => {
        cy.get('input[name="name-input"]').type(name);
        cy.get('input[name="email-input"]').type(email);

        cy.get('input[name="password-input"]').type(password);
        cy.get('input[name="password-check-input"]').type(password);
        cy.contains('Password strength: Strong').should('be.visible');

        cy.get('button[name="register-button"]').click();
        cy.wait(1000)

        cy.contains('Registered Successfully').should('be.visible');
        cy.contains('An email has been sent to ').should('be.visible');
        cy.contains(email).should('be.visible');
        cy.get('#login-page-button').click();
        cy.url().should('include', 'login');
    });
});
