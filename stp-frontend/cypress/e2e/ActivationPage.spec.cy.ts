describe('Activate Account Page Test', () => {
    const base_path = '/activate';
    const tokenUrl = (token: string) => `${base_path}?token=${token}`;

    afterEach(() => {
      cy.get('#login-page-button').click();
      cy.url().should('include', 'login');
    })

    it('Should detect no token', () => {
        cy.visit(base_path);
        cy.url().should('include', base_path);

        cy.contains('Invalid token').should('be.visible');
    });

    it('Should detect invalid token', () => {
        cy.visit(tokenUrl('fjklösdajlkfdaslkfd'));
        cy.url().should('include', base_path);

        cy.contains('Invalid token').should('be.visible');
    });
});
