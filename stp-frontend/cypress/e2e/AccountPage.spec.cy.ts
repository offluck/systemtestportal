import { checkPasswordStrenght } from '../support/utils';
import testuser from '../fixtures/testuser.json';

describe('Account Page Test', () => {
    const password = testuser.password;
    const modifier = 'fjösaldkfj';

    beforeEach(() => {
      // Requires Testuser to be added, see testuser.json
      cy.login();
      goToAccountPage();
    });

    function goToAccountPage() {
        cy.get('button[name="account-menu"]').click();
        cy.get('#account-settings-item').click();
        cy.url().should('include', 'account');
    }

    function resetPasswordToOriginal() {
        goToAccountPage();
        cy.get('input[name="current-password-input"]').type(password);
        cy.get('input[name="new-password-input"]').type(password);
    }

    it('Should access the account page successfully', () => {
        cy.fixture('testuser.json').as('userCredentials');
        cy.get('@userCredentials').then((credentials: any) => {
            cy.get('input[name="full-name-input"]').should(
                'have.value',
                credentials.name
            );
            cy.get('input[name="email-input"]').should(
                'have.value',
                credentials.email
            );
        });
    });

    it('should detect password strength', () => {
        const passwordFieldName = 'new-password-input';
        checkPasswordStrenght(passwordFieldName);
    });

    it('should detect non matching passwords', () => {
        cy.get('input[name="new-password-input"]').type(password);
        cy.get('input[name="new-password-check-input"]')
            .type(password + modifier)
            .blur();
        cy.contains("Passwords don't match.").should('be.visible');
    });

    it('should detect false current password', () => {
        cy.get('input[name="current-password-input"]').type(password+modifier);
        cy.get('input[name="new-password-input"]').type(password);
        cy.get('input[name="new-password-check-input"]').type(password);

        cy.get('button[name="update-account-button"]').click();
        cy.contains('Wrong password given.').should('be.visible');
    });


    it('should update the user successfully', () => {
        cy.get('input[name="current-password-input"]').type(password);
        cy.get('input[name="new-password-input"]').type(password + modifier);
        cy.get('input[name="new-password-check-input"]').type(
            password + modifier
        );

        cy.get('button[name="update-account-button"]').click();
        cy.contains('Updated account informations successfully').should(
            'be.visible'
        );

        resetPasswordToOriginal();
    });
});
