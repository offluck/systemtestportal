export function checkPasswordStrenght(inputFieldName: string) {
    const passwordShort = 'short';
    const passwordWeak = 'zyxwvutsr';
    const passwordCommon = 'password';
    const passwordOk = 'VerySecretPassword123';
    const passwordStrong = 'V3Ry_S3cRet-P4ssW0rd!';

    const passwordField = cy.get(`input[name="${inputFieldName}"]`);

    passwordField.type(passwordShort);
    cy.contains('Password strength: Short').should('be.visible');
    passwordField.clear();

    passwordField.type(passwordWeak);
    cy.contains('Password strength: Weak').should('be.visible');
    passwordField.clear();

    passwordField.type(passwordCommon);
    cy.contains('Password strength: Common').should('be.visible');
    passwordField.clear();

    passwordField.type(passwordOk);
    cy.contains('Password strength: Ok').should('be.visible');
    passwordField.clear();

    passwordField.type(passwordStrong);
    cy.contains('Password strength: Strong').should('be.visible');
    passwordField.clear();
}
