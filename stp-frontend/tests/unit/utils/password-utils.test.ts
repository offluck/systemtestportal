import 'node:crypto';
import {
    PasswordCheckStrength,
    checkPasswordStrength,
    getPasswordValidatorStyle,
    hashPassword,
} from 'src/utils/password-utils';

describe('hashPassword Test', () => {
    test('should return the hashed password', async () => {
        // Fixes for different enviroment in testing
        process.env.SALT = 'somesaltvalue';
        const crypto = require('crypto');
        global.crypto = crypto;

        const password = 'password123';
        // Hashed string created with https://emn178.github.io/online-tools/sha256.html last accessed on 26.06.2023
        const expectedHash =
            '2debd7e3c43b9dc612c0fd295cfba5d70bcbb6753258e1e2a5748fbe395545f8';

        const hashedPassword = await hashPassword(password);

        expect(hashedPassword).toBe(expectedHash);
    });
});

describe('checkPasswordStrength', () => {
    test('should return PasswordCheckStrength.Short for empty password', () => {
        const password = '';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Short);
    });

    test('should return PasswordCheckStrength.Short for password length less than minimum', () => {
        const password = 'pass';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Short);
    });

    test('should return PasswordCheckStrength.Common for common password', () => {
        const password = 'password123';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Common);
    });

    test('should return PasswordCheckStrength.Weak for password without numbers or special chars', () => {
        const password = 'abcdefgh';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Weak);
    });

    test('should return PasswordCheckStrength.Ok for password with 4 elements', () => {
        const password = 'Abcd1234';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Ok);
    });

    test('should return PasswordCheckStrength.Strong for password with more than 4 elements', () => {
        const password = 'Abcd1234$';
        const result = checkPasswordStrength(password);
        expect(result).toBe(PasswordCheckStrength.Strong);
    });
});

describe('getPasswordValidatorStyle', () => {
    test('should return the correct style for PasswordCheckStrength.Short and PasswordCheckStrength.Weak', () => {
        const style = getPasswordValidatorStyle(PasswordCheckStrength.Short);
        expect(style).toBe('font-weight: 600; color: darkred');

        const style2 = getPasswordValidatorStyle(PasswordCheckStrength.Weak);
        expect(style2).toBe('font-weight: 600; color: darkred');
    });

    test('should return the correct style for PasswordCheckStrength.Common', () => {
        const style = getPasswordValidatorStyle(PasswordCheckStrength.Common);
        expect(style).toBe('font-weight: 600; color: red');
    });

    test('should return the correct style for PasswordCheckStrength.Ok', () => {
        const style = getPasswordValidatorStyle(PasswordCheckStrength.Ok);
        expect(style).toBe('font-weight: 600; color: lightgreen');
    });

    test('should return the correct style for PasswordCheckStrength.Strong', () => {
        const style = getPasswordValidatorStyle(PasswordCheckStrength.Strong);
        expect(style).toBe('font-weight: 600; color: darkgreen');
    });
});
