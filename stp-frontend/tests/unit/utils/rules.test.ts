import {
    ruleIsEmail,
    ruleMatchingPasswords,
    ruleNotEmptyInput,
    rulePasswordStrength,
} from 'src/utils/rules';

describe('ruleNotEmptyInput', () => {
    test('should return true for a non-empty input', () => {
        const input = 'Hello, World!';
        const rule = ruleNotEmptyInput();
        const result = rule(input);
        expect(result).toBe(true);
    });

    test('should return "Please type something" for an empty input', () => {
        const input = '';
        const rule = ruleNotEmptyInput();
        const result = rule(input);
        expect(result).toBe('Please type something');
    });
});

describe('rulePasswordStrength', () => {
    test('should return true for a password with Ok or Strong strength', () => {
        const password = 'P@ssw0rd';
        const result = rulePasswordStrength(password);
        expect(result).toBe(true);
    });

    test('should return false for a password with Short, Common, or Weak strength', () => {
        const password = 'weakpassword';
        const result = rulePasswordStrength(password);
        expect(result).toBe(false);
    });
});

describe('ruleMatchingPasswords', () => {
    test('should return true if passwords match', () => {
        const password1 = 'password123';
        const password2 = 'password123';
        const result = ruleMatchingPasswords(password1, password2);
        expect(result).toBe(true);
    });

    test('should return false if passwords do not match', () => {
        const password1 = 'password123';
        const password2 = 'differentpassword';
        const result = ruleMatchingPasswords(password1, password2);
        expect(result).toBe(false);
    });
});

describe('ruleIsEmail', () => {
    const validEmails = [
        'example@example.com',
        'test.user@example.co.uk',
        'john.doe123@example.net',
    ];

    const invalidEmails = [
        'invalid',
        'invalid@',
        '@example.com',
        'invalid@example',
    ];

    const validationRule = ruleIsEmail();

    test('should return true for valid emails', () => {
        validEmails.forEach((email) => {
            expect(validationRule(email)).toBe(true);
        });
    });

    test('should return the error message for invalid emails', () => {
        invalidEmails.forEach((email) => {
            expect(validationRule(email)).toBe('Please give a valid email.');
        });
    });
});
