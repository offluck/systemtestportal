import { boot } from 'quasar/wrappers';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';

declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $timeago: TimeAgo;
    }
}

TimeAgo.addDefaultLocale(en);
const timeAgo = new TimeAgo(en.locale);

export default boot(({ app }) => {
    // for use inside Vue files (Options API) through this.$timeago
    app.config.globalProperties.$timeago = timeAgo;
});

export { timeAgo };
