import { boot } from 'quasar/wrappers';
import axios, {
    AxiosError,
    AxiosInstance,
    AxiosRequestConfig,
    AxiosResponse,
} from 'axios';
import { Router } from 'vue-router';
import { useAuthenticationStore } from 'src/stores/authentication-store';
import { RouteParts } from 'src/router/routes';
import { ApiRoute } from 'src/utils/store-utils';
import { notifyError } from 'src/utils/general-utils';


declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $axios: AxiosInstance;
    }
}

// as a default axios requests have no timeout enabled,
// so we absolutely have to define a timeout, to prevent infinite await
const requestTimeout = 5000;
const customAxiosConfig: AxiosRequestConfig = {
    timeout: requestTimeout,
    withCredentials: true,
};

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const HOST = process.env.HOST ?? 'localhost';
const HOST_PORT = process.env.HOST_PORT ?? 8080;
const serverAddress = `http://${HOST}:${HOST_PORT}`;
if (process.env.DEV) {
    console.log('Server address:', serverAddress);
}
const api = axios.create({ ...customAxiosConfig, baseURL: serverAddress });

// configure global axios
axios.defaults.timeout = requestTimeout;

export default boot(({ app, router }) => {
    // for use inside Vue files (Options API) through this.$axios and this.$api
    setAxiosInterceptors(router, api);

    app.config.globalProperties.$axios = axios;
    // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
    //       so you won't necessarily have to import axios in each vue file

    app.config.globalProperties.$api = api;
    // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
    //       so you can easily perform requests against your app's API
});

function setAxiosInterceptors(router: Router, api: AxiosInstance) {
    api.interceptors.response.use(
        (response) => {
            return response;
        },
        async (error) => {
            try {
                if (error.code == AxiosError.ECONNABORTED) {
                    notifyError('Timed out while trying to communicating with the backend.');
                } else {
                    const response = error.response;
                    authenticationInterceptor(response, router);
                    authorizationInterceptor(response, router);
                }

                return Promise.reject(error);
            } catch (error: unknown) {
                console.error('unkown interceptor error', error);

                throw error;
            }
        }
    );
}

function authenticationInterceptor(response: AxiosResponse, router: Router) {
    if (
        response.status === 401 &&
        !response.request.responseURL.includes(ApiRoute.Login) &&
        isNotUpdateUserEndPoint(response)
    ) {
        const authStore = useAuthenticationStore();
        authStore.$patch({ authenticationFail: true, user: null });
        notifyError('Your session expired.')
        router.push(RouteParts.Login);
    }
}

function authorizationInterceptor(response: AxiosResponse, router: Router) {
    if (response.status === 403) {
        router.push(RouteParts.Products);
    }
}

function isNotUpdateUserEndPoint(response: AxiosResponse): boolean {
    return !(
        response.request.responseURL.includes(ApiRoute.AppUsers) &&
        response.config.method === 'put'
    );
}

export { api, customAxiosConfig };
