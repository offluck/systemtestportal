import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { TimeStampUTC } from 'src/utils/date-utils';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface TestCaseExecution {
    id: number;
    preconditionFulfilled: boolean;
    createdAt: TimeStampUTC;
    updatedAt: TimeStampUTC;
    testCaseId: number;
    testSequenceExecutionId: number;
    executionStatusId: number;
}

export type TestCaseExecutionPostDTO = Pick<
    TestCaseExecution,
    | 'preconditionFulfilled'
    | 'testCaseId'
    | 'testSequenceExecutionId'
    | 'executionStatusId'
>;
export type TestCaseExecutionPutDTO = TestCaseExecutionPostDTO;
export type TestCaseExecutionMutateDTO =
    | TestCaseExecutionPostDTO
    | TestCaseExecutionPutDTO;

interface TestCaseExecutionStoreState {
    testCaseExecutions: TestCaseExecution[];
    singleTestCaseExecution: TestCaseExecution | null;
}

type QueryParams = {
    test_case_id?: number;
    test_sequence_execution_id?: number;
    execution_status_id?: number;
};

type GetAllQueryParams = QueryParams &
    Required<Pick<QueryParams, 'test_sequence_execution_id'>>;

export const useTestCaseExecutionStore = defineStore('test-case-execution', {
    persist: true,
    state: (): TestCaseExecutionStoreState => ({
        testCaseExecutions: [],
        singleTestCaseExecution: null,
    }),
    actions: {
        async getAll(
            queryParams: GetAllQueryParams
        ): Promise<TestCaseExecution[]> {
            try {
                const apiResponse = await api.get(ApiRoute.TestCaseExecutions, {
                    params: queryParams,
                });
                this.testCaseExecutions = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading test case executions!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(
            testCaseExecutionId: number
        ): Promise<TestCaseExecution> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.TestCaseExecutions}/${testCaseExecutionId}`
                );
                this.singleTestCaseExecution = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single test case execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(
            testCaseExecution: TestCaseExecutionPostDTO
        ): Promise<TestCaseExecution> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.TestCaseExecutions,
                    testCaseExecution
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new test case execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            testCaseExecution: TestCaseExecutionPutDTO,
            testCaseExecutionId: number
        ): Promise<void> {
            try {
                await api.put(
                    `${ApiRoute.TestCaseExecutions}/${testCaseExecutionId}`,
                    testCaseExecution
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating test case execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(testCaseExecutionId: number): Promise<void> {
            try {
                await api.delete(
                    `${ApiRoute.TestCaseExecutions}/${testCaseExecutionId}`
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting test case execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetTestCaseExecutions(): void {
            this.testCaseExecutions = [];
        },
        resetSingleTestCaseExecution(): void {
            this.singleTestCaseExecution = null;
        },
    },
});
