import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import {
    ApiRoute,
    convertToApiError,
    normalizeEmptyStringToNull,
} from '../utils/store-utils';

export interface TestSequence {
    id: number;
    name: string;
    description: string | null;
    precondition: string | null;
    createdAt: string;
    productVariantId: number;
}

export type TestSequencePostDTO = Pick<
    TestSequence,
    'name' | 'description' | 'precondition' | 'productVariantId'
>;
export type TestSequencePutDTO = TestSequencePostDTO;
export type TestSequenceMutateDTO = TestSequencePostDTO | TestSequencePutDTO;

interface TestSequenceStoreState {
    testSequences: TestSequence[];
    singleTestSequence: TestSequence | null;
}

export const useTestSequenceStore = defineStore('test-sequence', {
    persist: true,
    state: (): TestSequenceStoreState => ({
        testSequences: [],
        singleTestSequence: null,
    }),
    actions: {
        async getAll(productVariantId: number): Promise<TestSequence[]> {
            try {
                const apiResponse = await api.get(ApiRoute.TestSequences, {
                    params: { product_variant_id: productVariantId },
                });
                this.testSequences = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading test sequences!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(testSequenceId: number): Promise<TestSequence> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.TestSequences}/${testSequenceId}`
                );
                this.singleTestSequence = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single test sequence!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(testSequence: TestSequencePostDTO): Promise<TestSequence> {
            try {
                normalizeMutateDto(testSequence);
                const apiResponse = await api.post(
                    ApiRoute.TestSequences,
                    testSequence
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new test sequence!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            testSequence: TestSequencePutDTO,
            testSequenceId: number
        ): Promise<void> {
            try {
                normalizeMutateDto(testSequence);
                await api.put(
                    `${ApiRoute.TestSequences}/${testSequenceId}`,
                    testSequence
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating test sequence!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(testSequenceId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.TestSequences}/${testSequenceId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting test sequence!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetTestSequences(): void {
            this.testSequences = [];
        },
        resetSingleTestSequence(): void {
            this.singleTestSequence = null;
        },
    },
});

function normalizeMutateDto(dto: TestSequenceMutateDTO): void {
    dto.description = normalizeEmptyStringToNull(dto.description);
    dto.precondition = normalizeEmptyStringToNull(dto.precondition);
}
