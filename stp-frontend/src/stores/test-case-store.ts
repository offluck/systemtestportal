import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import {
    ApiRoute,
    convertToApiError,
    normalizeEmptyStringToNull,
} from '../utils/store-utils';

export interface TestCase {
    id: number;
    name: string;
    description: string | null;
    precondition: string | null;
    testSequenceIndex: number;
    createdAt: string;
    testSequenceId: number;
}

export type TestCasePostDTO = Pick<
    TestCase,
    | 'name'
    | 'description'
    | 'precondition'
    | 'testSequenceIndex'
    | 'testSequenceId'
>;
export type TestCasePutDTO = TestCasePostDTO;
export type TestCaseMutateDTO = TestCasePostDTO | TestCasePutDTO;

interface TestCaseStoreState {
    testCases: TestCase[];
    singleTestCase: TestCase | null;
}

export const useTestCaseStore = defineStore('test-case', {
    persist: true,
    state: (): TestCaseStoreState => ({
        testCases: [],
        singleTestCase: null,
    }),
    getters: {
        sortedTestCases(state): TestCase[] {
            return state.testCases.sort(
                (a, b) => a.testSequenceIndex - b.testSequenceIndex
            );
        },
    },
    actions: {
        async getAll(testSequenceId: number): Promise<TestCase[]> {
            try {
                const apiResponse = await api.get(ApiRoute.TestCases, {
                    params: { test_sequence_id: testSequenceId },
                });
                this.testCases = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading test cases!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(testCaseId: number): Promise<TestCase> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.TestCases}/${testCaseId}`
                );
                this.singleTestCase = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single test case!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(testCase: TestCasePostDTO): Promise<TestCase> {
            try {
                normalizeMutateDto(testCase);
                const apiResponse = await api.post(
                    ApiRoute.TestCases,
                    testCase
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new test case!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(testCase: TestCasePutDTO, testCaseId: number): Promise<void> {
            try {
                normalizeMutateDto(testCase);
                await api.put(`${ApiRoute.TestCases}/${testCaseId}`, testCase);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating test case!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(testCaseId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.TestCases}/${testCaseId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting test case!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetTestCases(): void {
            this.testCases = [];
        },
        resetSingleTestCase(): void {
            this.singleTestCase = null;
        },
    },
});

function normalizeMutateDto(dto: TestCaseMutateDTO): void {
    dto.description = normalizeEmptyStringToNull(dto.description);
    dto.precondition = normalizeEmptyStringToNull(dto.precondition);
}
