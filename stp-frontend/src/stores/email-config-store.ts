import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import {
    ApiRoute,
    convertToApiError,
} from '../utils/store-utils';

export interface EmailConfig{
    id: number;
    title: string;
    email: string;
    appPassword: string;
    smtpServer: string;
}

export type EmailConfigPostDTO = Pick<
    EmailConfig,
    | 'title'
    | 'email'
    | 'appPassword'
    | 'smtpServer'
>;
export type EmailConfigPutDTO = EmailConfigPostDTO;
export type EmailConfigMutateDTO = EmailConfigPostDTO | EmailConfigPutDTO;

interface EmailConfigsStoreState {
    emailConfigs: EmailConfig[];
    singleEmailConfig: EmailConfig | null;
}

export const useEmailConfigsStore = defineStore('email-config', {
    persist: true,
    state: (): EmailConfigsStoreState => ({
        emailConfigs: [],
        singleEmailConfig: null,
    }),
    actions: {
        async getAll(): Promise<EmailConfig[]> {
            try {
                const apiResponse = await api.get(ApiRoute.EmailConfigs);
                this.emailConfigs = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading email configs!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(emailConfigId: number): Promise<EmailConfig> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.EmailConfigs}/${emailConfigId}`
                );
                this.singleEmailConfig = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single email config!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(emailConfig: EmailConfigPostDTO): Promise<void> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.EmailConfigs,
                    emailConfig
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new email config!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(emailConfig: EmailConfigPutDTO, emailConfigId: number): Promise<void> {
            try {
                await api.put(`${ApiRoute.EmailConfigs}/${emailConfigId}`, emailConfig);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating email config!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(emailConfigId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.EmailConfigs}/${emailConfigId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting email config!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetEmailConfigs(): void {
            this.emailConfigs = [];
        },
        resetSingleEmailConfig(): void {
            this.singleEmailConfig = null;
        },
    },
});
