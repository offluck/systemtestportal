import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface EmailTemplate {
    id: number;
    name: string;
    content: string;
    emailConfigId: number;
}

export type EmailTemplatePutDTO = Pick<
    EmailTemplate,
    'name' | 'content' | 'emailConfigId'
>;

export type EmailMutateDTO = EmailTemplatePutDTO;

interface EmailTemplateStoreState {
    emailTemplates: EmailTemplate[];
    singleEmailTemplate: EmailTemplate | null;
}

export const useEmailTemplateStore = defineStore('email-templates', {
    persist: true,
    state: (): EmailTemplateStoreState => ({
        emailTemplates: [],
        singleEmailTemplate: null,
    }),
    actions: {
      async getAll(): Promise<EmailTemplate[]> {
        try {
            const apiResponse = await api.get(ApiRoute.EmailTemplates);
            this.emailTemplates = apiResponse.data;
            return apiResponse.data;
        } catch (e: unknown) {
            const apiError = convertToApiError(
                e,
                'Failed loading email templates!'
            );
            notifyError(apiError.messageToDisplay);
            throw apiError;
        }
    },
        async getSingle(emailTemplateId: number): Promise<EmailTemplate> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.EmailTemplates}/${emailTemplateId}`
                );
                this.singleEmailTemplate = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single email template!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            emailTemplate: EmailTemplatePutDTO,
            emailTemplateId: number
        ): Promise<void> {
            try {
                await api.put(
                    `${ApiRoute.EmailTemplates}/${emailTemplateId}`,
                    emailTemplate
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating email template!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetEmailTemplates(): void {
            this.emailTemplates = [];
        },
        resetSingleEmailTemplate(): void {
            this.singleEmailTemplate = null;
        },
    },
});
