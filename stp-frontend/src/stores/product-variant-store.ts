import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface ProductVariant {
    id: number;
    name: string;
    createdAt: string;
    productVersionId: number;
}

export type ProductVariantPostDTO = Pick<
    ProductVariant,
    'name' | 'productVersionId'
>;
export type ProductVariantPutDTO = ProductVariantPostDTO;

interface ProductVariantStoreState {
    productVariants: ProductVariant[];
    singleProductVariant: ProductVariant | null;
}

export const useProductVariantStore = defineStore('product-variant', {
    persist: true,
    state: (): ProductVariantStoreState => ({
        productVariants: [],
        singleProductVariant: null,
    }),
    actions: {
        async getAll(productVersionId: number): Promise<ProductVariant[]> {
            try {
                const apiResponse = await api.get(ApiRoute.Variants, {
                    params: { product_version_id: productVersionId },
                });
                this.productVariants = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading product variants!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(productVariantId: number): Promise<ProductVariant> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.Variants}/${productVariantId}`
                );
                this.singleProductVariant = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single product variant!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(
            productVariant: ProductVariantPostDTO
        ): Promise<ProductVariant> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.Variants,
                    productVariant
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new product variant!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            productVariant: ProductVariantPutDTO,
            productVariantId: number
        ): Promise<void> {
            try {
                await api.put(
                    `${ApiRoute.Variants}/${productVariantId}`,
                    productVariant
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating product variant!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(productVariantId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.Variants}/${productVariantId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting product variant!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetVariants(): void {
            this.productVariants = [];
        },
        resetSingleVariant(): void {
            this.singleProductVariant = null;
        },
    },
});
