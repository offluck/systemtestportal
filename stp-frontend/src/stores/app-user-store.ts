import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import {
    notifyError,
    notifyInfo,
    notifySuccess,
} from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from 'src/utils/store-utils';

export interface AppUser {
    id: number;
    name: string;
    email: string;
    isEnabled: boolean;
    biography?: string;
    imagePath?: string;
    isShownPublic: boolean;
    createdAt: Date;
    appRoleId: number;
}

export interface AppUserPostDTO {
    name: string;
    email: string;
    password: string;
    isEnabled: boolean;
    biography?: string;
    imagePath?: string;
    appRoleId: number;
}

export interface AppUserPutDTO extends AppUserPostDTO {
    newPassword: string;
}

interface AppUserStoreState {
    appUsers: AppUser[];
    singleAppUser: AppUser | null;
}

export const useAppUserStore = defineStore('appUser', {
    persist: true,
    state: (): AppUserStoreState => ({
        appUsers: [],
        singleAppUser: null,
    }),
    actions: {
        async getAll(): Promise<AppUser[]> {
            try {
                const apiResponse = await api.get(ApiRoute.AppUsers);
                this.appUsers = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading app users!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(userId: number): Promise<AppUser> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.AppUsers}/${userId}`
                );
                this.singleAppUser = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single app user!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(appUser: AppUserPostDTO): Promise<void> {
            try {
                await api.post(ApiRoute.AppUsers, appUser);
                notifyInfo('Created new user successfully.');
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new app user!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(appUser: AppUserPutDTO, userId: number): Promise<boolean> {
            try {
                const response = await api.put(
                    `${ApiRoute.AppUsers}/${userId}`,
                    appUser
                );

                if (response.status === 200) {
                    notifySuccess('Updated account informations successfully.');
                    return true;
                }
                return false;
            } catch (e: any) {
                if (e.response.status === 401) {
                    notifyError('Wrong password given.');
                    return false;
                }

                const apiError = convertToApiError(
                    e,
                    'Failed updating account informations! Try again later.'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(userId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.AppUsers}/${userId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting app user!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetAppUsers(): void {
            this.appUsers = [];
        },
        resetSingleAppUser(): void {
            this.singleAppUser = null;
        },
        async register(appUser: AppUserPostDTO): Promise<void> {
            try {
                await api.post(ApiRoute.Register, appUser);
            } catch (e: any) {
                switch (e.response.status) {
                    case undefined:
                        return Promise.reject();
                    case 409:
                        notifyError('Given mail is not available.');
                        break;
                    default:
                        notifyError(
                            'Something went wrong, please try later again'
                        );
                        break;
                }
                const apiError = convertToApiError(
                    e,
                    'Failed to register app user!'
                );
                throw apiError;
            }
        },
    },
});

export enum AppRole {
    ADMIN,
    USER,
}
