import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import {
  ApiRoute,
  convertToApiError,
} from '../utils/store-utils';

//model may need to change
export interface SubscribedUser {
  id: number;
  name: string;
  email: string;
}

interface SubscribedUsersStoreState {
  subscribedUsers: SubscribedUser[];
  singleSubscribedUser: SubscribedUser | null;
}

export const useSubscribedUsersStore = defineStore('subscribed-users', {
  persist: true,
  state: (): SubscribedUsersStoreState => ({
    subscribedUsers: [],
    singleSubscribedUser: null,
  }),
  getters: {
    sortedSubscribedUsers(state): SubscribedUser[] {
      // Implement any necessary getters
      return state.subscribedUsers;
    },
  },
  actions: {
    async getAll(testCaseId: number): Promise<SubscribedUser[]> {
      try {
        const apiResponse = await api.get(`${ApiRoute.TestCases}/${testCaseId}/subscribed-users`);
        this.subscribedUsers = apiResponse.data;
        return apiResponse.data;
      } catch (e: unknown) {
        const apiError = convertToApiError(
          e,
          'Failed loading subscribed users!'
        );
        notifyError(apiError.messageToDisplay);
        throw apiError;
      }
    },
    async getSingle(testCaseId: number, subscribedUserId: number): Promise<SubscribedUser> {
      try {
        console.log(subscribedUserId);
        const apiResponse = await api.get(
          `${ApiRoute.TestCases}/${testCaseId}/subscribed-users/${subscribedUserId}`
        );
        this.singleSubscribedUser = apiResponse.data;
        return apiResponse.data;
      } catch (e: unknown) {
        const apiError = convertToApiError(
          e,
          'Failed loading single subscribed user!'
        );
        notifyError(apiError.messageToDisplay);
        throw apiError;
      }
    },
    async post(testCaseId: number, subscribedUserId: number): Promise<void> {
      try {
        // Implement the necessary logic for adding a link between the resources
        await api.post(
          `${ApiRoute.TestCases}/${testCaseId}/subscribed-users/${subscribedUserId}`
        );
      } catch (e: unknown) {
        const apiError = convertToApiError(
          e,
          'Failed adding link between resources!'
        );
        notifyError(apiError.messageToDisplay);
        throw apiError;
      }
    },
    async delete(testCaseId: number, subscribedUserId: number): Promise<void> {
      try {
        // Implement the necessary logic for deleting a subscribed user
        await api.delete(`${ApiRoute.TestCases}/${testCaseId}/subscribed-users/${subscribedUserId}`);
      } catch (e: unknown) {
        const apiError = convertToApiError(
          e,
          'Failed deleting subscribed user!'
        );
        notifyError(apiError.messageToDisplay);
        throw apiError;
      }
    },
    resetSubscribedUsers(): void {
      this.subscribedUsers = [];
    },
    resetSingleSubscribedUser(): void {
      this.singleSubscribedUser = null;
    },
  },
});
