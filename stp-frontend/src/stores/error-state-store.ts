import { defineStore } from 'pinia';

type ErrorStateStore = {
    error: Error | null;
};

type GlobalError = ErrorStateStore['error'];

export const useErrorStateStore = defineStore('error-state', {
    //TODO: rethink if this should be persisted?
    state: (): ErrorStateStore => ({
        error: null,
    }),
    actions: {
        setError(newError: GlobalError): void {
            this.error = newError;
        },
        resetError(): void {
            this.setError(null);
        },
    },
});
