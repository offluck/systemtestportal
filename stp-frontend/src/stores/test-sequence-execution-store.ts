import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { TimeStampUTC } from 'src/utils/date-utils';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface TestSequenceExecution {
    id: number;
    preconditionFulfilled: boolean;
    createdAt: TimeStampUTC;
    updatedAt: TimeStampUTC;
    testSequenceId: number;
    testerId: number | null;
    executionStatusId: number;
}

export type TestSequenceExecutionPostDTO = Pick<
    TestSequenceExecution,
    | 'preconditionFulfilled'
    | 'testSequenceId'
    | 'testerId'
    | 'executionStatusId'
>;
export type TestSequenceExecutionPutDTO = TestSequenceExecutionPostDTO;
export type TestSequenceExecutionMutateDTO =
    | TestSequenceExecutionPostDTO
    | TestSequenceExecutionPutDTO;

interface TestSequenceExecutionStoreState {
    testSequenceExecutions: TestSequenceExecution[];
    singleTestSequenceExecution: TestSequenceExecution | null;
}

type QueryParams = {
    test_sequence_id?: number;
    tester_id?: number;
    execution_status_id?: number;
};

type GetAllQueryParams = QueryParams &
    Required<Pick<QueryParams, 'test_sequence_id'>>;

export const useTestSequenceExecutionStore = defineStore(
    'test-sequence-execution',
    {
        persist: true,
        state: (): TestSequenceExecutionStoreState => ({
            testSequenceExecutions: [],
            singleTestSequenceExecution: null,
        }),
        actions: {
            async getAll(
                queryParams: GetAllQueryParams
            ): Promise<TestSequenceExecution[]> {
                try {
                    const apiResponse = await api.get(
                        ApiRoute.TestSequenceExecutions,
                        { params: queryParams }
                    );
                    this.testSequenceExecutions = apiResponse.data;
                    return apiResponse.data;
                } catch (e: unknown) {
                    const apiError = convertToApiError(
                        e,
                        'Failed loading test sequence executions!'
                    );
                    notifyError(apiError.messageToDisplay);
                    throw apiError;
                }
            },
            async getSingle(
                testSequenceExecutionId: number,
                queryParams?: QueryParams
            ): Promise<TestSequenceExecution> {
                try {
                    const apiResponse = await api.get(
                        `${ApiRoute.TestSequenceExecutions}/${testSequenceExecutionId}`,
                        { params: queryParams }
                    );
                    this.singleTestSequenceExecution = apiResponse.data;
                    return apiResponse.data;
                } catch (e: unknown) {
                    const apiError = convertToApiError(
                        e,
                        'Failed loading single test sequence execution!'
                    );
                    notifyError(apiError.messageToDisplay);
                    throw apiError;
                }
            },
            async post(
                testSequenceExecution: TestSequenceExecutionPostDTO
            ): Promise<TestSequenceExecution> {
                try {
                    const apiResponse = await api.post(
                        ApiRoute.TestSequenceExecutions,
                        testSequenceExecution
                    );
                    return apiResponse.data;
                } catch (e: unknown) {
                    const apiError = convertToApiError(
                        e,
                        'Failed creating new test sequence execution!'
                    );
                    notifyError(apiError.messageToDisplay);
                    throw apiError;
                }
            },
            async put(
                testSequenceExecution: TestSequenceExecutionPutDTO,
                testSequenceExecutionId: number
            ): Promise<TestSequenceExecution> {
                try {
                    const apiResponse = await api.put(
                        `${ApiRoute.TestSequenceExecutions}/${testSequenceExecutionId}`,
                        testSequenceExecution
                    );
                    return apiResponse.data;
                } catch (e: unknown) {
                    const apiError = convertToApiError(
                        e,
                        'Failed updating test sequence execution!'
                    );
                    notifyError(apiError.messageToDisplay);
                    throw apiError;
                }
            },
            async delete(testSequenceExecutionId: number): Promise<void> {
                try {
                    await api.delete(
                        `${ApiRoute.TestSequenceExecutions}/${testSequenceExecutionId}`
                    );
                } catch (e: unknown) {
                    const apiError = convertToApiError(
                        e,
                        'Failed deleting test sequence execution!'
                    );
                    notifyError(apiError.messageToDisplay);
                    throw apiError;
                }
            },
            resetTestSequenceExecutions(): void {
                this.testSequenceExecutions = [];
            },
            resetSingleTestSequenceExecution(): void {
                this.singleTestSequenceExecution = null;
            },
        },
    }
);
