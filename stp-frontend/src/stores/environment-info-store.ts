import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface EnvironmentInfo {
    id: number;
    content: string;
    createdAt: string;
    updatedAt: string;
    testSequenceExecutionId: number;
}

export type EnvironmentInfoPostDTO = Pick<
    EnvironmentInfo,
    'content' | 'testSequenceExecutionId'
>;
export type EnvironmentInfoPutDTO = EnvironmentInfoPostDTO;
export type EnvironmentInfoMutateDTO =
    | EnvironmentInfoPostDTO
    | EnvironmentInfoPutDTO;

type QueryParams = {
    test_sequence_execution_id?: number;
};

type GetAllQueryParams = QueryParams &
    Required<Pick<QueryParams, 'test_sequence_execution_id'>>;

export const useEnvironmentInfoStore = defineStore('environment-info', {
    actions: {
        async getAll(
            queryParams: GetAllQueryParams
        ): Promise<EnvironmentInfo[]> {
            try {
                const apiResponse = await api.get(ApiRoute.EnvironmentInfos, {
                    params: queryParams,
                });
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading environment infos!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(environmentInfoId: number): Promise<EnvironmentInfo> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.EnvironmentInfos}/${environmentInfoId}`
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single environment info!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(
            environmentInfo: EnvironmentInfoPostDTO
        ): Promise<EnvironmentInfo> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.EnvironmentInfos,
                    environmentInfo
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new environment info!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            environmentInfo: EnvironmentInfoPutDTO,
            environmentInfoId: number
        ): Promise<void> {
            try {
                await api.put(
                    `${ApiRoute.EnvironmentInfos}/${environmentInfoId}`,
                    environmentInfo
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating environment info!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(environmentInfoId: number): Promise<void> {
            try {
                await api.delete(
                    `${ApiRoute.EnvironmentInfos}/${environmentInfoId}`
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting environment info!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
    },
});
