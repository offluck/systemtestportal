import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface TestStep {
    id: number;
    instruction: string;
    expectedResult: string;
    testCaseIndex: number;
    createdAt: string;
    testCaseId: number;
}

export type TestStepPostDTO = Pick<
    TestStep,
    'instruction' | 'expectedResult' | 'testCaseId' | 'testCaseIndex'
>;
export type TestStepPutDTO = TestStepPostDTO;
export type TestStepMutateDTO = TestStepPostDTO | TestStepPutDTO;

interface TestStepStoreState {
    testSteps: TestStep[];
    singleTestStep: TestStep | null;
}

export const useTestStepStore = defineStore('test-step', {
    persist: true,
    state: (): TestStepStoreState => ({
        testSteps: [],
        singleTestStep: null,
    }),
    getters: {
        sortedTestSteps(state): TestStep[] {
            return state.testSteps.sort(
                (a, b) => a.testCaseIndex - b.testCaseIndex
            );
        },
    },
    actions: {
        async getAll(testCaseId: number): Promise<TestStep[]> {
            try {
                const apiResponse = await api.get(ApiRoute.TestSteps, {
                    params: { test_case_id: testCaseId },
                });
                this.testSteps = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading test steps!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(testStepId: number): Promise<TestStep> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.TestSteps}/${testStepId}`
                );
                this.singleTestStep = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single test step!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(testStep: TestStepPostDTO): Promise<TestStep> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.TestSteps,
                    testStep
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new test step!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(testStep: TestStepPutDTO, testStepId: number): Promise<void> {
            try {
                await api.put(`${ApiRoute.TestSteps}/${testStepId}`, testStep);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating test step!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(testStepId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.TestSteps}/${testStepId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting test step!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetTestSteps(): void {
            this.testSteps = [];
        },
        resetSingleTestStep(): void {
            this.singleTestStep = null;
        },
    },
});
