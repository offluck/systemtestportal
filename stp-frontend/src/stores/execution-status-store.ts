import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export enum ExecutionStatusEnum {
    Untested = 'Untested',
    Skipped = 'Skipped',
    NotApplicable = 'NotApplicable',
    NotWorking = 'NotWorking',
    PartiallyWorking = 'PartiallyWorking',
    FullyWorking = 'FullyWorking',
}

export interface ExecutionStatus {
    id: number;
    name: ExecutionStatusEnum;
    createdAt: string;
    sendNotification: boolean;
}

export const useExecutionStatusStore = defineStore('execution-status', {
    actions: {
        async getAll(): Promise<ExecutionStatus[]> {
            try {
                const apiResponse = await api.get(ApiRoute.ExecutionStatuses);
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading execution statuses!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
    },
});
