import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError, notifyInfo } from 'src/utils/general-utils';
import { hashPassword } from 'src/utils/password-utils';
import { ApiRoute, convertToApiError } from 'src/utils/store-utils';
import { AppUser } from './app-user-store';

interface AuthenticationStoreState {
    user: AppUser | null;
    lastPage: string;
    authenticationFail: boolean;
}

export const useAuthenticationStore = defineStore('login', {
    persist: true,
    state: (): AuthenticationStoreState => ({
        user: null,
        lastPage: '',
        authenticationFail: false,
    }),
    actions: {
        async loginUser(email: string, password: string): Promise<boolean> {
            try {
                const data = {
                    email: email,
                    password: await hashPassword(password),
                };

                const apiResponse = await api.post(ApiRoute.Login, data);

                if (apiResponse.status === 200) {
                    this.user = apiResponse.data;

                    notifyInfo('Login successfull.');
                    return true;
                } else {
                    notifyError('Login failed!');
                    return false;
                }
            } catch (e: any) {
                let apiError;
                if (e.response.status === 401) {
                    apiError = convertToApiError(e, 'Wrong email or password.');
                } else {
                    apiError = convertToApiError(
                        e,
                        'Internal error with while trying to login in.'
                    );
                }
                notifyError(apiError.messageToDisplay);
                return Promise.reject();
            }
        },
        async logoutUser(): Promise<boolean> {
            try {
                const apiResponse = await api.get(ApiRoute.Logout);

                if (apiResponse.status === 200) {
                    this.user = null;
                    if (!this.authenticationFail) {
                        notifyInfo('Logout successfull.');
                    }
                    return true;
                } else {
                    notifyError('Logout failed!');
                    return false;
                }
            } catch (e: unknown) {
                const apiError = convertToApiError(e, 'Failed to logout user!');
                notifyError(apiError.messageToDisplay);

                return Promise.reject();
            }
        },

        updateUser(user: AppUser) {
            this.user = user;
        },
        isAuthenticated(): boolean {
            return this.user !== null && this.user !== undefined;
        },

        getUser(): AppUser | null {
            return this.user;
        },
    },
});
