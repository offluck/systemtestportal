import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from 'src/utils/store-utils';

export const useAppUserActivationStore = defineStore('appUserActivation', {
    actions: {
        async activate(activation_token: string): Promise<void> {
            try {
                await api.post(ApiRoute.Activation, {
                    activationToken: activation_token,
                });
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed activating account!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
    },
});
