import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import {
    ApiRoute,
    convertToApiError,
    normalizeEmptyStringToNull,
} from '../utils/store-utils';

export interface Product {
    id: number;
    name: string;
    isPublic: boolean;
    createdAt: string;
    description: string | null;
    imagePath: string | null;
}

export type ProductPostDTO = Pick<Product, 'name' | 'isPublic' | 'description'>;
export type ProductPutDTO = ProductPostDTO;
export type ProductMutateDTO = ProductPostDTO | ProductPutDTO;

/**
 * For data that is not yet loaded use the following as typing:
 *  arrayOfObjects: ObjectType[],   //for initialization use []
 *  singleObject: ObjectType | null //for initialization use null
 */
interface ProductStoreState {
    products: Product[];
    singleProduct: Product | null;
}

export const useProductStore = defineStore('product', {
    persist: true,
    state: (): ProductStoreState => ({
        products: [],
        singleProduct: null,
    }),
    actions: {
        async getAll(): Promise<Product[]> {
            try {
                const apiResponse = await api.get(ApiRoute.Products);
                this.products = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading products!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(productId: number): Promise<Product> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.Products}/${productId}`
                );
                this.singleProduct = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single product!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(product: ProductPostDTO): Promise<Product> {
            try {
                normalizeMutateDto(product);
                const apiResponse = await api.post(ApiRoute.Products, product);
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new product!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(product: ProductPutDTO, productId: number): Promise<void> {
            try {
                normalizeMutateDto(product);
                await api.put(`${ApiRoute.Products}/${productId}`, product);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating product!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(productId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.Products}/${productId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting product!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetProducts(): void {
            this.products = [];
        },
        resetSingleProduct(): void {
            this.singleProduct = null;
        },
    },
});

function normalizeMutateDto(productMutateDto: ProductMutateDTO): void {
    productMutateDto.description = normalizeEmptyStringToNull(
        productMutateDto.description
    );
}
