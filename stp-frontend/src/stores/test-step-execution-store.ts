import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { TimeStampUTC } from 'src/utils/date-utils';
import { notifyError } from 'src/utils/general-utils';
import {
    ApiRoute,
    convertToApiError,
    normalizeEmptyStringToNull,
} from '../utils/store-utils';

export interface TestStepExecution {
    id: number;
    comment: string | null;
    createdAt: TimeStampUTC;
    updatedAt: TimeStampUTC;
    testStepId: number;
    testCaseExecutionId: number;
    executionStatusId: number;
}

export type TestStepExecutionPostDTO = Pick<
    TestStepExecution,
    'comment' | 'testStepId' | 'testCaseExecutionId' | 'executionStatusId'
>;
export type TestStepExecutionPutDTO = TestStepExecutionPostDTO;
export type TestStepExecutionMutateDTO =
    | TestStepExecutionPostDTO
    | TestStepExecutionPutDTO;

interface TestStepExecutionStoreState {
    testStepExecutions: TestStepExecution[];
    singleTestStepExecution: TestStepExecution | null;
}

type QueryParams = {
    test_step_id?: number;
    test_case_execution_id?: number;
    execution_status_id?: number;
};

type GetAllQueryParams = QueryParams &
    Required<Pick<QueryParams, 'test_case_execution_id'>>;

export const useTestStepExecutionStore = defineStore('test-step-execution', {
    persist: true,
    state: (): TestStepExecutionStoreState => ({
        testStepExecutions: [],
        singleTestStepExecution: null,
    }),
    actions: {
        async getAll(
            queryParams: GetAllQueryParams
        ): Promise<TestStepExecution[]> {
            try {
                const apiResponse = await api.get(ApiRoute.TestStepExecutions, {
                    params: queryParams,
                });
                this.testStepExecutions = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading test step executions!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(
            testStepExecutionId: number
        ): Promise<TestStepExecution> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.TestStepExecutions}/${testStepExecutionId}`
                );
                this.singleTestStepExecution = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single test step execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(
            testStepExecution: TestStepExecutionPostDTO
        ): Promise<TestStepExecution> {
            try {
                normalizeMutateDto(testStepExecution);
                const apiResponse = await api.post(
                    ApiRoute.TestStepExecutions,
                    testStepExecution
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new test step execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            testStepExecution: TestStepExecutionPutDTO,
            testStepExecutionId: number
        ): Promise<void> {
            try {
                normalizeMutateDto(testStepExecution);
                await api.put(
                    `${ApiRoute.TestStepExecutions}/${testStepExecutionId}`,
                    testStepExecution
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating test step execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(testStepExecutionId: number): Promise<void> {
            try {
                await api.delete(
                    `${ApiRoute.TestStepExecutions}/${testStepExecutionId}`
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting test step execution!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetTestStepExecutions(): void {
            this.testStepExecutions = [];
        },
        resetSingleTestStepExecution(): void {
            this.singleTestStepExecution = null;
        },
    },
});

function normalizeMutateDto(
    testStepExecutionMutateDto: TestStepExecutionMutateDTO
): void {
    testStepExecutionMutateDto.comment = normalizeEmptyStringToNull(
        testStepExecutionMutateDto.comment
    );
}
