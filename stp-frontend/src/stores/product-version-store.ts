import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from '../utils/store-utils';

export interface ProductVersion {
    id: number;
    name: string;
    createdAt: string;
    productId: number;
}

export type ProductVersionPostDTO = Pick<ProductVersion, 'name' | 'productId'>;
export type ProductVersionPutDTO = ProductVersionPostDTO;

interface ProductVersionStoreState {
    productVersions: ProductVersion[];
    singleProductVersion: ProductVersion | null;
}

export const useProductVersionStore = defineStore('product-version', {
    persist: true,
    state: (): ProductVersionStoreState => ({
        productVersions: [],
        singleProductVersion: null,
    }),
    actions: {
        async getAll(productId: number): Promise<ProductVersion[]> {
            try {
                const apiResponse = await api.get(ApiRoute.Versions, {
                    params: { product_id: productId },
                });
                this.productVersions = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading product versions!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async getSingle(productVersionId: number): Promise<ProductVersion> {
            try {
                const apiResponse = await api.get(
                    `${ApiRoute.Versions}/${productVersionId}`
                );
                this.singleProductVersion = apiResponse.data;
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed loading single product version!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async post(
            productVersion: ProductVersionPostDTO
        ): Promise<ProductVersion> {
            try {
                const apiResponse = await api.post(
                    ApiRoute.Versions,
                    productVersion
                );
                return apiResponse.data;
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed creating new product version!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async put(
            productVersion: ProductVersionPutDTO,
            productVersionId: number
        ): Promise<void> {
            try {
                await api.put(
                    `${ApiRoute.Versions}/${productVersionId}`,
                    productVersion
                );
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed updating product version!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async delete(productVersionId: number): Promise<void> {
            try {
                await api.delete(`${ApiRoute.Versions}/${productVersionId}`);
            } catch (e: unknown) {
                const apiError = convertToApiError(
                    e,
                    'Failed deleting product version!'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        resetVersions(): void {
            this.productVersions = [];
        },
        resetSingleVersion(): void {
            this.singleProductVersion = null;
        },
    },
});
