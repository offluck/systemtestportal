import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { notifyError } from 'src/utils/general-utils';
import { ApiRoute, convertToApiError } from 'src/utils/store-utils';

export interface PasswordReset{
  password: string;
  token: string;
}

export type PasswordResetPostDTO = Pick<
  PasswordReset,
  | 'password'
  | 'token'
>;

export const usePasswordResetStore = defineStore('passwordResetStore', {
    actions: {
        async request_reset_token(email: string): Promise<void> {
            try {
              await api.post(`${ApiRoute.PasswordResetRequest}?email=${encodeURIComponent(email)}`);
            } catch (e: unknown) {
              console.log(e);
                const apiError = convertToApiError(
                    e,
                    'Failed to send reset request'
                );
                notifyError(apiError.messageToDisplay);
                throw apiError;
            }
        },
        async reset_password(resetData: PasswordResetPostDTO) : Promise<void> {
          try {
            await api.post(ApiRoute.PasswordReset, resetData);
          } catch (e: unknown) {
            const apiError = convertToApiError(
              e,
              'Password cannot be reset'
          );
          notifyError(apiError.messageToDisplay);
          throw apiError;
          }
        }
    },
});
