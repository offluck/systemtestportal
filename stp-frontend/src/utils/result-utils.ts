type Ok<T> = {
    readonly isOk: true;
    readonly isErr: false;
    readonly value: T;
    unwrap(): T;
};

type Err<E> = {
    readonly isOk: false;
    readonly isErr: true;
    readonly error: E;
    readonly statusCode?: number;
    unwrap(): never;
};

/**
 * Convert from Exceptions to typesafe Result Error handling:
 * ```ts
 * function functionThatReturnsResult(): Result<number> {
 *  try {
 *      const value: number = functionThatThrowsException()
 *      return ok(value);
 *  } catch (e: Error) {
 *      return err(e.message);
 *  }
 * }
 *
 * function main() {
 *  const result = functionThatReturnsResult();
 *  //to access the value in the result, it needs to be checked, wheter it is an Ok or Err type
 *  if (result.isOk) {
 *      // now we know it is of type Ok and can access value property
 *      console.log(result.value);
 *  } else {
 *      // now we know it is of type Err and can access error property
 *      console.log(result.error);
 *  }
 * }
 * ```
 */
export type Result<T, E = string> = Ok<T> | Err<E>;

export function ok<T>(data: T): Ok<T> {
    return {
        isOk: true,
        isErr: false,
        value: data,
        unwrap(): T {
            return data;
        },
    };
}

export function err<E>(error: E, statusCode?: number): Err<E> {
    return {
        isOk: false,
        isErr: true,
        error,
        statusCode,
        unwrap(): never {
            const err = String(error).concat(String(statusCode ?? ''));
            throw new Error(err);
        },
    };
}
