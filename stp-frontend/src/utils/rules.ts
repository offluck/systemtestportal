import {
    checkPasswordStrength,
    PasswordCheckStrength,
} from 'src/utils/password-utils';

type RuleEvaluationResult = boolean | string;

export function ruleOptionalName(val: string | null): boolean {
    return !!val && ruleName(val);
}

export function ruleName(val: string): boolean {
    return val.length > 0;
}

export function buildMinLengthRule(min: number): (value: string) => boolean {
    if (min < 0) {
        throw new Error('Minimal allowed length of a string is 0!');
    }
    return function minLengthRule(value: string) {
        return value.length > min;
    };
}

export function ruleActualResult(value: string): RuleEvaluationResult {
    if (buildMinLengthRule(5)(value))
        return 'The description has to be at least 5 characters long';

    return true;
}

export function ruleNotEmptyInput() {
    return (val: string) => (val && val.length > 0) || 'Please type something';
}

export function rulePasswordStrength(password: string): boolean {
    const passwordStrength = checkPasswordStrength(password);
    return (
        passwordStrength === PasswordCheckStrength.Ok ||
        passwordStrength === PasswordCheckStrength.Strong
    );
}

export function ruleMatchingPasswords(
    password1: string,
    password2: string
): boolean {
    return password1 === password2;
}

export function ruleIsEmail() {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return (email: string) => emailRegex.test(email) || 'Please give a valid email.';
}
