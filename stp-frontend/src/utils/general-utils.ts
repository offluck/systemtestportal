import {
    matError,
    matCheckCircle,
    matHelp,
    matInfo,
} from '@quasar/extras/material-icons';
import { Notify, QNotifyCreateOptions, QSelect } from 'quasar';
import { ExecutionStatusEnum } from 'src/stores/execution-status-store';
import { Ref } from 'vue';
import { Router } from 'vue-router';
import { ok, err, Result } from './result-utils';

// Notify utils

const baseQuasarNotifyOptions: QNotifyCreateOptions = {
    timeout: 2000,
    progress: true,
    textColor: 'white',
};
const warnNotifyOptions: QNotifyCreateOptions = {
    ...baseQuasarNotifyOptions,
    type: 'warning',
};
const errorNotifyOptions: QNotifyCreateOptions = {
    ...baseQuasarNotifyOptions,
    type: 'negative',
};
const successNotifyOptions: QNotifyCreateOptions = {
    ...baseQuasarNotifyOptions,
    type: 'positive',
};
const infoNotifyOptions: QNotifyCreateOptions = {
    ...baseQuasarNotifyOptions,
    type: 'info',
};

export const loremIpsum =
    'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';

export function notifyWarn(message: string, options?: QNotifyCreateOptions) {
    Notify.create({ ...warnNotifyOptions, message, ...options });
}

export function notifyError(message: string, options?: QNotifyCreateOptions) {
    Notify.create({ ...errorNotifyOptions, message, ...options });
}

export function notifySuccess(message: string, options?: QNotifyCreateOptions) {
    Notify.create({ ...successNotifyOptions, message, ...options });
}

export function notifyInfo(message: string, options?: QNotifyCreateOptions) {
    Notify.create({ ...infoNotifyOptions, message, ...options });
}

// secure parse utils

export function parseIntSecure(string: string, radix?: number): Result<number> {
    const parsedInt: number = parseInt(string, radix);
    if (isNaN(parsedInt)) {
        return err('Failed parsing string to number!');
    }
    return ok(parsedInt);
}
// Path or Query Param parse utils
export function parseParamAsInt(param: string | string[]): Result<number> {
    if (Array.isArray(param)) {
        return err(
            'Param is expected to be a string and not an array of strings!'
        );
    }
    return parseIntSecure(param);
}

export function parseMultipleParamsAsInt(
    param: string | null | (string | null)[]
): Result<number[]> {
    if (!Array.isArray(param)) {
        return err('Param is expected to be an array of strings!');
    }
    const parsedParams: number[] = [];
    for (const el of param) {
        if (el === null) {
            return err('Unexpectedly a param was not defined!');
        }
        const parsedParamResult = parseIntSecure(el);
        if (parsedParamResult.isErr) {
            return parsedParamResult;
        }
        parsedParams.push(parsedParamResult.value);
    }
    return ok(parsedParams);
}

export function parseSingleParamAndThrowOnErr(
    param: string | string[],
    paramName: string
): number {
    const parseResult = parseParamAsInt(param);
    if (parseResult.isErr) {
        notifyError(`${parseResult.error}: '${paramName}'`);
    }
    return parseResult.unwrap();
}

//TODO: hand route params and paramName separately for better debugging
export function parseMultipleParamAndThrowOnErr(
    param: string | null | (string | null)[]
): number[] {
    const parsedParamsResult = parseMultipleParamsAsInt(param);
    if (parsedParamsResult.isErr) {
        notifyError(parsedParamsResult.error);
    }
    return parsedParamsResult.unwrap();
}

// for buttons
export enum Color {
    Refresh = 'green',
    Open = 'primary',
    Continue = 'primary',
    Back = 'grey',
    Execute = 'green',
    Settings = 'orange',
    Search = 'cyan',
    Info = 'blue',
    Create = 'green',
    Edit = 'orange',
    Delete = 'red',
    Cancel = 'red',
}

export enum TextColor {
    White = 'white',
}

export enum ButtonLabel {
    Back = 'Back',
    Execute = 'Execute',
    Create = 'CREATE',
    Edit = 'EDIT',
    Delete = 'DELETE',
    Cancel = 'CANCEL',
    Continue = 'Continue',
}

// q-select filter function

export function filterFn(
    inputValue: string,
    doneFn: (callbackFn: () => void, afterFn?: (ref: QSelect) => void) => void,
    _abortFn: () => void,
    filteredOptions: Ref<string[]>,
    unfilteredOptions: Ref<string[]>
) {
    if (inputValue === '') {
        doneFn(() => {
            filteredOptions.value = unfilteredOptions.value;
        });
    } else {
        doneFn(() => {
            const needle = inputValue.toLowerCase();
            filteredOptions.value = unfilteredOptions.value.filter(
                (v) => v.toLowerCase().indexOf(needle) > -1
            );
        });
    }
}

// find increment value
export function findNextHighestValue(array: number[]): Result<number> {
    if (array.length === 0) {
        return ok(0);
    }
    const maxValue = Math.max(...array);
    if (!Number.isFinite(maxValue) || Number.isNaN(maxValue)) {
        return err('Could not compute the next higher value!');
    }
    return ok(maxValue + 1);
}

// identicality

export function containsAll<T>(first: T[], second: T[]): boolean {
    return first.every((elem) => second.includes(elem));
}

export function getOverlapFromSecond<F, S, V>(
    first: F[],
    valOfF: (f: F) => V & F[keyof F],
    second: S[],
    valOfS: (s: S) => V & S[keyof S]
): S[] {
    const overlapFromSecond = [];
    for (const elOfFirst of first) {
        const foundElInSecond = second.find(
            (elOfSecond) => valOfF(elOfFirst) === valOfS(elOfSecond)
        );
        if (foundElInSecond !== undefined) {
            overlapFromSecond.push(foundElInSecond);
        }
    }
    return overlapFromSecond;
}

export const getFullOverlapFromSecondOrThrow = ((
    first: any,
    valOfF: any,
    second: any,
    valOfS: any
) => {
    const result = getOverlapFromSecond(first, valOfF, second, valOfS);
    if (result.length !== first.length) {
        throw new Error(
            'The second array does not fully overlap with the first array!'
        );
    }
    return result;
}) as typeof getOverlapFromSecond;

function onlyUnique<T>(value: T, index: number, self: T[]) {
    return self.indexOf(value) === index;
}

// return to last page

//TODO: can be removed and replaced with router.back();
export function returnToLastPage(router: Router): void {
    router.go(-1);
}

// error messages
export function buildInformAdminMsg(error: string): string {
    return `Please inform the service administrator! ${error}`;
}

// for testing purposes only
export async function sleep(timeInMs: number): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(resolve, timeInMs);
    });
}

export async function execWithDelay(
    timeInMs: number,
    cb: () => void
): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(() => {
            cb();
            resolve();
        }, timeInMs);
    });
}

//execution status

export type ExecutionStatusIconInfo = { icon: string; color: string };

export function getExecutionStatusIcon(
    status: ExecutionStatusEnum
): ExecutionStatusIconInfo {
    switch (status) {
        case ExecutionStatusEnum.Untested:
            return { icon: matInfo, color: 'blue-grey' };
        case ExecutionStatusEnum.Skipped:
            return { icon: matInfo, color: 'blue' };
        case ExecutionStatusEnum.FullyWorking:
            return { icon: matCheckCircle, color: 'green' };
        case ExecutionStatusEnum.PartiallyWorking:
            return { icon: matInfo, color: 'orange' };
        case ExecutionStatusEnum.NotWorking:
            return { icon: matError, color: 'red' };
        case ExecutionStatusEnum.NotApplicable:
            return { icon: matInfo, color: 'grey' };
        default: {
            notifyError(
                buildInformAdminMsg(
                    `Unexpectedly an unknown Execution Status '${status}' was found!`
                )
            );
            return { icon: matHelp, color: 'red' };
        }
    }
}
