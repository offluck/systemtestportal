export async function hashPassword(password: string): Promise<string> {
  const salt = process.env.SALT;
  const saltedPassword = password + salt;

  const msgBuffer = new TextEncoder().encode(saltedPassword);
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray
      .map((b) => b.toString(16).padStart(2, '0'))
      .join('');

  return hashHex;
}


// inspired by https://gist.github.com/leewinder/a1f6bc5100fc2573b47bb2e0b7937f34
// last accessed on 26.06.2023

// Password strengths
export enum PasswordCheckStrength {
  Short,
  Common,
  Weak,
  Ok,
  Strong,
}

// Expected length of all passwords
const minimumLength = 8;

// Regex to check for a common password string - all based on 5+ length passwords
const commonPasswordPatterns =
  /passw.*|12345.*|09876.*|qwert.*|asdfg.*|zxcvb.*|footb.*|baseb.*|drago.*/;

//
// Checks if the given password matches a set of common password
//
function isPasswordCommon(password: string): boolean {
  return commonPasswordPatterns.test(password);
}

//
// Returns the strength of the current password
//
export function checkPasswordStrength(password: string): PasswordCheckStrength {
  // Build up the strenth of our password
  let numberOfElements = 0;
  numberOfElements = /.*[a-z].*/.test(password)
      ? ++numberOfElements
      : numberOfElements; // Lowercase letters
  numberOfElements = /.*[A-Z].*/.test(password)
      ? ++numberOfElements
      : numberOfElements; // Uppercase letters
  numberOfElements = /.*[0-9].*/.test(password)
      ? ++numberOfElements
      : numberOfElements; // Numbers
  numberOfElements = /[^a-zA-Z0-9]/.test(password)
      ? ++numberOfElements
      : numberOfElements; // Special characters (inc. space)

  // Assume we have a poor password already
  let currentPasswordStrength = PasswordCheckStrength.Short;

  // Check then strenth of this password using some simple rules
  if (password === null || password.length < minimumLength) {
      currentPasswordStrength = PasswordCheckStrength.Short;
  } else if (isPasswordCommon(password) === true) {
      currentPasswordStrength = PasswordCheckStrength.Common;
  } else if (
      numberOfElements === 0 ||
      numberOfElements === 1 ||
      numberOfElements === 2
  ) {
      currentPasswordStrength = PasswordCheckStrength.Weak;
  } else if (numberOfElements === 3) {
      currentPasswordStrength = PasswordCheckStrength.Ok;
  } else {
      currentPasswordStrength = PasswordCheckStrength.Strong;
  }

  // Return the strength of this password
  return currentPasswordStrength;
}

export function getPasswordValidatorStyle(
  passwordStrength: PasswordCheckStrength
) {
  let fontStyle = 'font-weight: 600; color: ';

  switch (passwordStrength) {
      case PasswordCheckStrength.Short:
      case PasswordCheckStrength.Weak:
          fontStyle += 'darkred';
          break;
      case PasswordCheckStrength.Common:
          fontStyle += 'red';
          break;
      case PasswordCheckStrength.Ok:
          fontStyle += 'lightgreen';
          break;
      case PasswordCheckStrength.Strong:
          fontStyle += 'darkgreen';
          break;
  }

  return fontStyle;
}

