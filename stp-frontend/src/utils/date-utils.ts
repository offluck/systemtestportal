import { timeAgo } from 'src/boot/timeago';

export type TimeStampUTC = string;

export function getRelativeDateDiff(timestampUTC: TimeStampUTC): string {
    //TODO: when localization gets implemented uncomment the following: TimeAgo.addLocale(de);
    //TODO: check if this can return something similar to NotANumber but for Dates
    const localeDate = new Date(timestampUTC);
    return getRelativeDateDiffFromDate(localeDate);
}

export function getRelativeDateDiffFromDate(date: Date): string {
    return timeAgo.format(date);
}
