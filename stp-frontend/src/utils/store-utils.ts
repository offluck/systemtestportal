import { isAxiosError } from 'axios';

export interface ApiError {
    messageToDisplay: string;
    statusCode?: number;
    backendError?: BackendError;
}

interface BackendError {
    message: string;
}

export function convertToApiError(
    e: unknown,
    messageToDisplay: string
): ApiError {
    const apiError: ApiError = { messageToDisplay };
    if (isAxiosError(e)) {
        if (e.response !== undefined) {
            apiError.backendError = e.response.data;
            apiError.statusCode = e.response.status;
        }
    } else {
        apiError.messageToDisplay =
            'Unknown error occurred while communicating with the server.';
    }
    return apiError;
}

export enum ApiRoute {
    Login = '/auth/login',
    Logout = '/auth/logout',
    AppUsers = '/users',
    Products = '/products',
    Versions = '/versions',
    Variants = '/variants',
    TestSequences = '/test-sequences',
    TestCases = '/test-cases',
    TestSteps = '/test-steps',
    TestSequenceExecutions = '/test-sequence-executions',
    TestCaseExecutions = '/test-case-executions',
    TestStepExecutions = '/test-step-executions',
    ExecutionStatuses = '/execution-statuses',
    EnvironmentInfos = '/execution-environment-info',
    EmailTemplates = '/email-templates',
    EmailConfigs = '/email-configs',
    PasswordResetRequest = '/pwd_reset_request',
    PasswordReset = '/set_password',
    Register = '/register',
    Activation = '/activate',
}

export function normalizeEmptyStringToNull(
    value: string | null
): string | null {
    return value !== null && value.trim() === '' ? null : value;
}

export const DefaultVersionName = 'Default';
export const DefaultVariantName = DefaultVersionName;
