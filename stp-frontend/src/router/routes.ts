import { RouteRecordRaw } from 'vue-router';

export enum PathParams {
    ProductId = 'productId',
    VersionId = 'versionId',
    VariantId = 'variantId',
    TestSequenceId = 'testSequenceId',
    TestCaseId = 'testCaseId',
    TestStepId = 'testStepId',
    TestSequenceExecutionId = 'testSequenceExecutionId',
}

export enum QueryParams {
    TestCaseIds = 'tc_id',
}

//RouteParts
export const RouteParts = {
    Inherit: '',
    Root: '/',
    Login: '/login',
    Registration: '/register',
    Products: '/products',
    Versions: '/versions',
    Variants: '/variants',
    TestSequences: '/test-sequences',
    TestCases: '/test-cases',
    Executions: '/executions',
    ExecutionProtocols: '/execution-protocols',
    Account: '/account',
    EmailTemplates: '/email-templates',
    EmailConfigs: '/email-configs',
    ForgotPassword: '/forgot-password',
    ResetPassword: '/reset-password',
    Activation: '/activate',
    About: '/about',
    Privacy: '/privacy',
    Imprint: '/imprint',
};

/** These Routes are only meant to be used by the router routes definition and functions defined inside routes that replace placeholders */
//TODO: define all Routes as variables which allows building Paths in a DRY way `${SingleProduct}/${SingleVersion}`...
export const RouterRoutes = {
    Inherit: RouteParts.Inherit,
    Root: RouteParts.Root,
    Login: RouteParts.Login,
    Registration: RouteParts.Registration,
    Products: RouteParts.Products,
    Account: RouteParts.Account,
    SingleProduct: `${RouteParts.Products}/:${PathParams.ProductId}`,
    NewSingleProduct: `${RouteParts.Products}/v2/:${PathParams.ProductId}`,
    TestSequences: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.TestSequences}`,
    SingleTestSequence: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.TestSequences}/:${PathParams.TestSequenceId}`,
    ExecuteTestSequence: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.TestSequences}/:${PathParams.TestSequenceId}${RouteParts.Executions}`,
    SingleTestCase: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.TestSequences}/:${PathParams.TestSequenceId}${RouteParts.TestCases}/:${PathParams.TestCaseId}`,
    ExecutionProtocols: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.ExecutionProtocols}`,
    SingleExecutionProtocol: `${RouteParts.Products}/:${PathParams.ProductId}${RouteParts.Versions}/:${PathParams.VersionId}${RouteParts.Variants}/:${PathParams.VariantId}${RouteParts.TestSequences}/:${PathParams.TestSequenceId}${RouteParts.ExecutionProtocols}/:${PathParams.TestSequenceExecutionId}`,
    EmailTemplates: RouteParts.EmailTemplates,
    EmailConfigs: RouteParts.EmailConfigs,
    Forgotpassword: RouteParts.ForgotPassword,
    ResetPassword: RouteParts.ResetPassword,
    Activation: `${RouteParts.Activation}`,

    About: RouteParts.About,
    Privacy: RouteParts.Privacy,
    Imprint: RouteParts.Imprint,
};

function buildParamPlaceholder(param: PathParams): string {
    return `:${param}`;
}

//RouteNames are UIDs for vue-router and can be used for navigation
export enum RouteName {
    Root = 'Root',
    NewSingleProduct = 'NewSingleProduct',

    Protocols = 'Protocols',
}

const routes: RouteRecordRaw[] = [
    {
        path: RouterRoutes.Root,
        component: () => import('src/layouts/BaseLayout.vue'),
        children: [
            {
                path: RouterRoutes.Inherit,
                component: () =>
                    import('src/pages/BasePageWithErrorHandling.vue'),
                redirect: () => RouterRoutes.Login,
                children: [
                    {
                        path: RouterRoutes.Login,
                        component: () => import('src/pages/LoginPage.vue'),
                    },
                    {
                      path: RouterRoutes.Registration,
                      component: () => import('src/pages/RegistrationPage.vue'),
                    },
                    {
                        path: RouterRoutes.Account,
                        component: () => import('src/pages/AccountPage.vue'),
                    },
                    {
                        path: RouterRoutes.Activation,
                        component: () => import('src/pages/ActivationPage.vue'),
                    },
                    {
                        path: RouterRoutes.EmailTemplates,
                        component: () => import('src/pages/EmailTemplatesPage.vue')
                    },
                    {
                        path: RouterRoutes.EmailConfigs,
                        component: () => import('src/pages/EmailConfigsPage.vue')
                    },
                    {
                        path: RouterRoutes.Forgotpassword,
                        component: () => import('src/pages/ForgotPasswordPage.vue')
                    },
                    {
                        path: RouterRoutes.ResetPassword,
                        component: () => import('src/pages/ResetPasswordPage.vue'),
                    },
                    {
                        path: RouterRoutes.Products,
                        component: () => import('src/pages/ProductsPage.vue'),
                    },
                    {
                        path: RouterRoutes.SingleProduct,
                        component: () => import('src/pages/ProductPage.vue'),
                    },
                    {
                        path: RouterRoutes.TestSequences,
                        component: () =>
                            import('src/pages/TestSequencesPage.vue'),
                    },
                    {
                        path: RouterRoutes.SingleTestSequence,
                        component: () =>
                            import('src/pages/TestSequencePage.vue'),
                    },
                    {
                        path: RouterRoutes.SingleTestCase,
                        component: () => import('src/pages/TestCasePage.vue'),
                    },
                    {
                        name: RouteName.NewSingleProduct,
                        path: RouterRoutes.NewSingleProduct,
                        component: () => import('src/pages/NewProductPage.vue'),
                        /* TODO: using the tabs with pages and also changing the url requires further thinking
            children: [
              {
                name: RouteName.Protocols,
                path: Routes.ExecutionProtocols,
                component: () => import('src/pages/ProtocolsPage.vue') },
            ]
            */
                    },
                    {
                        path: RouterRoutes.ExecuteTestSequence,
                        component: () => import('src/pages/ExecutionPage.vue'),
                    },
                    {
                        path: RouterRoutes.ExecutionProtocols,
                        component: () => import('src/pages/ProtocolsPage.vue'),
                    },
                    {
                        path: RouterRoutes.SingleExecutionProtocol,
                        component: () => import('src/pages/ProtocolPage.vue'),
                    },
                ],
            },
        ],
    },

    // Always leave this as last one,
    {
        path: '/:catchAll(.*)*',
        component: () => import('src/pages/ErrorNotFoundPage.vue'),
    },
];

export default routes;

// link builders

export function toProducts(): string {
    return RouterRoutes.Products;
}

export function toLogin(): string {
  return RouterRoutes.Login;
}

export function toSingleProduct(productId: number): string {
    return RouterRoutes.SingleProduct.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    );
}

export function toAlternativeSingleProduct(productId: number): string {
    return RouterRoutes.NewSingleProduct.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    );
}

export function toTestSequencesOfVariant(
    productId: number,
    versionId: number,
    variantId: number
): string {
    return RouterRoutes.TestSequences.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        );
}

export function toSingleTestSequence(
    productId: number,
    versionId: number,
    variantId: number,
    testSequenceId: number
): string {
    return RouterRoutes.SingleTestSequence.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestSequenceId),
            testSequenceId.toString()
        );
}

export function toSingleTestCase(
    productId: number,
    versionId: number,
    variantId: number,
    testSequenceId: number,
    testCaseId: number
): string {
    return RouterRoutes.SingleTestCase.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestSequenceId),
            testSequenceId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestCaseId),
            testCaseId.toString()
        );
}

export function toExecution(
    productId: number,
    versionId: number,
    variantId: number,
    testSeqId: number
): string {
    return RouterRoutes.ExecuteTestSequence.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestSequenceId),
            testSeqId.toString()
        );
}

export function toExecutionProtocolsOfVariant(
    productId: number,
    versionId: number,
    variantId: number
): string {
    return RouterRoutes.ExecutionProtocols.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        );
}

export function toSingleExecutionProtocol(
    productId: number,
    versionId: number,
    variantId: number,
    testSequenceId: number,
    testSequenceExecutionId: number
): string {
    return RouterRoutes.SingleExecutionProtocol.replace(
        buildParamPlaceholder(PathParams.ProductId),
        productId.toString()
    )
        .replace(
            buildParamPlaceholder(PathParams.VersionId),
            versionId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.VariantId),
            variantId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestSequenceId),
            testSequenceId.toString()
        )
        .replace(
            buildParamPlaceholder(PathParams.TestSequenceExecutionId),
            testSequenceExecutionId.toString()
        );
}
