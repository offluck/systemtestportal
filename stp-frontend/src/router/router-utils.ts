import { Router } from 'vue-router';
import { QueryParams, RouteParts, toExecution } from './routes';

export type OwnRouteLocationRaw = {
    path: string;
    query: Record<string, string | number | (string | number)[]>;
};

export async function goToExecution(
    router: Router,
    productId: number,
    versionId: number,
    variantId: number,
    testSeqId: number,
    testCaseIdsToExecute: number[]
) {
    const routeLocationRaw: OwnRouteLocationRaw = {
        path: toExecution(productId, versionId, variantId, testSeqId),
        query: {
            [QueryParams.TestCaseIds]: testCaseIdsToExecute,
        },
    };
    await router.push(routeLocationRaw);
}

export const nonAuthRoutes = [
    RouteParts.About,
    RouteParts.Privacy,
    RouteParts.Imprint,
    RouteParts.Login,
    RouteParts.Registration,
    RouteParts.Activation,
    RouteParts.ForgotPassword,
    RouteParts.ResetPassword,
];
