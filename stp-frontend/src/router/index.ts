import { route } from 'quasar/wrappers';
import { useErrorStateStore } from 'src/stores/error-state-store';
import {
    createMemoryHistory,
    createRouter,
    createWebHashHistory,
    createWebHistory,
} from 'vue-router';

import routes, { RouteParts } from './routes';
import { useAuthenticationStore } from 'src/stores/authentication-store';
import { notifyError } from 'src/utils/general-utils';
import { nonAuthRoutes } from './router-utils';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (
    {
        /*store, ssrContext */
    }
) {
    const createHistory = process.env.SERVER
        ? createMemoryHistory
        : process.env.VUE_ROUTER_MODE === 'history'
        ? createWebHistory
        : createWebHashHistory;

    const Router = createRouter({
        scrollBehavior: () => ({ left: 0, top: 0 }),
        routes,

        // Leave this as is and make changes in quasar.conf.js instead!
        // quasar.conf.js -> build -> vueRouterMode
        // quasar.conf.js -> build -> publicPath
        history: createHistory(process.env.VUE_ROUTER_BASE),
    });

    const authenticationStore = useAuthenticationStore();

    Router.beforeEach(async (to, from) => {
        if (
            authenticationStore.authenticationFail &&
            from.path === RouteParts.Login
        ) {
            authenticationStore.authenticationFail = false;
            return authenticationStore.lastPage;
        }

        if (!nonAuthRoutes.includes(to.path)) {
            if (authenticationStore.isAuthenticated()) {
                authenticationStore.lastPage = to.path;
            } else {
                notifyError('Your currently not logged in.');
                await authenticationStore.logoutUser();
                return RouteParts.Login;
            }
        }
    });
    Router.afterEach((_to, _from, _failure) => {
        //the error state that is used to display BasePageWithErrorHandling.vue error slot
        //has to be resetted after leaving a route, otherwise it will cause another error to bubble up (onVnodeUnmounted)
        //this is likely a bug with Vue Suspense or Vue-Router, there exist similar Issues and PR's on Github
        /* First stack trace lines of error:
      runtime-core.esm-bundler.js:40 [Vue warn]: Unhandled error during execution of setup function
      at <ExecutionPage onVnodeUnmounted=fn<onVnodeUnmounted> ref=Ref< null > >
    */
        const errorStateStore = useErrorStateStore();
        errorStateStore.resetError();
    });

    return Router;
});
