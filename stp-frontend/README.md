# STP-Frontend (stp-frontend)

## Install nvm (Node Version Manager)

```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```

Add the following to your ~/.bashrc or ~/.zshrc to make nvm available as a command in your system

```shell
# NVM Command Path
 export NVM_DIR=~/.nvm
 [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
```

Open a new Terminal and install node 18.13.0 (LTS)

```shell
nvm install 18.13.0
node -v # should be v18.13.0
```

## .nvmrc file

The repo should contain a .nvmrc file that contains the node version to use in this repo.
Execute the following to use the specified node version:

```shell
nvm use
```

## .env file

optional envs

```
HOST=<BACKEND_HOST> //default localhost
HOST_PORT=<BACKEND_HOST_PORT> //default 8080
```

## Install Quasar-CLI globally

```shell
npm install -g @quasar/cli@2.0.0
```

## Quasar CLI

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Build for production

Build will only succeed if no errors are in the project (project-itself or eslint)

```shell
# build SPA
quasar build -m spa
# path to the folder that contains the build
quasar serve dist/spa/
```

Frontend for SystemTestPortal-Application.

## Lint the files

```bash
yarn lint
# or
npm run lint
```

## Format the files

```bash
yarn format
# or
npm run format
```

## Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
