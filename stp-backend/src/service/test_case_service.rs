use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_case_dto::{TestCasePostDto, TestCaseUpdateDto},
    entity::{prelude::TestCase, test_case},
};

pub async fn find_all_test_cases(
    db_conn: &DatabaseConnection,
    test_sequence_id: Option<i32>,
) -> Result<Vec<test_case::Model>, SelectError> {
    let test_cases =
        TestCase::find()
            .filter(Condition::all().add_option(
                test_sequence_id.map(|ts_id| test_case::Column::TestSequenceId.eq(ts_id)),
            ))
            .all(db_conn)
            .await;

    transform_select_all_result(test_cases)
}

pub async fn find_single_test_case(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
) -> Result<test_case::Model, SelectError> {
    let found_test_case = TestCase::find_by_id(test_case_id).one(db_conn).await;

    transform_select_single_result(found_test_case)
}

pub async fn save_test_case(
    db_conn: &DatabaseConnection,
    new_test_case: TestCasePostDto,
) -> Result<test_case::Model, CreateError> {
    let test_case_to_save = test_case::ActiveModel::from(new_test_case);

    let saved_test_case = test_case_to_save.insert(db_conn).await;

    transform_create_result(saved_test_case)
}

pub async fn update_test_case(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
    updated_test_case: TestCaseUpdateDto,
) -> Result<test_case::Model, UpdateError> {
    let found_test_case = TestCase::find_by_id(test_case_id).one(db_conn).await;

    let test_case_to_update = transform_to_be_updated_result(found_test_case);

    match test_case_to_update {
        Ok(found_test_case) => {
            let mut test_case_to_update: test_case::ActiveModel = found_test_case.into();
            test_case_to_update.name = Set(updated_test_case.name);
            test_case_to_update.description = Set(updated_test_case.description);
            test_case_to_update.precondition = Set(updated_test_case.precondition);
            test_case_to_update.test_sequence_index = Set(updated_test_case.test_sequence_index);
            test_case_to_update.test_sequence_id = Set(updated_test_case.test_sequence_id);

            let updated_test_case = test_case_to_update.update(db_conn).await;
            transform_update_result(updated_test_case)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_case(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestCase::delete_by_id(test_case_id).exec(db_conn).await;

    transform_delete_result(delete_result)
}
