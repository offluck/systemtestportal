use actix_web::{
    cookie::{time::Duration as ActixWebDuration, Cookie},
    dev::Payload,
    error::ErrorUnauthorized,
    http, web, Error as ActixWebError, FromRequest, HttpRequest,
};
use chrono::{prelude::Utc, Duration};
use core::fmt;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};
use std::future::{ready, Ready};

use crate::AppState;

pub const JWT_COOKIE_NAME: &str = "token";

#[derive(Debug, Serialize)]
struct ErrorResponse {
    status: String,
    message: String,
}

impl fmt::Display for ErrorResponse {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}

#[derive(Debug)]
pub struct JwtMiddleware {
    pub user_id: i32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
    pub id: i32,
    pub iat: usize,
    pub exp: usize,
}

impl FromRequest for JwtMiddleware {
    type Error = ActixWebError;
    type Future = Ready<Result<Self, Self::Error>>;
    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let jwt_secret = &req
            .app_data::<web::Data<AppState>>()
            .unwrap()
            .app_config
            .jwt_secret;

        let token = req
            .cookie(JWT_COOKIE_NAME)
            .map(|c| c.value().to_string())
            .or_else(|| {
                req.headers()
                    .get(http::header::AUTHORIZATION)
                    .map(|h| h.to_str().unwrap().split_at(7).1.to_string())
            });

        if token.is_none() {
            let json_error = ErrorResponse {
                status: "fail".to_string(),
                message: "You are not logged in, please provide token".to_string(),
            };
            return ready(Err(ErrorUnauthorized(json_error)));
        }

        let claims = match decode::<TokenClaims>(
            &token.unwrap(),
            &DecodingKey::from_secret(jwt_secret.as_bytes()),
            &Validation::default(),
        ) {
            Ok(c) => c.claims,
            Err(_) => {
                let json_error = ErrorResponse {
                    status: "fail".to_string(),
                    message: "Invalid token".to_string(),
                };
                return ready(Err(ErrorUnauthorized(json_error)));
            }
        };

        ready(Ok(JwtMiddleware { user_id: claims.id }))
    }
}

fn create_jwt(id: i32, jwt_secret: &String, lifetime: i64) -> String {
    let now = Utc::now();
    let duration = Duration::hours(lifetime);

    let exp = (now + duration).timestamp() as usize;
    let iat = now.timestamp() as usize;

    let claims = TokenClaims { id, exp, iat };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(jwt_secret.as_bytes()),
    )
    .unwrap()
}

pub fn create_cookie(cookie_name: &str, content: String, lifetime_hours: i64) -> Cookie<'_> {
    Cookie::build(cookie_name, content)
        .path("/")
        .max_age(ActixWebDuration::hours(lifetime_hours))
        .http_only(true)
        .finish()
}

pub fn create_jwt_cookie(id: i32, jwt_secret: &String, lifetime_hours: i64) -> Cookie<'_> {
    let token = create_jwt(id, jwt_secret, lifetime_hours);
    create_cookie(JWT_COOKIE_NAME, token, lifetime_hours)
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{dev::Payload, test::TestRequest, Error};
    use config::AppConfig;
    use sea_orm::DatabaseConnection;

    use crate::config;
    use crate::service::jwt_auth_service::{create_jwt, JwtMiddleware};
    use crate::AppState;

    const DUMMY_ID: i32 = 40;
    const DUMMY_JWT_SECRET: &str = "jwt_test_secret";

    fn create_mock_app_data() -> AppState {
        AppState {
            app_config: AppConfig {
                jwt_secret: DUMMY_JWT_SECRET.to_string(),
                frontend_url: "".to_string(),
                db_url: "".to_string(),
                host: "".to_string(),
                port: "".to_string(),
                server_url: "".to_string(),
                pepper: "".to_string(),
                token_expiration: 123,
            },
            db_conn: DatabaseConnection::Disconnected,
        }
    }

    fn create_mock_request_with_cookie(token: String, lifetime_hours: i64) -> HttpRequest {
        let cookie = create_cookie(JWT_COOKIE_NAME, token, lifetime_hours);

        TestRequest::default()
            .data(create_mock_app_data())
            .cookie(cookie)
            .to_http_request()
    }

    fn get_test_jwt(lifetime: i64) -> String {
        create_jwt(DUMMY_ID, &DUMMY_JWT_SECRET.to_string(), lifetime)
    }

    #[actix_web::test]
    async fn test_valid_token_from_cookie() -> Result<(), Error> {
        let token = get_test_jwt(1);
        let req = create_mock_request_with_cookie(token, 1);

        let result = JwtMiddleware::from_request(&req, &mut Payload::None).await;

        assert!(result.is_ok());
        let middleware = result.unwrap();
        assert_eq!(middleware.user_id, DUMMY_ID);

        Ok(())
    }

    #[actix_web::test]
    async fn test_missing_token() -> Result<(), Error> {
        let req = TestRequest::default()
            .data(create_mock_app_data())
            .to_http_request();

        let result = JwtMiddleware::from_request(&req, &mut Payload::None).await;

        assert!(result.is_err());
        let err = result.unwrap_err();
        assert_eq!(
            err.error_response().status(),
            http::StatusCode::UNAUTHORIZED
        );

        Ok(())
    }

    #[actix_web::test]
    async fn test_invalid_token() -> Result<(), Error> {
        let lifetime = -2;
        let token = get_test_jwt(lifetime);
        let req = create_mock_request_with_cookie(token, lifetime);

        let result = JwtMiddleware::from_request(&req, &mut Payload::None).await;

        assert!(result.is_err());
        let err = result.unwrap_err();
        assert_eq!(
            err.error_response().status(),
            http::StatusCode::UNAUTHORIZED
        );

        Ok(())
    }

    #[test]
    fn test_create_jwt() {
        let id = 42;
        let lifetime = 1;
        let secret = &DUMMY_JWT_SECRET.to_string();

        let token = create_jwt(id, secret, lifetime);

        let token_data = decode::<TokenClaims>(
            &token,
            &DecodingKey::from_secret(secret.as_bytes()),
            &Validation::default(),
        );

        assert!(token_data.is_ok());
        let decoded_claims = token_data.unwrap().claims;

        assert_eq!(decoded_claims.id, id);
    }

    #[test]
    fn test_create_cookie() {
        let cookie_name = JWT_COOKIE_NAME;
        let content = "cookie_content".to_string();
        let lifetime_hours = 24;

        let result = create_cookie(cookie_name, content.clone(), lifetime_hours);

        assert_eq!(result.name(), cookie_name);
        assert_eq!(result.value(), content);
        assert_eq!(result.path(), Some("/"));
        assert_eq!(
            result.max_age(),
            Some(ActixWebDuration::hours(lifetime_hours))
        );
        assert!(result.http_only().unwrap());
    }

    #[test]
    fn test_create_jwt_cookie() {
        let id = 42;
        let jwt_secret = &DUMMY_JWT_SECRET.to_string();
        let lifetime_hours = 24;

        let result = create_jwt_cookie(id, jwt_secret, lifetime_hours);

        assert_eq!(result.name(), JWT_COOKIE_NAME);
        assert_eq!(result.path(), Some("/"));
        assert_eq!(
            result.max_age(),
            Some(ActixWebDuration::hours(lifetime_hours))
        );
        assert!(result.http_only().unwrap());

        let token = result.value();
        let decoded_claims = decode::<TokenClaims>(
            token,
            &DecodingKey::from_secret(jwt_secret.as_bytes()),
            &Validation::default(),
        );

        assert!(decoded_claims.is_ok());
        let claims = decoded_claims.unwrap().claims;

        assert_eq!(claims.id, id);
    }
}
