use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_sequence_dto::{TestSequencePostDto, TestSequenceUpdateDto},
    entity::{prelude::TestSequence, test_sequence},
};

pub async fn find_all_test_sequences(
    db_conn: &DatabaseConnection,
    product_variant_id: Option<i32>,
) -> Result<Vec<test_sequence::Model>, SelectError> {
    let test_sequences = TestSequence::find()
        .filter(Condition::all().add_option(
            product_variant_id.map(|pv_id| test_sequence::Column::ProductVariantId.eq(pv_id)),
        ))
        .all(db_conn)
        .await;

    transform_select_all_result(test_sequences)
}

pub async fn find_single_test_sequence(
    db_conn: &DatabaseConnection,
    test_sequence_id: i32,
) -> Result<test_sequence::Model, SelectError> {
    let found_test_sequence = TestSequence::find_by_id(test_sequence_id)
        .one(db_conn)
        .await;

    transform_select_single_result(found_test_sequence)
}

pub async fn save_test_sequence(
    db_conn: &DatabaseConnection,
    new_test_sequence: TestSequencePostDto,
) -> Result<test_sequence::Model, CreateError> {
    let test_sequence_to_save = test_sequence::ActiveModel::from(new_test_sequence);

    let saved_test_sequence = test_sequence_to_save.insert(db_conn).await;

    transform_create_result(saved_test_sequence)
}

pub async fn update_test_sequence(
    db_conn: &DatabaseConnection,
    test_sequence_id: i32,
    updated_test_sequence: TestSequenceUpdateDto,
) -> Result<test_sequence::Model, UpdateError> {
    let found_test_sequence = TestSequence::find_by_id(test_sequence_id)
        .one(db_conn)
        .await;

    let test_sequence_to_update = transform_to_be_updated_result(found_test_sequence);

    match test_sequence_to_update {
        Ok(found_test_sequence) => {
            let mut test_sequence_to_update: test_sequence::ActiveModel =
                found_test_sequence.into();
            test_sequence_to_update.name = Set(updated_test_sequence.name);
            test_sequence_to_update.description = Set(updated_test_sequence.description);
            test_sequence_to_update.precondition = Set(updated_test_sequence.precondition);
            test_sequence_to_update.product_variant_id =
                Set(updated_test_sequence.product_variant_id);

            let test_sequence_after_update = test_sequence_to_update.update(db_conn).await;

            transform_update_result(test_sequence_after_update)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_sequence(
    db_conn: &DatabaseConnection,
    test_sequence_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestSequence::delete_by_id(test_sequence_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
