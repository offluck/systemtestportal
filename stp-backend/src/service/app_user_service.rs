use std::io::Error;

use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::app_user_dto::{AppUserPostDto, AppUserUpdateDto},
    entity::{app_user, prelude::AppUser},
    util::hash_string,
};

pub async fn find_all_app_users(
    db_conn: &DatabaseConnection,
    app_role_id: Option<i32>,
) -> Result<Vec<app_user::Model>, SelectError> {
    let app_users = AppUser::find()
        .filter(
            Condition::all()
                .add_option(app_role_id.map(|ar_id| app_user::Column::AppRoleId.eq(ar_id))),
        )
        .all(db_conn)
        .await;

    transform_select_all_result(app_users)
}

pub async fn find_single_app_user(
    db_conn: &DatabaseConnection,
    app_user_id: i32,
) -> Result<app_user::Model, SelectError> {
    let app_user = AppUser::find_by_id(app_user_id).one(db_conn).await;
    transform_select_single_result(app_user)
}

pub async fn find_single_app_user_by_email_and_enabled(
    db_conn: &DatabaseConnection,
    email: &str,
) -> Result<Option<app_user::Model>, Error> {
    let found_app_user = AppUser::find()
        .filter(app_user::Column::Email.eq(email))
        .filter(app_user::Column::IsEnabled.eq(true))
        .one(db_conn)
        .await
        .expect("Failed to find app user by email in the database!");

    Ok(found_app_user)
}

pub async fn save_app_user(
    db_conn: &DatabaseConnection,
    new_app_user: AppUserPostDto,
    pepper: &str,
) -> Result<app_user::Model, CreateError> {
    let new_app_user = AppUserPostDto {
        password: hash_string(new_app_user.password, pepper),
        ..new_app_user
    };
    let app_user = app_user::ActiveModel::from(new_app_user)
        .insert(db_conn)
        .await;
    transform_create_result(app_user)
}

pub async fn update_app_user(
    db_conn: &DatabaseConnection,
    app_user_id: i32,
    updated_app_user: AppUserUpdateDto,
    pepper: &str,
) -> Result<app_user::Model, UpdateError> {
    let found_app_user = AppUser::find_by_id(app_user_id).one(db_conn).await;
    let app_user_to_update = transform_to_be_updated_result(found_app_user);

    match app_user_to_update {
        Ok(found_app_user) => {
            let password = if updated_app_user.new_password.is_some() {
                hash_string(updated_app_user.new_password.unwrap(), pepper)
            } else {
                hash_string(updated_app_user.password, pepper)
            };

            let mut app_user_to_update: app_user::ActiveModel = found_app_user.into();

            app_user_to_update.name = Set(updated_app_user.name);
            app_user_to_update.password_hash = Set(password);
            app_user_to_update.biography = Set(updated_app_user.biography);
            app_user_to_update.image_path = Set(updated_app_user.image_path);
            app_user_to_update.app_role_id = Set(updated_app_user.app_role_id);

            let updated_app_user = app_user_to_update.update(db_conn).await;

            transform_update_result(updated_app_user)
        }
        Err(error) => Err(error),
    }
}

pub async fn activate_app_user(
    db_conn: &DatabaseConnection,
    app_user_id: i32,
) -> Result<app_user::Model, UpdateError> {
    let found_app_user = AppUser::find_by_id(app_user_id)
        .one(db_conn)
        .await?
        .expect("Didn't find app user when trying to activate account.");

    let mut updated_app_user: app_user::ActiveModel = found_app_user.into();
    updated_app_user.is_enabled = Set(true);
    let updated_app_user = updated_app_user.update(db_conn).await;

    transform_update_result(updated_app_user)
}

pub async fn delete_app_user(
    db_conn: &DatabaseConnection,
    app_user_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = AppUser::delete_by_id(app_user_id).exec(db_conn).await;
    transform_delete_result(delete_result)
}
