use crate::entity::prelude::EmailConfig;
use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::app_mail_config_dto::{AppEmailConfigPostDto, AppEmailConfigUpdateDto},
    entity::email_config,
};

use sea_orm::{ActiveModelTrait, DatabaseConnection, DeleteResult, EntityTrait, Set};

pub async fn find_all_email_configs(
    db_conn: &DatabaseConnection,
) -> Result<Vec<email_config::Model>, SelectError> {
    let email_configs = EmailConfig::find().all(db_conn).await;

    transform_select_all_result(email_configs)
}

pub async fn find_single_email_config(
    db_conn: &DatabaseConnection,
    email_config_id: i32,
) -> Result<email_config::Model, SelectError> {
    let email_config = EmailConfig::find_by_id(email_config_id).one(db_conn).await;

    transform_select_single_result(email_config)
}

pub async fn save_email_config(
    db_conn: &DatabaseConnection,
    new_email_config: AppEmailConfigPostDto,
) -> Result<email_config::Model, CreateError> {
    let email_config = email_config::ActiveModel::from(new_email_config)
        .insert(db_conn)
        .await;

    transform_create_result(email_config)
}

pub async fn update_email_config(
    db_conn: &DatabaseConnection,
    email_config_id: i32,
    updated_email_config: AppEmailConfigUpdateDto,
) -> Result<email_config::Model, UpdateError> {
    let email_config = EmailConfig::find_by_id(email_config_id).one(db_conn).await;

    let email_config_to_update = transform_to_be_updated_result(email_config);

    match email_config_to_update {
        Ok(found_email_config) => {
            let mut email_config_to_update: email_config::ActiveModel = found_email_config.into();
            email_config_to_update.title = Set(updated_email_config.title);
            email_config_to_update.email = Set(updated_email_config.email);
            email_config_to_update.app_password = Set(updated_email_config.app_password);
            email_config_to_update.smtp_server = Set(updated_email_config.smtp_server);

            let updated_email_config = email_config_to_update.update(db_conn).await;

            transform_update_result(updated_email_config)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_email_config(
    db_conn: &DatabaseConnection,
    email_config_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = EmailConfig::delete_by_id(email_config_id)
        .exec(db_conn)
        .await;
    transform_delete_result(delete_result)
}
