use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_step_dto::{TestStepPostDto, TestStepUpdateDto},
    entity::{prelude::TestStep, test_step},
};

pub async fn find_all_test_steps(
    db_conn: &DatabaseConnection,
    test_case_id: Option<i32>,
) -> Result<Vec<test_step::Model>, SelectError> {
    let test_steps = TestStep::find()
        .filter(
            Condition::all()
                .add_option(test_case_id.map(|tc_id| test_step::Column::TestCaseId.eq(tc_id))),
        )
        .all(db_conn)
        .await;

    transform_select_all_result(test_steps)
}

pub async fn find_single_test_step(
    db_conn: &DatabaseConnection,
    test_step_id: i32,
) -> Result<test_step::Model, SelectError> {
    let found_test_step = TestStep::find_by_id(test_step_id).one(db_conn).await;

    transform_select_single_result(found_test_step)
}

pub async fn save_test_step(
    db_conn: &DatabaseConnection,
    new_test_step: TestStepPostDto,
) -> Result<test_step::Model, CreateError> {
    let test_step_to_save = test_step::ActiveModel::from(new_test_step);

    let saved_test_step = test_step_to_save.insert(db_conn).await;

    transform_create_result(saved_test_step)
}

pub async fn update_test_step(
    db_conn: &DatabaseConnection,
    test_step_id: i32,
    updated_test_step: TestStepUpdateDto,
) -> Result<test_step::Model, UpdateError> {
    let found_test_step = TestStep::find_by_id(test_step_id).one(db_conn).await;

    let test_step_to_update = transform_to_be_updated_result(found_test_step);

    match test_step_to_update {
        Ok(found_test_step) => {
            let mut test_step_to_update: test_step::ActiveModel = found_test_step.into();
            test_step_to_update.instruction = Set(updated_test_step.instruction);
            test_step_to_update.expected_result = Set(updated_test_step.expected_result);
            test_step_to_update.test_case_index = Set(updated_test_step.test_case_index);
            test_step_to_update.test_case_id = Set(updated_test_step.test_case_id);

            let test_step_after_update = test_step_to_update.update(db_conn).await;

            transform_update_result(test_step_after_update)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_step(
    db_conn: &DatabaseConnection,
    test_step_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestStep::delete_by_id(test_step_id).exec(db_conn).await;

    transform_delete_result(delete_result)
}
