use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::email_notification_service::send_notification_to_subscribers;
use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_case_execution_dto::{TestCaseExecutionPostDto, TestCaseExecutionUpdateDto},
    entity::{prelude::TestCaseExecution, test_case_execution},
    util::get_current_datetime_with_timezone_utc,
};

pub async fn find_all_test_case_executions(
    db_conn: &DatabaseConnection,
    test_case_id: Option<i32>,
    test_sequence_execution_id: Option<i32>,
    execution_status_id: Option<i32>,
) -> Result<Vec<test_case_execution::Model>, SelectError> {
    let test_case_executions =
        TestCaseExecution::find()
            .filter(
                Condition::all()
                    .add_option(
                        test_case_id.map(|tc_id| test_case_execution::Column::TestCaseId.eq(tc_id)),
                    )
                    .add_option(test_sequence_execution_id.map(|tse_id| {
                        test_case_execution::Column::TestSequenceExecutionId.eq(tse_id)
                    }))
                    .add_option(
                        execution_status_id
                            .map(|es_id| test_case_execution::Column::ExecutionStatusId.eq(es_id)),
                    ),
            )
            .all(db_conn)
            .await;

    transform_select_all_result(test_case_executions)
}

pub async fn find_single_test_case_execution(
    db_conn: &DatabaseConnection,
    test_case_execution_id: i32,
) -> Result<test_case_execution::Model, SelectError> {
    let found_test_case_execution = TestCaseExecution::find_by_id(test_case_execution_id)
        .one(db_conn)
        .await;

    transform_select_single_result(found_test_case_execution)
}

pub async fn save_test_case_execution(
    db_conn: &DatabaseConnection,
    new_test_case_execution: TestCaseExecutionPostDto,
) -> Result<test_case_execution::Model, CreateError> {
    let test_case_execution_to_save =
        test_case_execution::ActiveModel::from(new_test_case_execution);

    let saved_test_case_execution = test_case_execution_to_save.insert(db_conn).await;

    send_notification_to_subscribers(db_conn, new_test_case_execution).await;

    transform_create_result(saved_test_case_execution)
}

pub async fn update_test_case_execution(
    db_conn: &DatabaseConnection,
    test_case_execution_id: i32,
    updated_test_case_execution: TestCaseExecutionUpdateDto,
) -> Result<test_case_execution::Model, UpdateError> {
    let found_test_case_execution = TestCaseExecution::find_by_id(test_case_execution_id)
        .one(db_conn)
        .await;

    let test_case_execution_to_update = transform_to_be_updated_result(found_test_case_execution);

    match test_case_execution_to_update {
        Ok(found_test_case_execution) => {
            let mut test_case_execution_to_update: test_case_execution::ActiveModel =
                found_test_case_execution.into();
            test_case_execution_to_update.precondition_fulfilled =
                Set(updated_test_case_execution.precondition_fulfilled);
            test_case_execution_to_update.test_case_id =
                Set(updated_test_case_execution.test_case_id);
            test_case_execution_to_update.test_sequence_execution_id =
                Set(updated_test_case_execution.test_sequence_execution_id);
            test_case_execution_to_update.execution_status_id =
                Set(updated_test_case_execution.execution_status_id);
            test_case_execution_to_update.updated_at =
                Set(get_current_datetime_with_timezone_utc());

            let test_case_execution_after_update =
                test_case_execution_to_update.update(db_conn).await;

            send_notification_to_subscribers(
                db_conn,
                TestCaseExecutionPostDto {
                    execution_status_id: updated_test_case_execution.execution_status_id,
                    test_case_id: updated_test_case_execution.test_case_id,
                    test_sequence_execution_id: updated_test_case_execution
                        .test_sequence_execution_id,
                    precondition_fulfilled: updated_test_case_execution.precondition_fulfilled,
                },
            )
            .await;

            transform_update_result(test_case_execution_after_update)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_case_execution(
    db_conn: &DatabaseConnection,
    test_case_execution_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestCaseExecution::delete_by_id(test_case_execution_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
