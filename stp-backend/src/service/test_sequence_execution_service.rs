use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_sequence_execution_dto::{
        TestSequenceExecutionPostDto, TestSequenceExecutionUpdateDto,
    },
    entity::{prelude::TestSequenceExecution, test_sequence_execution},
    util::get_current_datetime_with_timezone_utc,
};

pub async fn find_all_test_sequence_executions(
    db_conn: &DatabaseConnection,
    test_sequence_id: Option<i32>,
    tester_id: Option<i32>,
    execution_status_id: Option<i32>,
) -> Result<Vec<test_sequence_execution::Model>, SelectError> {
    let test_sequence_executions = TestSequenceExecution::find()
        .filter(
            Condition::all()
                .add_option(
                    test_sequence_id
                        .map(|ts_id| test_sequence_execution::Column::TestSequenceId.eq(ts_id)),
                )
                .add_option(
                    tester_id.map(|t_id| test_sequence_execution::Column::TesterId.eq(t_id)),
                )
                .add_option(
                    execution_status_id
                        .map(|es_id| test_sequence_execution::Column::ExecutionStatusId.eq(es_id)),
                ),
        )
        .all(db_conn)
        .await;

    transform_select_all_result(test_sequence_executions)
}

pub async fn find_single_test_sequence_execution(
    db_conn: &DatabaseConnection,
    test_sequence_execution_id: i32,
) -> Result<test_sequence_execution::Model, SelectError> {
    let found_test_sequence_execution =
        TestSequenceExecution::find_by_id(test_sequence_execution_id)
            .one(db_conn)
            .await;

    transform_select_single_result(found_test_sequence_execution)
}

pub async fn save_test_sequence_execution(
    db_conn: &DatabaseConnection,
    new_test_sequence_execution: TestSequenceExecutionPostDto,
) -> Result<test_sequence_execution::Model, CreateError> {
    let test_sequence_execution_to_save =
        test_sequence_execution::ActiveModel::from(new_test_sequence_execution);

    let saved_test_sequence_execution = test_sequence_execution_to_save.insert(db_conn).await;

    transform_create_result(saved_test_sequence_execution)
}

pub async fn update_test_sequence_execution(
    db_conn: &DatabaseConnection,
    test_sequence_execution_id: i32,
    updated_test_sequence_execution: TestSequenceExecutionUpdateDto,
) -> Result<test_sequence_execution::Model, UpdateError> {
    let found_test_sequence_execution =
        TestSequenceExecution::find_by_id(test_sequence_execution_id)
            .one(db_conn)
            .await;

    let test_sequence_execution_to_update =
        transform_to_be_updated_result(found_test_sequence_execution);

    match test_sequence_execution_to_update {
        Ok(found_test_sequence_execution) => {
            let mut test_sequence_execution_to_update: test_sequence_execution::ActiveModel =
                found_test_sequence_execution.into();
            test_sequence_execution_to_update.precondition_fulfilled =
                Set(updated_test_sequence_execution.precondition_fulfilled);
            test_sequence_execution_to_update.test_sequence_id =
                Set(updated_test_sequence_execution.test_sequence_id);
            test_sequence_execution_to_update.tester_id =
                Set(updated_test_sequence_execution.tester_id);
            test_sequence_execution_to_update.execution_status_id =
                Set(updated_test_sequence_execution.execution_status_id);
            test_sequence_execution_to_update.updated_at =
                Set(get_current_datetime_with_timezone_utc());

            let updated_test_sequence_execution =
                test_sequence_execution_to_update.update(db_conn).await;
            transform_update_result(updated_test_sequence_execution)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_sequence_execution(
    db_conn: &DatabaseConnection,
    test_sequence_execution_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestSequenceExecution::delete_by_id(test_sequence_execution_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
