use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use sea_orm::{DbErr, DeleteResult, ModelTrait};

pub fn transform_select_all_result<M>(
    select_result: Result<Vec<M>, DbErr>,
) -> Result<Vec<M>, SelectError>
where
    M: ModelTrait,
{
    match select_result {
        Ok(products) => Ok(products),
        Err(error) => Err(SelectError::from(error)),
    }
}

pub fn transform_select_single_result<M>(model: Result<Option<M>, DbErr>) -> Result<M, SelectError>
where
    M: ModelTrait,
{
    match model {
        Ok(found_model) => found_model.ok_or(SelectError::NotFoundError),
        Err(error) => Err(SelectError::from(error)),
    }
}

pub fn transform_create_result<M>(inserted_model: Result<M, DbErr>) -> Result<M, CreateError>
where
    M: ModelTrait,
{
    match inserted_model {
        Ok(model) => Ok(model),
        Err(error) => Err(CreateError::from(error)),
    }
}

pub fn transform_to_be_updated_result<M>(
    to_be_updated_result: Result<Option<M>, DbErr>,
) -> Result<M, UpdateError>
where
    M: ModelTrait,
{
    match to_be_updated_result {
        Ok(found_model) => match found_model {
            None => Err(UpdateError::NotFound),
            Some(model) => Ok(model),
        },
        Err(error) => Err(UpdateError::from(error)),
    }
}

pub fn transform_update_result<M>(update_result: Result<M, DbErr>) -> Result<M, UpdateError>
where
    M: ModelTrait,
{
    match update_result {
        Ok(product_after_update) => Ok(product_after_update),
        Err(error) => Err(UpdateError::from(error)),
    }
}

pub fn transform_delete_result(
    db_delete_result: Result<DeleteResult, DbErr>,
) -> Result<DeleteResult, DeleteError> {
    match db_delete_result {
        Ok(result) => match result.rows_affected {
            0 => Err(DeleteError::NotFoundError),
            1 => Ok(result),
            _ => Err(DeleteError::DeletedMoreThanOne),
        },
        Err(error) => Err(DeleteError::from(error)),
    }
}
