use sea_orm::{ActiveEnum, ActiveModelTrait, DatabaseConnection, DeleteResult, EntityTrait, Set};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::app_role_dto::{AppRolePostDto, AppRoleUpdateDto},
    entity::{app_role, prelude::AppRole},
};

pub async fn find_all_app_roles(
    db_conn: &DatabaseConnection,
) -> Result<Vec<app_role::Model>, SelectError> {
    let app_roles = AppRole::find().all(db_conn).await;
    transform_select_all_result(app_roles)
}

pub async fn find_single_app_role(
    db_conn: &DatabaseConnection,
    app_role_id: i32,
) -> Result<app_role::Model, SelectError> {
    let app_role = AppRole::find_by_id(app_role_id).one(db_conn).await;
    transform_select_single_result(app_role)
}

pub async fn save_app_role(
    db_conn: &DatabaseConnection,
    new_app_role: AppRolePostDto,
) -> Result<app_role::Model, CreateError> {
    let app_role = app_role::ActiveModel::from(new_app_role)
        .insert(db_conn)
        .await;
    transform_create_result(app_role)
}

pub async fn update_app_role(
    db_conn: &DatabaseConnection,
    app_role_id: i32,
    updated_app_role: AppRoleUpdateDto,
) -> Result<app_role::Model, UpdateError> {
    let found_app_role = AppRole::find_by_id(app_role_id).one(db_conn).await;
    let app_role_to_update = transform_to_be_updated_result(found_app_role);

    match app_role_to_update {
        Ok(found_app_role) => {
            let mut app_role_to_update: app_role::ActiveModel = found_app_role.into();
            app_role_to_update.name = Set(updated_app_role.name.to_value());

            let updated_app_role = app_role_to_update.update(db_conn).await;

            transform_update_result(updated_app_role)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_app_role(
    db_conn: &DatabaseConnection,
    app_role_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = AppRole::delete_by_id(app_role_id).exec(db_conn).await;
    transform_delete_result(delete_result)
}
