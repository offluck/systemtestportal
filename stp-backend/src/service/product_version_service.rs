use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::product_version_dto::{ProductVersionPostDto, ProductVersionUpdateDto},
    entity::{prelude::ProductVersion, product_version},
};

pub async fn find_all_product_versions(
    db_conn: &DatabaseConnection,
    product_id: Option<i32>,
) -> Result<Vec<product_version::Model>, SelectError> {
    let product_versions = ProductVersion::find()
        // filter results by (optional) query param
        .filter(
            Condition::all()
                .add_option(product_id.map(|p_id| product_version::Column::ProductId.eq(p_id))),
        )
        .all(db_conn)
        .await;

    transform_select_all_result(product_versions)
}

pub async fn find_single_product_version(
    db_conn: &DatabaseConnection,
    product_version_id: i32,
) -> Result<product_version::Model, SelectError> {
    let found_product_version = ProductVersion::find_by_id(product_version_id)
        .one(db_conn)
        .await;

    transform_select_single_result(found_product_version)
}

pub async fn save_product_version(
    db_conn: &DatabaseConnection,
    new_product_version: ProductVersionPostDto,
) -> Result<product_version::Model, CreateError> {
    let product_version_to_save = product_version::ActiveModel::from(new_product_version);

    let saved_product_version = product_version_to_save.insert(db_conn).await;

    transform_create_result(saved_product_version)
}

pub async fn update_product_version(
    db_conn: &DatabaseConnection,
    product_version_id: i32,
    updated_product_version: ProductVersionUpdateDto,
) -> Result<product_version::Model, UpdateError> {
    let found_product_version = ProductVersion::find_by_id(product_version_id)
        .one(db_conn)
        .await;

    let product_version_to_update = transform_to_be_updated_result(found_product_version);

    match product_version_to_update {
        Ok(found_product_version) => {
            let mut product_version_to_update: product_version::ActiveModel =
                found_product_version.into();
            product_version_to_update.name = Set(updated_product_version.name);
            product_version_to_update.product_id = Set(updated_product_version.product_id);

            let updated_product_version = product_version_to_update.update(db_conn).await;

            transform_update_result(updated_product_version)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_product_version(
    db_conn: &DatabaseConnection,
    product_version_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = ProductVersion::delete_by_id(product_version_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
