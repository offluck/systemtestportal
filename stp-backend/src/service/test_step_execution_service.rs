use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::test_step_execution_dto::{TestStepExecutionPostDto, TestStepExecutionUpdateDto},
    entity::{prelude::TestStepExecution, test_step_execution},
    util::get_current_datetime_with_timezone_utc,
};

pub async fn find_all_test_step_executions(
    db_conn: &DatabaseConnection,
    test_step_id: Option<i32>,
    test_case_execution_id: Option<i32>,
    execution_status_id: Option<i32>,
) -> Result<Vec<test_step_execution::Model>, SelectError> {
    let test_step_executions = TestStepExecution::find()
        .filter(
            Condition::all()
                .add_option(
                    test_step_id.map(|ts_id| test_step_execution::Column::TestStepId.eq(ts_id)),
                )
                .add_option(
                    test_case_execution_id
                        .map(|tce_id| test_step_execution::Column::TestCaseExecutionId.eq(tce_id)),
                )
                .add_option(
                    execution_status_id
                        .map(|es_id| test_step_execution::Column::ExecutionStatusId.eq(es_id)),
                ),
        )
        .all(db_conn)
        .await;

    transform_select_all_result(test_step_executions)
}

pub async fn find_single_test_step_execution(
    db_conn: &DatabaseConnection,
    test_step_execution_id: i32,
) -> Result<test_step_execution::Model, SelectError> {
    let found_test_step_execution = TestStepExecution::find_by_id(test_step_execution_id)
        .one(db_conn)
        .await;

    transform_select_single_result(found_test_step_execution)
}

pub async fn save_test_step_execution(
    db_conn: &DatabaseConnection,
    new_test_step_execution: TestStepExecutionPostDto,
) -> Result<test_step_execution::Model, CreateError> {
    let test_step_execution_to_save =
        test_step_execution::ActiveModel::from(new_test_step_execution);

    let saved_test_step_execution = test_step_execution_to_save.insert(db_conn).await;

    transform_create_result(saved_test_step_execution)
}

pub async fn update_test_step_execution(
    db_conn: &DatabaseConnection,
    test_step_execution_id: i32,
    updated_test_step_execution: TestStepExecutionUpdateDto,
) -> Result<test_step_execution::Model, UpdateError> {
    let found_test_step_execution = TestStepExecution::find_by_id(test_step_execution_id)
        .one(db_conn)
        .await;

    let test_step_execution_to_update = transform_to_be_updated_result(found_test_step_execution);

    match test_step_execution_to_update {
        Ok(found_test_step_execution) => {
            let mut test_step_execution_to_update: test_step_execution::ActiveModel =
                found_test_step_execution.into();
            test_step_execution_to_update.comment = Set(updated_test_step_execution.comment);
            test_step_execution_to_update.test_step_id =
                Set(updated_test_step_execution.test_step_id);
            test_step_execution_to_update.test_case_execution_id =
                Set(updated_test_step_execution.test_case_execution_id);
            test_step_execution_to_update.execution_status_id =
                Set(updated_test_step_execution.execution_status_id);
            test_step_execution_to_update.updated_at =
                Set(get_current_datetime_with_timezone_utc());

            let updated_test_step_execution = test_step_execution_to_update.update(db_conn).await;
            transform_update_result(updated_test_step_execution)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_test_step_execution(
    db_conn: &DatabaseConnection,
    test_step_execution_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = TestStepExecution::delete_by_id(test_step_execution_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
