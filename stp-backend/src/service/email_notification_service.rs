use crate::dto::app_user_dto::SubscribedUserGetDto;
use crate::dto::test_case_execution_dto::TestCaseExecutionPostDto;
use crate::email::email_service::send;
use crate::email::email_type::{EmailType, ExecutionNotificationData};
use crate::entity::app_user::Model;
use crate::entity::execution_status;
use crate::service::error::select_error::SelectError;
use crate::service::execution_status_service::find_single_execution_status;
use crate::service::test_case_subscription_service::find_all_test_case_subscriptions;
use log::debug;
use sea_orm::DatabaseConnection;

pub async fn send_notification_to_subscribers(
    db_conn: &DatabaseConnection,
    test_case_execution: TestCaseExecutionPostDto,
) {
    let execution_data =
        should_send_notification(db_conn, test_case_execution.execution_status_id).await;

    if let Err(err) = execution_data {
        debug!("{}", err);
        // TODO: handle error
        return;
    }

    let execution_result = execution_data.unwrap();

    let subscriber_data = get_subscribed_users(db_conn, test_case_execution.test_case_id).await;

    if let Err(err) = subscriber_data {
        debug!("{}", err);
        //TODO: handle error
        return;
    }

    let subscribers = subscriber_data.unwrap();

    if execution_result.send_notification {
        for user_dto in subscribers.into_iter().map(SubscribedUserGetDto::from) {
            let execution_notification_data = ExecutionNotificationData {
                user_recipient: user_dto.name.clone(),
                test_id: test_case_execution.test_case_id,
                execution_date: execution_result.name.clone(),
            };

            send(
                EmailType::ExecutionNotification(execution_notification_data),
                user_dto.email,
                db_conn,
            )
            .await;
        }
    }
}

async fn get_subscribed_users(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
) -> Result<Vec<Model>, SelectError> {
    find_all_test_case_subscriptions(db_conn, test_case_id, None).await
}

async fn should_send_notification(
    db_conn: &DatabaseConnection,
    execution_status_id: i32,
) -> Result<execution_status::Model, SelectError> {
    find_single_execution_status(db_conn, execution_status_id).await
}
