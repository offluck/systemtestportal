use crate::entity::prelude::EmailTemplate;
use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::app_mail_template_dto::{EmailTemplatePostDto, EmailTemplatePutDto},
    entity::email_template,
};

use sea_orm::{ActiveModelTrait, DatabaseConnection, DeleteResult, EntityTrait, Set};

pub async fn find_all_email_templates(
    db_conn: &DatabaseConnection,
) -> Result<Vec<email_template::Model>, SelectError> {
    let email_templates = EmailTemplate::find().all(db_conn).await;

    transform_select_all_result(email_templates)
}

pub async fn find_single_email_template(
    db_conn: &DatabaseConnection,
    email_template_id: i32,
) -> Result<email_template::Model, SelectError> {
    let email_template = EmailTemplate::find_by_id(email_template_id)
        .one(db_conn)
        .await;

    transform_select_single_result(email_template)
}

pub async fn save_email_template(
    db_conn: &DatabaseConnection,
    new_email_template: EmailTemplatePostDto,
) -> Result<email_template::Model, CreateError> {
    let email_template = email_template::ActiveModel::from(new_email_template)
        .insert(db_conn)
        .await;

    transform_create_result(email_template)
}

pub async fn update_email_template(
    db_conn: &DatabaseConnection,
    email_template_id: i32,
    updated_email_template: EmailTemplatePutDto,
) -> Result<email_template::Model, UpdateError> {
    let found_app_mail_template = EmailTemplate::find_by_id(email_template_id)
        .one(db_conn)
        .await;

    let email_template_to_update = transform_to_be_updated_result(found_app_mail_template);

    match email_template_to_update {
        Ok(found_email_template) => {
            let mut email_template_to_update: email_template::ActiveModel =
                found_email_template.into();
            email_template_to_update.name = Set(updated_email_template.name);
            email_template_to_update.content = Set(updated_email_template.content);
            email_template_to_update.email_config_id = Set(updated_email_template.email_config_id);

            let updated_email_template = email_template_to_update.update(db_conn).await;
            transform_update_result(updated_email_template)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_email_template(
    db_conn: &DatabaseConnection,
    email_template_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = EmailTemplate::delete_by_id(email_template_id)
        .exec(db_conn)
        .await;
    transform_delete_result(delete_result)
}
