use sea_orm::{ActiveEnum, ActiveModelTrait, DatabaseConnection, DeleteResult, EntityTrait, Set};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::execution_status_dto::{ExecutionStatusPostDto, ExecutionStatusUpdateDto},
    entity::{execution_status, prelude::ExecutionStatus},
};

pub async fn find_all_execution_statuses(
    db_conn: &DatabaseConnection,
) -> Result<Vec<execution_status::Model>, SelectError> {
    let execution_status = ExecutionStatus::find().all(db_conn).await;
    transform_select_all_result(execution_status)
}

pub async fn find_single_execution_status(
    db_conn: &DatabaseConnection,
    execution_status_id: i32,
) -> Result<execution_status::Model, SelectError> {
    let execution_status = ExecutionStatus::find_by_id(execution_status_id)
        .one(db_conn)
        .await;
    transform_select_single_result(execution_status)
}

pub async fn save_execution_status(
    db_conn: &DatabaseConnection,
    new_execution_status: ExecutionStatusPostDto,
) -> Result<execution_status::Model, CreateError> {
    let execution_status = execution_status::ActiveModel::from(new_execution_status)
        .insert(db_conn)
        .await;
    transform_create_result(execution_status)
}

pub async fn update_execution_status(
    db_conn: &DatabaseConnection,
    execution_status_id: i32,
    updated_execution_status: ExecutionStatusUpdateDto,
) -> Result<execution_status::Model, UpdateError> {
    let found_execution_status = ExecutionStatus::find_by_id(execution_status_id)
        .one(db_conn)
        .await;
    let execution_status_to_update = transform_to_be_updated_result(found_execution_status);

    match execution_status_to_update {
        Ok(found_execution_status) => {
            let mut execution_status_to_update: execution_status::ActiveModel =
                found_execution_status.into();
            execution_status_to_update.name = Set(updated_execution_status.name.to_value());

            let updated_execution_status = execution_status_to_update.update(db_conn).await;

            transform_update_result(updated_execution_status)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_execution_status(
    db_conn: &DatabaseConnection,
    execution_status_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = ExecutionStatus::delete_by_id(execution_status_id)
        .exec(db_conn)
        .await;
    transform_delete_result(delete_result)
}
