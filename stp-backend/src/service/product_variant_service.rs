use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::product_variant_dto::{ProductVariantPostDto, ProductVariantUpdateDto},
    entity::{prelude::ProductVariant, product_variant},
};

pub async fn find_all_product_variants(
    db_conn: &DatabaseConnection,
    product_version_id: Option<i32>,
) -> Result<Vec<product_variant::Model>, SelectError> {
    let product_variants = ProductVariant::find()
        .filter(Condition::all().add_option(
            product_version_id.map(|pv_id| product_variant::Column::ProductVersionId.eq(pv_id)),
        ))
        .all(db_conn)
        .await;

    transform_select_all_result(product_variants)
}

pub async fn find_single_product_variant(
    db_conn: &DatabaseConnection,
    product_variant_id: i32,
) -> Result<product_variant::Model, SelectError> {
    let found_product_variant = ProductVariant::find_by_id(product_variant_id)
        .one(db_conn)
        .await;

    transform_select_single_result(found_product_variant)
}

pub async fn save_product_variant(
    db_conn: &DatabaseConnection,
    new_product_variant: ProductVariantPostDto,
) -> Result<product_variant::Model, CreateError> {
    let product_variant_to_save = product_variant::ActiveModel::from(new_product_variant);

    let saved_product_variant = product_variant_to_save.insert(db_conn).await;

    transform_create_result(saved_product_variant)
}

pub async fn update_product_variant(
    db_conn: &DatabaseConnection,
    product_variant_id: i32,
    updated_product_variant: ProductVariantUpdateDto,
) -> Result<product_variant::Model, UpdateError> {
    let found_product_variant = ProductVariant::find_by_id(product_variant_id)
        .one(db_conn)
        .await;

    let product_variant_to_update = transform_to_be_updated_result(found_product_variant);

    match product_variant_to_update {
        Ok(found_product_variant) => {
            let mut product_variant_to_update: product_variant::ActiveModel =
                found_product_variant.into();
            product_variant_to_update.name = Set(updated_product_variant.name);
            product_variant_to_update.product_version_id =
                Set(updated_product_variant.product_version_id);

            let updated_product_variant = product_variant_to_update.update(db_conn).await;
            transform_update_result(updated_product_variant)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_product_variant(
    db_conn: &DatabaseConnection,
    product_variant_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = ProductVariant::delete_by_id(product_variant_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
