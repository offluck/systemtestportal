use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, Set,
};

use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::execution_environment_info_dto::{
        ExecutionEnvironmentInfoPostDto, ExecutionEnvironmentInfoUpdateDto,
    },
    entity::{execution_environment_info, prelude::ExecutionEnvironmentInfo},
    util::get_current_datetime_with_timezone_utc,
};

pub async fn find_all_execution_environment_infos(
    db_conn: &DatabaseConnection,
    test_sequence_execution_id: Option<i32>,
) -> Result<Vec<execution_environment_info::Model>, SelectError> {
    let execution_environment_infos = ExecutionEnvironmentInfo::find()
        .filter(Condition::all().add_option(
            test_sequence_execution_id.map(|tse_id| {
                execution_environment_info::Column::TestSequenceExecutionId.eq(tse_id)
            }),
        ))
        .all(db_conn)
        .await;

    transform_select_all_result(execution_environment_infos)
}

pub async fn find_single_execution_environment_info(
    db_conn: &DatabaseConnection,
    execution_environment_info_id: i32,
) -> Result<execution_environment_info::Model, SelectError> {
    let execution_environment_info =
        ExecutionEnvironmentInfo::find_by_id(execution_environment_info_id)
            .one(db_conn)
            .await;
    transform_select_single_result(execution_environment_info)
}

pub async fn save_execution_environment_info(
    db_conn: &DatabaseConnection,
    new_execution_environment_info: ExecutionEnvironmentInfoPostDto,
) -> Result<execution_environment_info::Model, CreateError> {
    let execution_environment_info =
        execution_environment_info::ActiveModel::from(new_execution_environment_info)
            .insert(db_conn)
            .await;
    transform_create_result(execution_environment_info)
}

pub async fn update_execution_environment_info(
    db_conn: &DatabaseConnection,
    execution_environment_info_id: i32,
    updated_execution_environment_info: ExecutionEnvironmentInfoUpdateDto,
) -> Result<execution_environment_info::Model, UpdateError> {
    let found_execution_environment_info =
        ExecutionEnvironmentInfo::find_by_id(execution_environment_info_id)
            .one(db_conn)
            .await;

    let execution_environment_info_to_update =
        transform_to_be_updated_result(found_execution_environment_info);

    match execution_environment_info_to_update {
        Ok(found_execution_environment_info) => {
            let mut execution_environment_info_to_update: execution_environment_info::ActiveModel =
                found_execution_environment_info.into();
            execution_environment_info_to_update.content =
                Set(updated_execution_environment_info.content);
            execution_environment_info_to_update.test_sequence_execution_id =
                Set(updated_execution_environment_info.test_sequence_execution_id);
            execution_environment_info_to_update.updated_at =
                Set(get_current_datetime_with_timezone_utc());

            let updated_execution_environment_info =
                execution_environment_info_to_update.update(db_conn).await;

            transform_update_result(updated_execution_environment_info)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_execution_environment_info(
    db_conn: &DatabaseConnection,
    execution_environment_info_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = ExecutionEnvironmentInfo::delete_by_id(execution_environment_info_id)
        .exec(db_conn)
        .await;
    transform_delete_result(delete_result)
}
