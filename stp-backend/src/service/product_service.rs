use sea_orm::{ActiveModelTrait, DatabaseConnection, DeleteResult, EntityTrait, Set};

use crate::entity::product::Model;
use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::error::update_error::UpdateError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result, transform_to_be_updated_result, transform_update_result,
};
use crate::{
    dto::product_dto::{ProductPostDto, ProductUpdateDto},
    entity::{prelude::Product, product},
};

pub async fn find_all_products(db_conn: &DatabaseConnection) -> Result<Vec<Model>, SelectError> {
    let products = Product::find().all(db_conn).await;
    transform_select_all_result(products)
}

pub async fn find_single_product(
    db_conn: &DatabaseConnection,
    product_id: i32,
) -> Result<Model, SelectError> {
    let product = Product::find_by_id(product_id).one(db_conn).await;
    transform_select_single_result(product)
}

pub async fn save_product(
    db_conn: &DatabaseConnection,
    new_product: ProductPostDto,
) -> Result<Model, CreateError> {
    let product = product::ActiveModel::from(new_product)
        .insert(db_conn)
        .await;
    transform_create_result(product)
}

pub async fn update_product(
    db_conn: &DatabaseConnection,
    product_id: i32,
    updated_product: ProductUpdateDto,
) -> Result<Model, UpdateError> {
    let found_product = Product::find_by_id(product_id).one(db_conn).await;
    let product_to_update = transform_to_be_updated_result(found_product);

    match product_to_update {
        Ok(found_product) => {
            let mut product_to_update: product::ActiveModel = found_product.into();
            product_to_update.name = Set(updated_product.name);
            product_to_update.description = Set(updated_product.description);
            product_to_update.image_path = Set(updated_product.image_path);
            product_to_update.is_public = Set(updated_product.is_public);

            let updated_product = product_to_update.update(db_conn).await;

            transform_update_result(updated_product)
        }
        Err(error) => Err(error),
    }
}

pub async fn delete_product(
    db_conn: &DatabaseConnection,
    product_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = Product::delete_by_id(product_id).exec(db_conn).await;
    transform_delete_result(delete_result)
}
