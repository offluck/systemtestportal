use sea_orm::DbErr;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq, Eq)]
pub enum SelectError {
    DbError,
    NotFoundError,
}

impl Display for SelectError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for SelectError {}

impl From<DbErr> for SelectError {
    fn from(_error: DbErr) -> SelectError {
        SelectError::DbError
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sea_orm::error::RuntimeErr;

    #[test]
    fn any_db_error_maps_to_db_error() {
        let occured_error = DbErr::Conn(RuntimeErr::Internal("Disconnected".to_owned()));

        assert_eq!(SelectError::DbError, SelectError::from(occured_error))
    }
}
