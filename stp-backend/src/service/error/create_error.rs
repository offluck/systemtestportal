use sea_orm::DbErr;
use std::cmp::PartialEq;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq, Eq)]
pub enum CreateError {
    DbError,
    QueryError,
}

impl Display for CreateError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for CreateError {}

impl From<DbErr> for CreateError {
    fn from(_error: DbErr) -> CreateError {
        match _error {
            DbErr::Query(_) => CreateError::QueryError,
            _ => CreateError::DbError,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sea_orm::error::RuntimeErr;

    #[test]
    fn db_query_error_maps_to_create_query_error() {
        let occured_error = DbErr::Query(RuntimeErr::Internal("Query invalid".to_owned()));

        assert_eq!(CreateError::QueryError, CreateError::from(occured_error))
    }

    #[test]
    fn db_other_error_maps_to_db_error() {
        let occured_error = DbErr::Conn(RuntimeErr::Internal("Disconnected".to_owned()));

        assert_eq!(CreateError::DbError, CreateError::from(occured_error))
    }
}
