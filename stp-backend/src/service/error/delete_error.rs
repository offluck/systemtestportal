use sea_orm::DbErr;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq, Eq)]
pub enum DeleteError {
    DbError,
    NotFoundError,
    DeletedMoreThanOne,
}

impl Display for DeleteError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for DeleteError {}

impl From<DbErr> for DeleteError {
    fn from(_error: DbErr) -> DeleteError {
        DeleteError::DbError
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sea_orm::error::RuntimeErr;

    #[test]
    fn any_db_error_maps_to_db_error() {
        let occured_error = DbErr::Conn(RuntimeErr::Internal("Disconnected".to_owned()));

        assert_eq!(DeleteError::DbError, DeleteError::from(occured_error))
    }
}
