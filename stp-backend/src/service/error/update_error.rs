use sea_orm::DbErr;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, PartialEq, Eq)]
pub enum UpdateError {
    DbError,
    NotFound,
    QueryError,
}

impl Display for UpdateError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for UpdateError {}

impl From<DbErr> for UpdateError {
    fn from(_error: DbErr) -> UpdateError {
        match _error {
            DbErr::Query(_) => UpdateError::QueryError,
            _ => UpdateError::DbError,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sea_orm::error::RuntimeErr;

    #[test]
    fn db_query_error_maps_to_create_query_error() {
        let occured_error = DbErr::Query(RuntimeErr::Internal("Query invalid".to_owned()));

        assert_eq!(UpdateError::QueryError, UpdateError::from(occured_error))
    }

    #[test]
    fn db_other_error_maps_to_db_error() {
        let occured_error = DbErr::Conn(RuntimeErr::Internal("Disconnected".to_owned()));

        assert_eq!(UpdateError::DbError, UpdateError::from(occured_error))
    }
}
