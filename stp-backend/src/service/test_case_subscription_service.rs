use crate::entity::app_user;
use crate::entity::prelude::AppUser;
use crate::service::error::create_error::CreateError;
use crate::service::error::delete_error::DeleteError;
use crate::service::error::select_error::SelectError;
use crate::service::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_all_result,
    transform_select_single_result,
};
use crate::{
    dto::test_case_subscription_dto::TestCaseSubscriptionPostDto,
    entity::{prelude::TestCaseSubscription, test_case_subscription},
};
use sea_orm::{
    ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter,
};

pub async fn find_all_test_case_subscriptions(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
    app_user_id: Option<i32>,
) -> Result<Vec<app_user::Model>, SelectError> {
    let test_case_subscriptions = TestCaseSubscription::find()
        .filter(
            Condition::all()
                .add(test_case_subscription::Column::TestCaseId.eq(test_case_id))
                .add_option(
                    app_user_id.map(|au_id| test_case_subscription::Column::AppUserId.eq(au_id)),
                ),
        )
        .all(db_conn)
        .await
        .expect("Failed to find test cases in the database!");

    let app_user_ids: Vec<i32> = test_case_subscriptions
        .iter()
        .map(|subscription| subscription.app_user_id)
        .collect();

    let app_users = AppUser::find()
        .filter(app_user::Column::Id.is_in(app_user_ids))
        .all(db_conn)
        .await;

    transform_select_all_result(app_users)
}

pub async fn find_single_test_case_subscription(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
    user_id: i32,
) -> Result<app_user::Model, SelectError> {
    let found_test_case_subscription = TestCaseSubscription::find()
        .filter(
            test_case_subscription::Column::TestCaseId
                .eq(test_case_id)
                .and(test_case_subscription::Column::AppUserId.eq(user_id)),
        )
        .one(db_conn)
        .await;

    let result = transform_select_single_result(found_test_case_subscription);

    match &result {
        Ok(model) => {
            let found_app_user = AppUser::find_by_id(model.app_user_id).one(db_conn).await;

            transform_select_single_result(found_app_user)
        }
        Err(_) => Err(SelectError::NotFoundError),
    }
}

pub async fn save_test_case_subscription(
    db_conn: &DatabaseConnection,
    new_test_case: TestCaseSubscriptionPostDto,
) -> Result<test_case_subscription::Model, CreateError> {
    let test_case_subscription_to_save = test_case_subscription::ActiveModel::from(new_test_case);

    let saved_test_case_subscription = test_case_subscription_to_save.insert(db_conn).await;

    transform_create_result(saved_test_case_subscription)
}

pub async fn delete_test_case_subscription(
    db_conn: &DatabaseConnection,
    test_case_id: i32,
    user_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let result = TestCaseSubscription::delete_many()
        .filter(
            test_case_subscription::Column::TestCaseId
                .eq(test_case_id)
                .and(test_case_subscription::Column::AppUserId.eq(user_id)),
        )
        .exec(db_conn)
        .await;

    transform_delete_result(result)
}
