use super::error::{
    create_error::CreateError, delete_error::DeleteError, select_error::SelectError,
};
use super::result_transformer::{
    transform_create_result, transform_delete_result, transform_select_single_result,
};
use sea_orm::{
    ActiveModelTrait, ColumnTrait, DatabaseConnection, DeleteResult, EntityTrait, QueryFilter,
};

use crate::entity::prelude::AppUserActivation;
use crate::{dto::app_user_activation_dto::AppUserActivationPostDto, entity::app_user_activation};

pub async fn find_single_activation_token(
    db_conn: &DatabaseConnection,
    activation_token: String,
) -> Result<app_user_activation::Model, SelectError> {
    let user_activation = AppUserActivation::find()
        .filter(app_user_activation::Column::ActivationToken.eq(activation_token))
        .one(db_conn)
        .await;

    transform_select_single_result(user_activation)
}

pub async fn save_app_user_activation(
    db_conn: &DatabaseConnection,
    new_user_activation: AppUserActivationPostDto,
) -> Result<app_user_activation::Model, CreateError> {
    let user_activation = app_user_activation::ActiveModel::from(new_user_activation)
        .insert(db_conn)
        .await;

    transform_create_result(user_activation)
}

#[allow(dead_code)]
pub async fn delete_app_user_activation_by_app_user_id(
    db_conn: &DatabaseConnection,
    app_user_id: i32,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = AppUserActivation::delete_by_id(app_user_id)
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}

pub async fn delete_app_user_activation_by_token(
    db_conn: &DatabaseConnection,
    activation_token: String,
) -> Result<DeleteResult, DeleteError> {
    let delete_result = AppUserActivation::delete_many()
        .filter(app_user_activation::Column::ActivationToken.eq(activation_token))
        .exec(db_conn)
        .await;

    transform_delete_result(delete_result)
}
