use serde::Deserialize;

#[derive(Deserialize)]
pub struct Email {
    pub email: String,
}
