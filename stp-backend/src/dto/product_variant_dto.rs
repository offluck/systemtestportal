use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::product_variant, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductVariantPostDto {
    pub name: String,
    pub product_version_id: i32,
}

pub type ProductVariantUpdateDto = ProductVariantPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductVariantGetDto {
    pub id: i32,
    pub name: String,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub product_version_id: i32,
}

impl From<product_variant::Model> for ProductVariantGetDto {
    fn from(product_variant_model: product_variant::Model) -> Self {
        let product_variant::Model {
            id,
            name,
            created_at,
            product_version_id,
            ..
        } = product_variant_model;
        Self {
            id,
            name,
            created_at,
            product_version_id,
        }
    }
}

impl From<ProductVariantPostDto> for product_variant::ActiveModel {
    fn from(product_variant_post_dto: ProductVariantPostDto) -> Self {
        let ProductVariantPostDto {
            name,
            product_version_id,
            ..
        } = product_variant_post_dto;
        Self {
            name: Set(name),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            product_version_id: Set(product_version_id),
            ..Default::default()
        }
    }
}
