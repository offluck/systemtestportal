use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::{ActiveEnum, Set};
use serde::{Deserialize, Serialize};

use crate::{entity::app_role, util::get_current_datetime_with_timezone_utc};

use super::active_enums::AppRoleName;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppRolePostDto {
    pub name: AppRoleName,
}

pub type AppRoleUpdateDto = AppRolePostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AppRoleGetDto {
    pub id: i32,
    pub name: AppRoleName,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
}

impl From<app_role::Model> for AppRoleGetDto {
    fn from(app_role_model: app_role::Model) -> Self {
        let app_role::Model {
            id,
            name,
            created_at,
            ..
        } = app_role_model;
        Self {
            id,
            name: AppRoleName::try_from_value(&name).unwrap_or_else(|_| {
                panic!(
                    "Failed to convert name '{}' from database to AppRoleName!",
                    name
                )
            }),
            created_at,
        }
    }
}

impl From<AppRolePostDto> for app_role::ActiveModel {
    fn from(app_role_post_dto: AppRolePostDto) -> Self {
        let AppRolePostDto { name, .. } = app_role_post_dto;
        Self {
            name: Set(name.to_value()),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            ..Default::default()
        }
    }
}
