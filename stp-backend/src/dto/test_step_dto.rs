use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_step, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestStepPostDto {
    pub instruction: String,
    pub expected_result: String,
    pub test_case_index: i32,
    pub test_case_id: i32,
}

pub type TestStepUpdateDto = TestStepPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestStepGetDto {
    pub id: i32,
    pub instruction: String,
    pub expected_result: String,
    pub test_case_index: i32,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub test_case_id: i32,
}

impl From<test_step::Model> for TestStepGetDto {
    fn from(test_step_model: test_step::Model) -> Self {
        let test_step::Model {
            id,
            instruction,
            expected_result,
            test_case_index,
            created_at,
            test_case_id,
            ..
        } = test_step_model;
        Self {
            id,
            instruction,
            expected_result,
            test_case_index,
            created_at,
            test_case_id,
        }
    }
}

impl From<TestStepPostDto> for test_step::ActiveModel {
    fn from(test_step_post_dto: TestStepPostDto) -> Self {
        let TestStepPostDto {
            instruction,
            expected_result,
            test_case_index,
            test_case_id,
            ..
        } = test_step_post_dto;
        Self {
            instruction: Set(instruction),
            expected_result: Set(expected_result),
            test_case_index: Set(test_case_index),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            test_case_id: Set(test_case_id),
            ..Default::default()
        }
    }
}
