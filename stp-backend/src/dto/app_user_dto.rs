use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::app_user, util::get_current_datetime_with_timezone_utc};

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SubscribedUserGetDto {
    pub id: i32,
    pub name: String,
    pub email: String,
}

impl From<app_user::Model> for SubscribedUserGetDto {
    fn from(app_user_model: app_user::Model) -> Self {
        let app_user::Model {
            id, name, email, ..
        } = app_user_model;
        Self { id, name, email }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AppUserPostDto {
    pub name: String,
    pub email: String,
    pub password: String,
    pub is_enabled: bool,
    pub biography: Option<String>,
    pub image_path: Option<String>,
    pub app_role_id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppUserUpdateDto {
    pub name: String,
    pub email: String,
    pub password: String,
    pub new_password: Option<String>,
    pub biography: Option<String>,
    pub image_path: Option<String>,
    pub app_role_id: i32,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AppUserGetDto {
    pub id: i32,
    pub name: String,
    pub email: String,
    pub is_enabled: bool,
    pub biography: Option<String>,
    pub image_path: Option<String>,
    pub is_shown_public: bool,
    pub is_email_public: bool,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub app_role_id: i32,
}

impl From<app_user::Model> for AppUserGetDto {
    fn from(app_user_model: app_user::Model) -> Self {
        // TODO: restrict info depending on requesting user
        let app_user::Model {
            id,
            name,
            email,
            is_enabled,
            biography,
            image_path,
            is_shown_public,
            is_email_public,
            created_at,
            app_role_id,
            ..
        } = app_user_model;

        Self {
            id,
            name,
            email,
            is_enabled,
            biography,
            image_path,
            is_shown_public,
            is_email_public,
            created_at,
            app_role_id,
        }
    }
}

impl From<AppUserPostDto> for app_user::ActiveModel {
    fn from(app_user_post_dto: AppUserPostDto) -> Self {
        let AppUserPostDto {
            name,
            email,
            password,
            is_enabled,
            biography,
            image_path,
            app_role_id,
        } = app_user_post_dto;

        Self {
            name: Set(name),
            email: Set(email),
            password_hash: Set(password.trim().to_string()),
            is_enabled: Set(is_enabled),
            biography: Set(biography),
            image_path: Set(image_path),
            is_shown_public: Set(false),
            is_email_public: Set(false),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            app_role_id: Set(app_role_id),
            ..Default::default()
        }
    }
}

impl From<AppUserUpdateDto> for app_user::ActiveModel {
    fn from(app_user_post_dto: AppUserUpdateDto) -> Self {
        let AppUserUpdateDto {
            name,
            email,
            mut password,
            new_password,
            biography,
            image_path,
            app_role_id,
        } = app_user_post_dto;

        if let Some(new_password) = new_password {
            password = new_password;
        }

        Self {
            name: Set(name),
            email: Set(email),
            password_hash: Set(password),
            is_enabled: Set(true),
            biography: Set(biography),
            image_path: Set(image_path),
            is_shown_public: Set(false),
            is_email_public: Set(false),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            app_role_id: Set(app_role_id),
            ..Default::default()
        }
    }
}
