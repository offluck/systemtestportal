use crate::entity::email_config;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppEmailConfigPostDto {
    pub title: String,
    pub email: String,
    pub app_password: String,
    pub smtp_server: String,
}

pub type AppEmailConfigUpdateDto = AppEmailConfigPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AppEmailConfigGetDto {
    pub id: i32,
    pub title: String,
    pub email: String,
    pub app_password: String,
    pub smtp_server: String,
}

impl From<email_config::Model> for AppEmailConfigGetDto {
    fn from(email_config_model: email_config::Model) -> Self {
        let email_config::Model {
            id,
            title,
            email,
            app_password,
            smtp_server,
            ..
        } = email_config_model;
        Self {
            id,
            title,
            email,
            app_password,
            smtp_server,
        }
    }
}

impl From<AppEmailConfigPostDto> for email_config::ActiveModel {
    fn from(email_config_post_dto: AppEmailConfigPostDto) -> Self {
        let AppEmailConfigPostDto {
            title,
            email,
            app_password,
            smtp_server,
            ..
        } = email_config_post_dto;
        Self {
            title: Set(title),
            email: Set(email),
            app_password: Set(app_password),
            smtp_server: Set(smtp_server),
            ..Default::default()
        }
    }
}
