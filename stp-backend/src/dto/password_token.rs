use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct PasswordToken {
    pub password: String,
    pub token: String,
}
