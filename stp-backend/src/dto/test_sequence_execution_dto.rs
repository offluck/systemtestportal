use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_sequence_execution, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestSequenceExecutionPostDto {
    pub precondition_fulfilled: bool,
    pub test_sequence_id: i32,
    pub tester_id: Option<i32>,
    pub execution_status_id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestSequenceExecutionUpdateDto {
    pub precondition_fulfilled: bool,
    pub test_sequence_id: i32,
    pub tester_id: Option<i32>,
    pub execution_status_id: i32,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestSequenceExecutionGetDto {
    pub id: i32,
    pub precondition_fulfilled: bool,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    #[serde(with = "time::serde::rfc3339")]
    pub updated_at: TimeDateTimeWithTimeZone,
    pub test_sequence_id: i32,
    pub tester_id: Option<i32>,
    pub execution_status_id: i32,
}

impl From<test_sequence_execution::Model> for TestSequenceExecutionGetDto {
    fn from(test_sequence_execution_model: test_sequence_execution::Model) -> Self {
        let test_sequence_execution::Model {
            id,
            precondition_fulfilled,
            created_at,
            updated_at,
            test_sequence_id,
            tester_id,
            execution_status_id,
            ..
        } = test_sequence_execution_model;
        Self {
            id,
            precondition_fulfilled,
            created_at,
            updated_at,
            test_sequence_id,
            tester_id,
            execution_status_id,
        }
    }
}

impl From<TestSequenceExecutionPostDto> for test_sequence_execution::ActiveModel {
    fn from(test_sequence_execution_post_dto: TestSequenceExecutionPostDto) -> Self {
        let TestSequenceExecutionPostDto {
            precondition_fulfilled,
            test_sequence_id,
            tester_id,
            execution_status_id,
            ..
        } = test_sequence_execution_post_dto;
        Self {
            precondition_fulfilled: Set(precondition_fulfilled),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_sequence_id: Set(test_sequence_id),
            tester_id: Set(tester_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}

impl From<TestSequenceExecutionUpdateDto> for test_sequence_execution::ActiveModel {
    fn from(test_sequence_execution_update_dto: TestSequenceExecutionUpdateDto) -> Self {
        let TestSequenceExecutionUpdateDto {
            precondition_fulfilled,
            test_sequence_id,
            tester_id,
            execution_status_id,
            ..
        } = test_sequence_execution_update_dto;
        Self {
            precondition_fulfilled: Set(precondition_fulfilled),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_sequence_id: Set(test_sequence_id),
            tester_id: Set(tester_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}
