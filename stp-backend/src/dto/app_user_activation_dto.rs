use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::Deserialize;

use crate::{entity::app_user_activation, util::get_current_datetime_with_timezone_utc};

pub struct AppUserActivationPostDto {
    pub activation_token: String,
    pub app_user_id: i32,
}

pub struct AppUserActivationGetDto {
    pub id: i32,
    pub activation_token: String,
    pub app_user_id: i32,
    pub created_at: TimeDateTimeWithTimeZone,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppUserActivationToken {
    pub activation_token: String,
}

impl From<app_user_activation::Model> for AppUserActivationGetDto {
    fn from(app_user_activation_model: app_user_activation::Model) -> Self {
        let app_user_activation::Model {
            id,
            activation_token,
            app_user_id,
            created_at,
            ..
        } = app_user_activation_model;

        Self {
            id,
            activation_token,
            app_user_id,
            created_at,
        }
    }
}

impl From<AppUserActivationPostDto> for app_user_activation::ActiveModel {
    fn from(app_user_activation_post_dto: AppUserActivationPostDto) -> Self {
        let AppUserActivationPostDto {
            activation_token,
            app_user_id,
        } = app_user_activation_post_dto;

        Self {
            activation_token: Set(activation_token),
            app_user_id: Set(app_user_id),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            ..Default::default()
        }
    }
}
