use crate::entity::email_template;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EmailTemplatePostDto {
    pub name: String,
    pub content: String,
    pub email_config_id: i32,
}

pub type EmailTemplatePutDto = EmailTemplatePostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct EmailTemplateGetDto {
    pub id: i32,
    pub name: String,
    pub content: String,
    pub email_config_id: i32,
}

impl From<email_template::Model> for EmailTemplateGetDto {
    fn from(email_template_model: email_template::Model) -> Self {
        let email_template::Model {
            id,
            name,
            content,
            email_config_id,
            ..
        } = email_template_model;
        Self {
            id,
            name,
            content,
            email_config_id,
        }
    }
}

impl From<EmailTemplatePostDto> for email_template::ActiveModel {
    fn from(email_template_post_dto: EmailTemplatePostDto) -> Self {
        let EmailTemplatePostDto {
            name,
            content,
            email_config_id,
            ..
        } = email_template_post_dto;
        Self {
            name: Set(name),
            content: Set(content),
            email_config_id: Set(email_config_id),
            ..Default::default()
        }
    }
}
