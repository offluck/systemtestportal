use sea_orm::{DeriveActiveEnum, EnumIter};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "String(Some(1))")]
pub enum AppRoleName {
    // String value is what is saved in the database
    #[sea_orm(string_value = "ADMIN")]
    Admin,
    #[sea_orm(string_value = "USER")]
    User,
}

#[derive(Debug, Clone, PartialEq, Eq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "String(Some(1))")]
pub enum ExecutionStatusName {
    // String value is what is saved in the database
    #[sea_orm(string_value = "UNTESTED")]
    Untested,
    #[sea_orm(string_value = "SKIPPED")]
    Skipped,
    #[sea_orm(string_value = "NOT_APPLICABLE")]
    NotApplicable,
    #[sea_orm(string_value = "NOT_WORKING")]
    NotWorking,
    #[sea_orm(string_value = "PARTIALLY_WORKING")]
    PartiallyWorking,
    #[sea_orm(string_value = "FULLY_WORKING")]
    FullyWorking,
}
