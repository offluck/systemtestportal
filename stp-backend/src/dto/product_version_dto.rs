use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::product_version, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductVersionPostDto {
    pub name: String,
    pub product_id: i32,
}

pub type ProductVersionUpdateDto = ProductVersionPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductVersionGetDto {
    pub id: i32,
    pub name: String,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub product_id: i32,
}

impl From<product_version::Model> for ProductVersionGetDto {
    fn from(product_version_model: product_version::Model) -> Self {
        let product_version::Model {
            id,
            name,
            created_at,
            product_id,
            ..
        } = product_version_model;
        Self {
            id,
            name,
            created_at,
            product_id,
        }
    }
}

impl From<ProductVersionPostDto> for product_version::ActiveModel {
    fn from(product_version_post_dto: ProductVersionPostDto) -> Self {
        let ProductVersionPostDto {
            name, product_id, ..
        } = product_version_post_dto;
        Self {
            name: Set(name),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            product_id: Set(product_id),
            ..Default::default()
        }
    }
}
