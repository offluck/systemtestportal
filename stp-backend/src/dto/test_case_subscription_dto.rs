use crate::entity::test_case_subscription;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseSubscriptionPostDto {
    pub app_user_id: i32,
    pub test_case_id: i32,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseSubscriptionGetDto {
    pub id: i32,
    pub app_user_id: i32,
    pub test_case_id: i32,
}

impl From<test_case_subscription::Model> for TestCaseSubscriptionGetDto {
    fn from(test_case_subscription: test_case_subscription::Model) -> Self {
        let test_case_subscription::Model {
            id,
            app_user_id,
            test_case_id,
        } = test_case_subscription;
        Self {
            id,
            app_user_id,
            test_case_id,
        }
    }
}

impl From<TestCaseSubscriptionPostDto> for test_case_subscription::ActiveModel {
    fn from(test_case_subscription_post_dto: TestCaseSubscriptionPostDto) -> Self {
        let TestCaseSubscriptionPostDto {
            app_user_id,
            test_case_id,
        } = test_case_subscription_post_dto;
        Self {
            app_user_id: Set(app_user_id),
            test_case_id: Set(test_case_id),
            ..Default::default()
        }
    }
}
