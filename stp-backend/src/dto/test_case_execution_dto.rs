use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_case_execution, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize, Clone, Copy)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseExecutionPostDto {
    pub precondition_fulfilled: bool,
    pub test_case_id: i32,
    pub test_sequence_execution_id: i32,
    pub execution_status_id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseExecutionUpdateDto {
    pub precondition_fulfilled: bool,
    pub test_case_id: i32,
    pub test_sequence_execution_id: i32,
    pub execution_status_id: i32,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseExecutionGetDto {
    pub id: i32,
    pub precondition_fulfilled: bool,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    #[serde(with = "time::serde::rfc3339")]
    pub updated_at: TimeDateTimeWithTimeZone,
    pub test_case_id: i32,
    pub test_sequence_execution_id: i32,
    pub execution_status_id: i32,
}

impl From<test_case_execution::Model> for TestCaseExecutionGetDto {
    fn from(test_case_execution_model: test_case_execution::Model) -> Self {
        let test_case_execution::Model {
            id,
            precondition_fulfilled,
            created_at,
            updated_at,
            test_case_id,
            test_sequence_execution_id,
            execution_status_id,
            ..
        } = test_case_execution_model;
        Self {
            id,
            precondition_fulfilled,
            created_at,
            updated_at,
            test_case_id,
            test_sequence_execution_id,
            execution_status_id,
        }
    }
}

impl From<TestCaseExecutionPostDto> for test_case_execution::ActiveModel {
    fn from(test_case_execution_post_dto: TestCaseExecutionPostDto) -> Self {
        let TestCaseExecutionPostDto {
            precondition_fulfilled,
            test_case_id,
            test_sequence_execution_id,
            execution_status_id,
            ..
        } = test_case_execution_post_dto;
        Self {
            precondition_fulfilled: Set(precondition_fulfilled),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_case_id: Set(test_case_id),
            test_sequence_execution_id: Set(test_sequence_execution_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}

impl From<TestCaseExecutionUpdateDto> for test_case_execution::ActiveModel {
    fn from(test_case_execution_update_dto: TestCaseExecutionUpdateDto) -> Self {
        let TestCaseExecutionUpdateDto {
            precondition_fulfilled,
            test_case_id,
            test_sequence_execution_id,
            execution_status_id,
            ..
        } = test_case_execution_update_dto;
        Self {
            precondition_fulfilled: Set(precondition_fulfilled),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_case_id: Set(test_case_id),
            test_sequence_execution_id: Set(test_sequence_execution_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}
