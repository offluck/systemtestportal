use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_sequence, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestSequencePostDto {
    pub name: String,
    pub description: Option<String>,
    pub precondition: Option<String>,
    pub product_variant_id: i32,
}

pub type TestSequenceUpdateDto = TestSequencePostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestSequenceGetDto {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub precondition: Option<String>,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub product_variant_id: i32,
}

impl From<test_sequence::Model> for TestSequenceGetDto {
    fn from(test_sequence_model: test_sequence::Model) -> Self {
        let test_sequence::Model {
            id,
            name,
            description,
            precondition,
            created_at,
            product_variant_id,
            ..
        } = test_sequence_model;
        Self {
            id,
            name,
            description,
            precondition,
            created_at,
            product_variant_id,
        }
    }
}

impl From<TestSequencePostDto> for test_sequence::ActiveModel {
    fn from(test_sequence_post_dto: TestSequencePostDto) -> Self {
        let TestSequencePostDto {
            name,
            description,
            precondition,
            product_variant_id,
            ..
        } = test_sequence_post_dto;
        Self {
            name: Set(name),
            description: Set(description),
            precondition: Set(precondition),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            product_variant_id: Set(product_variant_id),
            ..Default::default()
        }
    }
}
