use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::product, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductPostDto {
    pub name: String,
    pub description: Option<String>,
    pub image_path: Option<String>,
    pub is_public: bool,
}

pub type ProductUpdateDto = ProductPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ProductGetDto {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub image_path: Option<String>,
    pub is_public: bool,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
}

impl From<product::Model> for ProductGetDto {
    fn from(product_model: product::Model) -> Self {
        let product::Model {
            id,
            name,
            description,
            image_path,
            is_public,
            created_at,
            ..
        } = product_model;
        Self {
            id,
            name,
            description,
            image_path,
            is_public,
            created_at,
        }
    }
}

impl From<ProductPostDto> for product::ActiveModel {
    fn from(product_post_dto: ProductPostDto) -> Self {
        let ProductPostDto {
            name,
            description,
            image_path,
            is_public,
            ..
        } = product_post_dto;
        Self {
            name: Set(name),
            description: Set(description),
            image_path: Set(image_path),
            is_public: Set(is_public),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            ..Default::default()
        }
    }
}
