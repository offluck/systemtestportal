use serde::Deserialize;

#[derive(Deserialize)]
pub struct LoginCredentials {
    pub email: String,
    pub password: String,
}
