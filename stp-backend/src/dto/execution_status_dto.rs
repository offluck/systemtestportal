use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::{ActiveEnum, Set};
use serde::{Deserialize, Serialize};

use crate::{entity::execution_status, util::get_current_datetime_with_timezone_utc};

use super::active_enums::ExecutionStatusName;

#[derive(Deserialize)]
pub struct ExecutionStatusPostDto {
    pub name: ExecutionStatusName,
    pub send_notification: bool,
}

pub type ExecutionStatusUpdateDto = ExecutionStatusPostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ExecutionStatusGetDto {
    pub id: i32,
    pub name: ExecutionStatusName,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub send_notification: bool,
}

impl From<execution_status::Model> for ExecutionStatusGetDto {
    fn from(execution_status_model: execution_status::Model) -> Self {
        let execution_status::Model {
            id,
            name,
            created_at,
            send_notification,
            ..
        } = execution_status_model;
        Self {
            id,
            name: ExecutionStatusName::try_from_value(&name).unwrap_or_else(|_| {
                panic!(
                    "Failed to convert name '{}' from database to ExecutionStatusName!",
                    name
                )
            }),
            created_at,
            send_notification,
        }
    }
}

impl From<ExecutionStatusPostDto> for execution_status::ActiveModel {
    fn from(execution_status_post_dto: ExecutionStatusPostDto) -> Self {
        let ExecutionStatusPostDto { name, .. } = execution_status_post_dto;
        Self {
            name: Set(name.to_value()),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            ..Default::default()
        }
    }
}
