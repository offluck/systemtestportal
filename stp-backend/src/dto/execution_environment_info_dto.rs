use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::execution_environment_info, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExecutionEnvironmentInfoPostDto {
    pub content: String,
    pub test_sequence_execution_id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExecutionEnvironmentInfoUpdateDto {
    pub content: String,
    pub test_sequence_execution_id: i32,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ExecutionEnvironmentInfoGetDto {
    pub id: i32,
    pub content: String,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    #[serde(with = "time::serde::rfc3339")]
    pub updated_at: TimeDateTimeWithTimeZone,
    pub test_sequence_execution_id: i32,
}

impl From<execution_environment_info::Model> for ExecutionEnvironmentInfoGetDto {
    fn from(execution_environment_info_model: execution_environment_info::Model) -> Self {
        let execution_environment_info::Model {
            id,
            content,
            created_at,
            updated_at,
            test_sequence_execution_id,
            ..
        } = execution_environment_info_model;
        Self {
            id,
            content,
            created_at,
            updated_at,
            test_sequence_execution_id,
        }
    }
}

impl From<ExecutionEnvironmentInfoPostDto> for execution_environment_info::ActiveModel {
    fn from(execution_environment_info_post_dto: ExecutionEnvironmentInfoPostDto) -> Self {
        let ExecutionEnvironmentInfoPostDto {
            content,
            test_sequence_execution_id,
            ..
        } = execution_environment_info_post_dto;
        Self {
            content: Set(content),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_sequence_execution_id: Set(test_sequence_execution_id),
            ..Default::default()
        }
    }
}

impl From<ExecutionEnvironmentInfoUpdateDto> for execution_environment_info::ActiveModel {
    fn from(execution_environment_info_update_dto: ExecutionEnvironmentInfoUpdateDto) -> Self {
        let ExecutionEnvironmentInfoUpdateDto {
            content,
            test_sequence_execution_id,
            ..
        } = execution_environment_info_update_dto;
        Self {
            content: Set(content),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_sequence_execution_id: Set(test_sequence_execution_id),
            ..Default::default()
        }
    }
}
