use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_case, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCasePostDto {
    pub name: String,
    pub description: Option<String>,
    pub precondition: Option<String>,
    pub test_sequence_index: i32,
    pub test_sequence_id: i32,
}

pub type TestCaseUpdateDto = TestCasePostDto;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestCaseGetDto {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub precondition: Option<String>,
    pub test_sequence_index: i32,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    pub test_sequence_id: i32,
}

impl From<test_case::Model> for TestCaseGetDto {
    fn from(test_case_model: test_case::Model) -> Self {
        let test_case::Model {
            id,
            name,
            description,
            precondition,
            test_sequence_index,
            created_at,
            test_sequence_id,
            ..
        } = test_case_model;
        Self {
            id,
            name,
            description,
            precondition,
            test_sequence_index,
            created_at,
            test_sequence_id,
        }
    }
}

impl From<TestCasePostDto> for test_case::ActiveModel {
    fn from(test_case_post_dto: TestCasePostDto) -> Self {
        let TestCasePostDto {
            name,
            description,
            precondition,
            test_sequence_index,
            test_sequence_id,
            ..
        } = test_case_post_dto;
        Self {
            name: Set(name),
            precondition: Set(precondition),
            description: Set(description),
            test_sequence_index: Set(test_sequence_index),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            test_sequence_id: Set(test_sequence_id),
            ..Default::default()
        }
    }
}
