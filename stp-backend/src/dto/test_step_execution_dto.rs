use sea_orm::entity::prelude::TimeDateTimeWithTimeZone;
use sea_orm::Set;
use serde::{Deserialize, Serialize};

use crate::{entity::test_step_execution, util::get_current_datetime_with_timezone_utc};

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestStepExecutionPostDto {
    pub comment: Option<String>,
    pub test_step_id: i32,
    pub test_case_execution_id: i32,
    pub execution_status_id: i32,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TestStepExecutionUpdateDto {
    pub comment: Option<String>,
    pub test_step_id: i32,
    pub test_case_execution_id: i32,
    pub execution_status_id: i32,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TestStepExecutionGetDto {
    pub id: i32,
    pub comment: Option<String>,
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: TimeDateTimeWithTimeZone,
    #[serde(with = "time::serde::rfc3339")]
    pub updated_at: TimeDateTimeWithTimeZone,
    pub test_step_id: i32,
    pub test_case_execution_id: i32,
    pub execution_status_id: i32,
}

impl From<test_step_execution::Model> for TestStepExecutionGetDto {
    fn from(test_step_execution_model: test_step_execution::Model) -> Self {
        let test_step_execution::Model {
            id,
            comment,
            created_at,
            updated_at,
            test_step_id,
            test_case_execution_id,
            execution_status_id,
            ..
        } = test_step_execution_model;
        Self {
            id,
            comment,
            created_at,
            updated_at,
            test_step_id,
            test_case_execution_id,
            execution_status_id,
        }
    }
}

impl From<TestStepExecutionPostDto> for test_step_execution::ActiveModel {
    fn from(test_step_execution_post_dto: TestStepExecutionPostDto) -> Self {
        let TestStepExecutionPostDto {
            comment,
            test_step_id,
            test_case_execution_id,
            execution_status_id,
            ..
        } = test_step_execution_post_dto;
        Self {
            comment: Set(comment),
            created_at: Set(get_current_datetime_with_timezone_utc()),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_step_id: Set(test_step_id),
            test_case_execution_id: Set(test_case_execution_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}

impl From<TestStepExecutionUpdateDto> for test_step_execution::ActiveModel {
    fn from(test_step_execution_update_dto: TestStepExecutionUpdateDto) -> Self {
        let TestStepExecutionUpdateDto {
            comment,
            test_step_id,
            test_case_execution_id,
            execution_status_id,
            ..
        } = test_step_execution_update_dto;
        Self {
            comment: Set(comment),
            updated_at: Set(get_current_datetime_with_timezone_utc()),
            test_step_id: Set(test_step_id),
            test_case_execution_id: Set(test_case_execution_id),
            execution_status_id: Set(execution_status_id),
            ..Default::default()
        }
    }
}
