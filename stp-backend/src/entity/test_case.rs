//! `SeaORM` Entity. Generated by sea-orm-codegen 0.11.2

use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq)]
#[sea_orm(table_name = "test_case")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub precondition: Option<String>,
    pub test_sequence_index: i32,
    pub created_at: TimeDateTimeWithTimeZone,
    pub test_sequence_id: i32,
}

#[allow(clippy::enum_variant_names)]
#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::test_case_execution::Entity")]
    TestCaseExecution,
    #[sea_orm(
        belongs_to = "super::test_sequence::Entity",
        from = "Column::TestSequenceId",
        to = "super::test_sequence::Column::Id",
        on_update = "Cascade",
        on_delete = "Cascade"
    )]
    TestSequence,
    #[sea_orm(has_many = "super::test_step::Entity")]
    TestStep,
}

impl Related<super::test_case_execution::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::TestCaseExecution.def()
    }
}

impl Related<super::test_sequence::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::TestSequence.def()
    }
}

impl Related<super::test_step::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::TestStep.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
