#[allow(dead_code)]
pub enum EmailType {
    PasswordReset(PasswordResetData),
    ExecutionNotification(ExecutionNotificationData),
    RegistrationConfirmation(RegistrationConfirmationData),
}

impl EmailType {
    pub fn subject(&self) -> String {
        match self {
            EmailType::PasswordReset(_) => "Reset your password".to_string(),
            EmailType::ExecutionNotification(_) => "Test execution result".to_string(),
            EmailType::RegistrationConfirmation(_) => "Verify account".to_string(),
        }
    }

    pub fn name(&self) -> String {
        match self {
            EmailType::PasswordReset(_) => "Forgot Password".to_string(),
            EmailType::ExecutionNotification(_) => "Failed Test".to_string(),
            EmailType::RegistrationConfirmation(_) => "Verify Email".to_string(),
        }
    }
}

pub struct PasswordResetData {
    pub reset_url: String,
    pub hours_expiration: i32,
}

pub struct ExecutionNotificationData {
    pub user_recipient: String,
    pub test_id: i32,
    pub execution_date: String,
}

pub struct RegistrationConfirmationData {
    pub user_recipient: String,
    pub confirmation_url: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_password_reset_subject() {
        let email_type = EmailType::PasswordReset(PasswordResetData {
            reset_url: "https://example.com/reset".to_string(),
            hours_expiration: 4,
        });
        assert_eq!(email_type.subject(), "Reset your password");
    }

    #[test]
    fn test_password_reset_name() {
        let email_type = EmailType::PasswordReset(PasswordResetData {
            reset_url: "https://example.com/reset".to_string(),
            hours_expiration: 4,
        });
        assert_eq!(email_type.name(), "Forgot Password");
    }

    #[test]
    fn test_execution_notification_subject() {
        let email_type = EmailType::ExecutionNotification(ExecutionNotificationData {
            user_recipient: "user@example.com".to_string(),
            test_id: 123,
            execution_date: "2023-06-27".to_string(),
        });
        assert_eq!(email_type.subject(), "Test execution result");
    }

    #[test]
    fn test_execution_notification_name() {
        let email_type = EmailType::ExecutionNotification(ExecutionNotificationData {
            user_recipient: "user@example.com".to_string(),
            test_id: 123,
            execution_date: "2023-06-27".to_string(),
        });
        assert_eq!(email_type.name(), "Failed Test");
    }

    #[test]
    fn test_registration_confirmation_subject() {
        let email_type = EmailType::RegistrationConfirmation(RegistrationConfirmationData {
            user_recipient: "user@example.com".to_string(),
            confirmation_url: "https://example.com/confirm".to_string(),
        });
        assert_eq!(email_type.subject(), "Verify account");
    }

    #[test]
    fn test_registration_confirmation_name() {
        let email_type = EmailType::RegistrationConfirmation(RegistrationConfirmationData {
            user_recipient: "user@example.com".to_string(),
            confirmation_url: "https://example.com/confirm".to_string(),
        });
        assert_eq!(email_type.name(), "Verify Email");
    }
}
