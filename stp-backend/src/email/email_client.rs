use crate::email::Email;
use lettre::message::header::ContentType;
use lettre::transport::smtp::authentication::Credentials;
use lettre::transport::smtp::client::{Tls, TlsParameters};
use lettre::{Message, SmtpTransport, Transport};

/// Sends an email using the provided email data, password, and SMTP server.
///
/// # Arguments
/// * `email_data` - The email data containing the sender, recipient, subject, and content.
/// * `password` - The password associated with the sender's email account.
/// * `smtp_server` - The SMTP server address to use for sending the email.
///
/// # Panics
/// This function will panic if the email message construction fails or if the SMTP transport
/// cannot be created.
///
pub fn send(email_data: Email, password: String, smtp_server: &str) {
    let email = Message::builder()
        .from(email_data.from.parse().unwrap())
        .to(email_data.to.parse().unwrap())
        .subject(email_data.subject)
        .header(ContentType::TEXT_HTML)
        .body(email_data.content)
        .unwrap();

    let creds = Credentials::new(email_data.from, password);

    let tls = TlsParameters::builder(smtp_server.to_string())
        .dangerous_accept_invalid_certs(true)
        .build()
        .unwrap();

    let mailer = SmtpTransport::relay(smtp_server)
        .unwrap()
        .port(587)
        .credentials(creds)
        .tls(Tls::Required(tls))
        .build();

    match mailer.send(&email) {
        Ok(_) => println!("Email sent successfully!"),
        Err(e) => eprintln!("Could not send email: {e:?}"),
    }
}
