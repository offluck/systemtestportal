pub(self) mod email_client;
pub mod email_service;
pub mod email_type;

pub struct Email {
    pub from: String,
    pub to: String,
    pub subject: String,
    pub content: String,
}
