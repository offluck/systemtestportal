use crate::email::email_client;
use crate::email::email_type::EmailType;
use crate::email::Email;
use log::debug;
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, ModelTrait, QueryFilter};

use crate::entity::email_template::Model;
use crate::entity::prelude::{EmailConfig, EmailTemplate};
use crate::entity::{email_config, email_template};
use crate::service::error::select_error::SelectError;
use crate::service::result_transformer::transform_select_single_result;

/// Creates email struct for sending an email. Hands the struct to the email client
/// The email content and mail client configuration are retrieved from the database.
///
/// # Arguments
/// * `email_type` - The type of email to send (e.g., PasswordReset, ExecutionNotification, RegistrationConfirmation), including individual content (`EmailType`).
/// * `to` - The email recipient (`String`)
/// * `db_conn` - The database connection used for retrieval of mail template and config (`&DatabaseConnection`).
///
pub async fn send(email_type: EmailType, to: String, db_conn: &DatabaseConnection) {
    let email_template_result = find_email_template_by_name(email_type.name(), db_conn).await;

    if let Err(err) = email_template_result {
        debug!("{}", err);
        // TODO: handle error
        return;
    }

    let email_template = email_template_result.unwrap();

    let email_config_result = find_email_config(&email_template, db_conn).await;

    if let Err(err) = email_config_result {
        debug!("{}", err);
        // TODO: handle error
        return;
    }

    let email_config = email_config_result.unwrap();

    let email = Email {
        from: email_config.email,
        to,
        subject: email_type.subject(),
        content: generate_content(email_type, &email_template.content),
    };

    email_client::send(email, email_config.app_password, &email_config.smtp_server);
}

async fn find_email_config(
    email_template: &Model,
    db_conn: &DatabaseConnection,
) -> Result<email_config::Model, SelectError> {
    let email_config = email_template.find_related(EmailConfig).one(db_conn).await;

    transform_select_single_result(email_config)
}

async fn find_email_template_by_name(
    name: String,
    db_conn: &DatabaseConnection,
) -> Result<Model, SelectError> {
    let result = EmailTemplate::find()
        .filter(email_template::Column::Name.eq(name))
        .one(db_conn)
        .await;

    transform_select_single_result(result)
}

fn generate_content(email_type: EmailType, config_content: &str) -> String {
    match email_type {
        EmailType::PasswordReset(password_reset_email) => config_content
            .replace(
                "[Expire hours]",
                &password_reset_email.hours_expiration.to_string(),
            )
            .replace("[Reset Link]", &password_reset_email.reset_url),
        EmailType::ExecutionNotification(execution_notification_email) => config_content
            .replace(
                "[Recipient's Name]",
                &execution_notification_email.user_recipient,
            )
            .replace(
                "[Test ID]",
                &execution_notification_email.test_id.to_string(),
            )
            .replace("[Test Date]", &execution_notification_email.execution_date),
        EmailType::RegistrationConfirmation(registration_confirmation_email) => config_content
            .replace(
                "[Recipient Name]",
                &registration_confirmation_email.user_recipient,
            )
            .replace(
                "[Verification Link]",
                &registration_confirmation_email.confirmation_url,
            ),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::email::email_type::{
        ExecutionNotificationData, PasswordResetData, RegistrationConfirmationData,
    };

    #[test]
    fn test_generate_content_password_reset() {
        let email_type = EmailType::PasswordReset(PasswordResetData {
            reset_url: "https://example.com/reset".to_string(),
            hours_expiration: 4,
        });
        let config_content = "Forgot password\n\nReset your password.\n\nPlease click the following link to reset your password: [Reset Link]\n\nThis link expires in [Expire hours] hours.\n\nIf you did not request a password reset, please get in contact immediately.\n\nThank you,\nSTP-Team";

        let expected_content = "Forgot password\n\nReset your password.\n\nPlease click the following link to reset your password: https://example.com/reset\n\nThis link expires in 4 hours.\n\nIf you did not request a password reset, please get in contact immediately.\n\nThank you,\nSTP-Team";

        assert_eq!(
            generate_content(email_type, config_content),
            expected_content
        );
    }

    #[test]
    fn test_generate_content_execution_notification() {
        let email_type = EmailType::ExecutionNotification(ExecutionNotificationData {
            user_recipient: "John Doe".to_string(),
            test_id: 1,
            execution_date: "2022-01-01".to_string(),
        });
        let config_content = "Hello [Recipient's Name],\n\nWe regret to inform you that the following test has failed:\n\nTest ID: [Test ID]\nDate: [Test Date]\n\nPlease review the test results and take appropriate actions to address the issues encountered during the test.\nIf you have any questions or need further assistance, please don't hesitate to contact our team.\n\nThank you for your attention to this matter.\n\nBest regards,\nSTP-Team";

        let expected_content = "Hello John Doe,\n\nWe regret to inform you that the following test has failed:\n\nTest ID: 1\nDate: 2022-01-01\n\nPlease review the test results and take appropriate actions to address the issues encountered during the test.\nIf you have any questions or need further assistance, please don't hesitate to contact our team.\n\nThank you for your attention to this matter.\n\nBest regards,\nSTP-Team";

        assert_eq!(
            generate_content(email_type, config_content),
            expected_content
        );
    }

    #[test]
    fn test_generate_content_registration_confirmation() {
        let email_type = EmailType::RegistrationConfirmation(RegistrationConfirmationData {
            user_recipient: "Jane Smith".to_string(),
            confirmation_url: "https://example.com/confirm".to_string(),
        });
        let config_content = "Dear [Recipient Name],\n\nThank you for signing up for our service. To complete the registration process, please verify your email address by clicking on the following link:\n\n[Verification Link]\n\nIf you did not register on our platform, please ignore this mail.\n\nThank you,\nSTP-Team";

        let expected_content = "Dear Jane Smith,\n\nThank you for signing up for our service. To complete the registration process, please verify your email address by clicking on the following link:\n\nhttps://example.com/confirm\n\nIf you did not register on our platform, please ignore this mail.\n\nThank you,\nSTP-Team";

        assert_eq!(
            generate_content(email_type, config_content),
            expected_content
        );
    }
}
