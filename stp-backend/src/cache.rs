use std::collections::HashMap;
use tokio::sync::Mutex;

#[derive(Debug)]
pub struct Token {
    pub user_id: i32,
    pub validity: i64,
}

#[derive(Default)]
pub struct AppCache {
    pub disabled_users: Mutex<HashMap<String, u64>>,
    pub users_tokens: Mutex<HashMap<String, Token>>,
}

impl AppCache {
    pub fn new() -> AppCache {
        AppCache::default()
    }

    pub fn insert_disable_user(&self, user: String) {
        let mut disabled_users = self.disabled_users.blocking_lock();
        let failed_attempts = disabled_users.entry(user);
        *failed_attempts.or_insert(0) += 1;
    }

    pub async fn async_insert_disable_user(&self, user: String) {
        let mut disabled_users = self.disabled_users.lock().await;
        let failed_attempts = disabled_users.entry(user);
        *failed_attempts.or_insert(0) += 1;
    }

    pub fn get_disabled_users(&self) -> HashMap<String, u64> {
        self.disabled_users.blocking_lock().clone()
    }

    pub async fn async_get_disabled_users(&self) -> HashMap<String, u64> {
        self.disabled_users.lock().await.clone()
    }

    pub fn clear_cache(&self) {
        self.clear_disabled_users_cache();
        self.clear_users_tokens_cache();
    }

    pub fn clear_disabled_users_cache(&self) {
        self.disabled_users.blocking_lock().clear()
    }

    pub fn clear_users_tokens_cache(&self) {
        self.disabled_users.blocking_lock().clear()
    }

    pub fn clear_users_cache(&self) {
        self.disabled_users.blocking_lock().clear()
    }

    pub async fn async_clear_cache(&self) {
        self.async_clear_disabled_users_cache().await;
        self.async_clear_users_tokens_cache().await;
    }

    pub async fn async_clear_disabled_users_cache(&self) {
        self.disabled_users.lock().await.clear()
    }

    pub async fn async_clear_users_tokens_cache(&self) {
        self.disabled_users.lock().await.clear()
    }
}
