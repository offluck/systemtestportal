use sea_orm_migration::prelude::*;

use super::{
    m20220101_000005_create_test_case_table::TestCase,
    m20220101_000009_create_execution_status_table::ExecutionStatus,
    m20220101_000010_create_test_sequence_execution_table::TestSequenceExecution,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestCaseExecution::Table)
                    .col(
                        ColumnDef::new(TestCaseExecution::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::PreconditionFulfilled)
                            .boolean()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::UpdatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::TestCaseId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::TestSequenceExecutionId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseExecution::ExecutionStatusId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestCaseExecution::Table, TestCaseExecution::TestCaseId)
                            .to(TestCase::Table, TestCase::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestCaseExecution::Table,
                                TestCaseExecution::TestSequenceExecutionId,
                            )
                            .to(TestSequenceExecution::Table, TestSequenceExecution::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestCaseExecution::Table,
                                TestCaseExecution::ExecutionStatusId,
                            )
                            .to(ExecutionStatus::Table, ExecutionStatus::Id)
                            .on_delete(ForeignKeyAction::NoAction)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(TestCaseExecution::Table)
                            .col(TestCaseExecution::TestCaseId)
                            .col(TestCaseExecution::TestSequenceExecutionId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestCaseExecution::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestCaseExecution {
    Table,
    Id,
    PreconditionFulfilled,
    CreatedAt,
    UpdatedAt,
    TestCaseId,
    TestSequenceExecutionId,
    ExecutionStatusId,
}
