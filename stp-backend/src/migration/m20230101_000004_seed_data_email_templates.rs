use crate::entity;
use crate::migration::m20230101_000002_create_email_template_table::EmailTemplate;
use crate::migration::Query;
use sea_orm::DbErr;
use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::entity::*;
use sea_orm_migration::{MigrationTrait, SchemaManager};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let failed_test_template = include_str!("../assets/failed-test-notify.txt");
        let forgot_password_template = include_str!("../assets/forgot-password.txt");
        let verify_email_template = include_str!("../assets/verify-email.txt");

        //insert email templates
        let insert = Query::insert()
            .into_table(EmailTemplate::Table)
            .columns([
                EmailTemplate::Name,
                EmailTemplate::Content,
                EmailTemplate::EmailConfigId,
            ])
            .values_panic([
                "Forgot Password".into(),
                forgot_password_template.into(),
                1.into(),
            ])
            .values_panic(["Failed Test".into(), failed_test_template.into(), 1.into()])
            .values_panic([
                "Verify Email".into(),
                verify_email_template.into(),
                1.into(),
            ])
            .to_owned();

        manager.exec_stmt(insert).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        entity::prelude::EmailTemplate::delete_many()
            .exec(db_conn)
            .await?;

        Ok(())
    }
}
