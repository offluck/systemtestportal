use sea_orm_migration::prelude::*;

use super::{
    m20220101_000006_create_test_step_table::TestStep,
    m20220101_000009_create_execution_status_table::ExecutionStatus,
    m20220101_000011_create_test_case_execution_table::TestCaseExecution,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestStepExecution::Table)
                    .col(
                        ColumnDef::new(TestStepExecution::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(TestStepExecution::Comment).string())
                    .col(
                        ColumnDef::new(TestStepExecution::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestStepExecution::UpdatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestStepExecution::TestStepId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestStepExecution::TestCaseExecutionId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestStepExecution::ExecutionStatusId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestStepExecution::Table, TestStepExecution::TestStepId)
                            .to(TestStep::Table, TestStep::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestStepExecution::Table,
                                TestStepExecution::TestCaseExecutionId,
                            )
                            .to(TestCaseExecution::Table, TestCaseExecution::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestStepExecution::Table,
                                TestStepExecution::ExecutionStatusId,
                            )
                            .to(ExecutionStatus::Table, ExecutionStatus::Id)
                            .on_delete(ForeignKeyAction::NoAction)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(TestStepExecution::Table)
                            .col(TestStepExecution::TestStepId)
                            .col(TestStepExecution::TestCaseExecutionId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestStepExecution::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestStepExecution {
    Table,
    Id,
    Comment,
    CreatedAt,
    UpdatedAt,
    TestStepId,
    TestCaseExecutionId,
    ExecutionStatusId,
}
