use crate::entity;
use crate::migration::m20230101_000001_create_email_config_table::EmailConfig;
use crate::migration::Query;
use sea_orm::DbErr;
use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::entity::*;
use sea_orm_migration::{MigrationTrait, SchemaManager};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let insert = Query::insert()
            .into_table(EmailConfig::Table)
            .columns([
                EmailConfig::Title,
                EmailConfig::Email,
                EmailConfig::AppPassword,
                EmailConfig::SmtpServer,
            ])
            .values_panic([
                "Dummy Title".into(),
                "s@gmail.com".into(),
                "k".into(),
                "smtp.gmail.com".into(),
            ])
            .to_owned();

        manager.exec_stmt(insert).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        entity::prelude::EmailConfig::delete_many()
            .exec(db_conn)
            .await?;

        Ok(())
    }
}
