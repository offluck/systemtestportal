use sea_orm_migration::prelude::*;

use super::m20220101_000002_create_product_version_table::ProductVersion;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(ProductVariant::Table)
                    .col(
                        ColumnDef::new(ProductVariant::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(ProductVariant::Name).string().not_null())
                    .col(
                        ColumnDef::new(ProductVariant::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ProductVariant::ProductVersionId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(ProductVariant::Table, ProductVariant::ProductVersionId)
                            .to(ProductVersion::Table, ProductVersion::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(ProductVariant::Table)
                            .col(ProductVariant::Name)
                            .col(ProductVariant::ProductVersionId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ProductVariant::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum ProductVariant {
    Table,
    Id,
    Name,
    CreatedAt,
    ProductVersionId,
}
