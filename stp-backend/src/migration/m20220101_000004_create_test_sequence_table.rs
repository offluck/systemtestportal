use sea_orm_migration::prelude::*;

use super::m20220101_000003_create_product_variant_table::ProductVariant;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestSequence::Table)
                    .col(
                        ColumnDef::new(TestSequence::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(TestSequence::Name).string().not_null())
                    .col(ColumnDef::new(TestSequence::Description).string())
                    .col(ColumnDef::new(TestSequence::Precondition).string())
                    .col(
                        ColumnDef::new(TestSequence::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestSequence::ProductVariantId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestSequence::Table, TestSequence::ProductVariantId)
                            .to(ProductVariant::Table, ProductVariant::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(TestSequence::Table)
                            .col(TestSequence::Name)
                            .col(TestSequence::ProductVariantId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestSequence::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestSequence {
    Table,
    Id,
    Name,
    Description,
    Precondition,
    CreatedAt,
    ProductVariantId,
}
