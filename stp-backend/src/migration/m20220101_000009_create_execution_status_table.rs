use sea_orm_migration::prelude::*;

use crate::entity::execution_status;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(ExecutionStatus::Table)
                    .col(
                        ColumnDef::new(ExecutionStatus::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    // SeaORM doesn't support constraints yet
                    // since SeaQuery implemented them just recently (as of 2022-01-12)
                    // that leads to only the code layer preventing insertion of non-enum strings
                    // https://github.com/SeaQL/sea-query/pull/567
                    .col(
                        ColumnDef::new(execution_status::Column::Name)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(ExecutionStatus::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ExecutionStatus::SendNotification)
                            .boolean()
                            .default(false),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ExecutionStatus::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum ExecutionStatus {
    Table,
    Id,
    // Name is a enum which is referred to by its entity field
    _Name,
    CreatedAt,
    SendNotification,
}
