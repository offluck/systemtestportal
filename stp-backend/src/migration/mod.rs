pub use sea_orm_migration::prelude::*;

// add each migration file as a module
mod m20220101_000001_create_product_table;
mod m20220101_000002_create_product_version_table;
mod m20220101_000003_create_product_variant_table;
mod m20220101_000004_create_test_sequence_table;
mod m20220101_000005_create_test_case_table;
mod m20220101_000006_create_test_step_table;
mod m20220101_000007_create_app_role_table;
mod m20220101_000008_create_user_table;
mod m20220101_000009_create_execution_status_table;
mod m20220101_000010_create_test_sequence_execution_table;
mod m20220101_000011_create_test_case_execution_table;
mod m20220101_000012_create_test_step_execution_table;
mod m20220101_000013_create_execution_environment_info_table;
mod m20220101_000014_seed_enum_variants_app_role;
mod m20220101_000015_seed_enum_variants_execution_status;
mod m20230101_000001_create_email_config_table;
mod m20230101_000002_create_email_template_table;
mod m20230101_000003_seed_data_email_config;
mod m20230101_000004_create_app_user_activation_table;
mod m20230101_000004_seed_data_email_templates;
mod m20230101_000005_create_test_case_subscription_table;
mod m20230101_000006_seed_data_user_table;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            // Define the order of the migrations
            Box::new(m20220101_000001_create_product_table::Migration),
            Box::new(m20220101_000002_create_product_version_table::Migration),
            Box::new(m20220101_000003_create_product_variant_table::Migration),
            Box::new(m20220101_000004_create_test_sequence_table::Migration),
            Box::new(m20220101_000005_create_test_case_table::Migration),
            Box::new(m20220101_000006_create_test_step_table::Migration),
            Box::new(m20220101_000007_create_app_role_table::Migration),
            Box::new(m20220101_000008_create_user_table::Migration),
            Box::new(m20220101_000009_create_execution_status_table::Migration),
            Box::new(m20220101_000010_create_test_sequence_execution_table::Migration),
            Box::new(m20220101_000011_create_test_case_execution_table::Migration),
            Box::new(m20220101_000012_create_test_step_execution_table::Migration),
            Box::new(m20220101_000013_create_execution_environment_info_table::Migration),
            Box::new(m20220101_000014_seed_enum_variants_app_role::Migration),
            Box::new(m20220101_000015_seed_enum_variants_execution_status::Migration),
            Box::new(m20230101_000001_create_email_config_table::Migration),
            Box::new(m20230101_000002_create_email_template_table::Migration),
            Box::new(m20230101_000003_seed_data_email_config::Migration),
            Box::new(m20230101_000004_seed_data_email_templates::Migration),
            Box::new(m20230101_000005_create_test_case_subscription_table::Migration),
            Box::new(m20230101_000004_create_app_user_activation_table::Migration),
            Box::new(m20230101_000006_seed_data_user_table::Migration),
        ]
    }
}
