use sea_orm_migration::prelude::*;

use super::m20220101_000008_create_user_table::AppUser;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(AppUserActivation::Table)
                    .col(
                        ColumnDef::new(AppUserActivation::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(AppUserActivation::ActivationToken)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(AppUserActivation::AppUserId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(AppUserActivation::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(AppUserActivation::Table, AppUserActivation::AppUserId)
                            .to(AppUser::Table, AppUser::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(AppUserActivation::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum AppUserActivation {
    Table,
    Id,
    ActivationToken,
    AppUserId,
    CreatedAt,
}
