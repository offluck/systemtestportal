use sea_orm_migration::prelude::*;

use super::m20220101_000010_create_test_sequence_execution_table::TestSequenceExecution;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(ExecutionEnvironmentInfo::Table)
                    .col(
                        ColumnDef::new(ExecutionEnvironmentInfo::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(ExecutionEnvironmentInfo::Content)
                            .string()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ExecutionEnvironmentInfo::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ExecutionEnvironmentInfo::UpdatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ExecutionEnvironmentInfo::TestSequenceExecutionId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                ExecutionEnvironmentInfo::Table,
                                ExecutionEnvironmentInfo::TestSequenceExecutionId,
                            )
                            .to(TestSequenceExecution::Table, TestSequenceExecution::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(ExecutionEnvironmentInfo::Table)
                    .to_owned(),
            )
            .await
    }
}

#[derive(Iden)]
pub enum ExecutionEnvironmentInfo {
    Table,
    Id,
    Content,
    CreatedAt,
    UpdatedAt,
    TestSequenceExecutionId,
}
