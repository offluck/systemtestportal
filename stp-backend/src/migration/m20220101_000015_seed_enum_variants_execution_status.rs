use crate::{
    dto::{active_enums::ExecutionStatusName, execution_status_dto::ExecutionStatusPostDto},
    entity::{execution_status, prelude::ExecutionStatus},
};
use sea_orm::EntityTrait;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        let variants: Vec<execution_status::ActiveModel> = vec![
            (ExecutionStatusName::Untested, true),
            (ExecutionStatusName::NotApplicable, true),
            (ExecutionStatusName::Skipped, true),
            (ExecutionStatusName::NotWorking, true),
            (ExecutionStatusName::PartiallyWorking, true),
            (ExecutionStatusName::FullyWorking, false),
        ]
        .into_iter()
        .map(|(name, send_notification)| {
            execution_status::ActiveModel::from(ExecutionStatusPostDto {
                name,
                send_notification,
            })
        })
        .collect();

        ExecutionStatus::insert_many(variants).exec(db_conn).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        ExecutionStatus::delete_many().exec(db_conn).await?;

        Ok(())
    }
}
