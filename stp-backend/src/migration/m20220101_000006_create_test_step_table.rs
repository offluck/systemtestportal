use sea_orm_migration::prelude::*;

use super::m20220101_000005_create_test_case_table::TestCase;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestStep::Table)
                    .col(
                        ColumnDef::new(TestStep::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(TestStep::Instruction).string().not_null())
                    .col(ColumnDef::new(TestStep::ExpectedResult).string().not_null())
                    .col(ColumnDef::new(TestStep::TestCaseIndex).integer().not_null())
                    .col(
                        ColumnDef::new(TestStep::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(ColumnDef::new(TestStep::TestCaseId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestStep::Table, TestStep::TestCaseId)
                            .to(TestCase::Table, TestCase::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    // Create unique constraint on test_case_id + test_case_index
                    // to avoid assigning the same index multiple times for a test case
                    .index(
                        Index::create()
                            .table(TestStep::Table)
                            .col(TestStep::TestCaseId)
                            .col(TestStep::TestCaseIndex)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestStep::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestStep {
    Table,
    Id,
    Instruction,
    ExpectedResult,
    TestCaseIndex,
    CreatedAt,
    TestCaseId,
}
