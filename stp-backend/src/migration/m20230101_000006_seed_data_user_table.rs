use crate::entity;
use crate::migration::m20220101_000008_create_user_table::AppUser;
use crate::migration::Query;
use chrono::prelude::*;
use sea_orm::DbErr;
use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::entity::*;
use sea_orm_migration::{MigrationTrait, SchemaManager};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let now = Utc::now();
        let insert = Query::insert()
            .into_table(AppUser::Table)
            .columns([
                AppUser::Name,
                AppUser::Email,
                AppUser::PasswordHash,
                AppUser::IsEnabled,
                AppUser::Biography,
                AppUser::ImagePath,
                AppUser::IsShownPublic,
                AppUser::IsEmailPublic,
                AppUser::CreatedAt,
                AppUser::AppRoleId,
            ])
            .values_panic([
                "L".into(),
                "l@study.thws.de".into(),
                "cc18280ae90379240e4cffb910ca4a433fcd593d0754283e31326d2fddd05636".into(),
                true.into(),
                "biography".into(),
                "image_path".into(),
                false.into(),
                false.into(),
                now.into(),
                1.into(),
            ])
            .to_owned();

        manager.exec_stmt(insert).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        entity::prelude::AppUser::delete_many()
            .exec(db_conn)
            .await?;

        Ok(())
    }
}
