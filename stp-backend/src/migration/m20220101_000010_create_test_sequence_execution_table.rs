use sea_orm_migration::prelude::*;

use super::{
    m20220101_000004_create_test_sequence_table::TestSequence,
    m20220101_000008_create_user_table::AppUser,
    m20220101_000009_create_execution_status_table::ExecutionStatus,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestSequenceExecution::Table)
                    .col(
                        ColumnDef::new(TestSequenceExecution::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(TestSequenceExecution::PreconditionFulfilled)
                            .boolean()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestSequenceExecution::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestSequenceExecution::UpdatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestSequenceExecution::TestSequenceId)
                            .integer()
                            .not_null(),
                    )
                    .col(ColumnDef::new(TestSequenceExecution::TesterId).integer())
                    .col(
                        ColumnDef::new(TestSequenceExecution::ExecutionStatusId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestSequenceExecution::Table,
                                TestSequenceExecution::TestSequenceId,
                            )
                            .to(TestSequence::Table, TestSequence::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestSequenceExecution::Table,
                                TestSequenceExecution::TesterId,
                            )
                            .to(AppUser::Table, AppUser::Id)
                            .on_delete(ForeignKeyAction::NoAction)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestSequenceExecution::Table,
                                TestSequenceExecution::ExecutionStatusId,
                            )
                            .to(ExecutionStatus::Table, ExecutionStatus::Id)
                            .on_delete(ForeignKeyAction::NoAction)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestSequenceExecution::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestSequenceExecution {
    Table,
    Id,
    PreconditionFulfilled,
    CreatedAt,
    UpdatedAt,
    TestSequenceId,
    TesterId,
    ExecutionStatusId,
}
