use sea_orm_migration::prelude::*;

use super::m20220101_000005_create_test_case_table::TestCase;
use super::m20220101_000008_create_user_table::AppUser;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestCaseSubscription::Table)
                    .col(
                        ColumnDef::new(TestCaseSubscription::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(TestCaseSubscription::TestCaseId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCaseSubscription::AppUserId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(
                                TestCaseSubscription::Table,
                                TestCaseSubscription::TestCaseId,
                            )
                            .to(TestCase::Table, TestCase::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestCaseSubscription::Table, TestCaseSubscription::AppUserId)
                            .to(AppUser::Table, AppUser::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestCaseSubscription::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestCaseSubscription {
    Table,
    Id,
    TestCaseId,
    AppUserId,
}
