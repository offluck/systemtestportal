use sea_orm_migration::prelude::*;

use super::m20230101_000001_create_email_config_table::EmailConfig;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(EmailTemplate::Table)
                    .col(
                        ColumnDef::new(EmailTemplate::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(EmailTemplate::Name).string().not_null())
                    .col(ColumnDef::new(EmailTemplate::Content).string().not_null())
                    .col(
                        ColumnDef::new(EmailTemplate::EmailConfigId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(EmailTemplate::Table, EmailTemplate::EmailConfigId)
                            .to(EmailConfig::Table, EmailConfig::Id)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(EmailTemplate::Table)
                            .col(EmailTemplate::Name)
                            .col(EmailTemplate::EmailConfigId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(EmailTemplate::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum EmailTemplate {
    Table,
    Id,
    Name,
    Content,
    EmailConfigId,
}
