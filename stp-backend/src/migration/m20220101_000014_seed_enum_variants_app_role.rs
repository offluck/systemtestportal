use crate::{
    dto::{active_enums::AppRoleName, app_role_dto::AppRolePostDto},
    entity::{app_role, prelude::AppRole},
};
use sea_orm::EntityTrait;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        let variants: Vec<app_role::ActiveModel> = vec![AppRoleName::Admin, AppRoleName::User]
            .into_iter()
            .map(|name| app_role::ActiveModel::from(AppRolePostDto { name }))
            .collect();

        AppRole::insert_many(variants).exec(db_conn).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db_conn = manager.get_connection();

        AppRole::delete_many().exec(db_conn).await?;

        Ok(())
    }
}
