use sea_orm_migration::prelude::*;

use super::m20220101_000001_create_product_table::Product;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(ProductVersion::Table)
                    .col(
                        ColumnDef::new(ProductVersion::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(ProductVersion::Name).string().not_null())
                    .col(
                        ColumnDef::new(ProductVersion::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ProductVersion::ProductId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(ProductVersion::Table, ProductVersion::ProductId)
                            .to(Product::Table, Product::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .index(
                        Index::create()
                            .table(ProductVersion::Table)
                            .col(ProductVersion::Name)
                            .col(ProductVersion::ProductId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ProductVersion::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum ProductVersion {
    Table,
    Id,
    Name,
    CreatedAt,
    ProductId,
}
