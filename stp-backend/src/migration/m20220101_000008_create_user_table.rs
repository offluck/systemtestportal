use sea_orm_migration::prelude::*;

use super::m20220101_000007_create_app_role_table::AppRole;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(AppUser::Table)
                    .col(
                        ColumnDef::new(AppUser::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(AppUser::Name).string().not_null())
                    .col(
                        ColumnDef::new(AppUser::Email)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(AppUser::PasswordHash).string().not_null())
                    .col(
                        ColumnDef::new(AppUser::IsEnabled)
                            .boolean()
                            .not_null()
                            .default(true),
                    )
                    .col(ColumnDef::new(AppUser::Biography).string())
                    .col(ColumnDef::new(AppUser::ImagePath).string())
                    .col(
                        ColumnDef::new(AppUser::IsShownPublic)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .col(
                        ColumnDef::new(AppUser::IsEmailPublic)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .col(
                        ColumnDef::new(AppUser::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    // hacky but currently not possible otherwise: referenced entity needs to exist
                    // before migration can be executed
                    .col(ColumnDef::new(AppUser::AppRoleId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from(AppUser::Table, AppUser::AppRoleId)
                            .to(AppRole::Table, AppRole::Id)
                            .on_delete(ForeignKeyAction::NoAction)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(AppUser::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum AppUser {
    Table,
    Id,
    Name,
    Email,
    PasswordHash,
    IsEnabled,
    Biography,
    ImagePath,
    IsShownPublic,
    IsEmailPublic,
    CreatedAt,
    AppRoleId,
}
