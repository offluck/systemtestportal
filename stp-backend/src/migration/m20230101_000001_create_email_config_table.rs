use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(EmailConfig::Table)
                    .col(
                        ColumnDef::new(EmailConfig::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(EmailConfig::Title)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(EmailConfig::Email).string().not_null())
                    .col(ColumnDef::new(EmailConfig::AppPassword).string().not_null())
                    .col(ColumnDef::new(EmailConfig::SmtpServer).string().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(EmailConfig::Table).to_owned())
            .await
    }
}

// for ease of access
#[derive(Iden)]
pub enum EmailConfig {
    Table,
    Id,
    Title,
    Email,
    AppPassword,
    SmtpServer,
}
