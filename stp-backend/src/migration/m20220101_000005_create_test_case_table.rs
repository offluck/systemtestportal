use sea_orm_migration::prelude::*;

use super::m20220101_000004_create_test_sequence_table::TestSequence;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(TestCase::Table)
                    .col(
                        ColumnDef::new(TestCase::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(TestCase::Name).string().not_null())
                    .col(ColumnDef::new(TestCase::Description).string())
                    .col(ColumnDef::new(TestCase::Precondition).string())
                    .col(
                        ColumnDef::new(TestCase::TestSequenceIndex)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCase::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(TestCase::TestSequenceId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(TestCase::Table, TestCase::TestSequenceId)
                            .to(TestSequence::Table, TestSequence::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    // Create unique constraint on test_sequence_id + test_sequence_index
                    // to avoid assigning the same index multiple times for a test sequence
                    .index(
                        Index::create()
                            .table(TestCase::Table)
                            .col(TestCase::TestSequenceId)
                            .col(TestCase::TestSequenceIndex)
                            .unique(),
                    )
                    .index(
                        Index::create()
                            .table(TestCase::Table)
                            .col(TestCase::Name)
                            .col(TestCase::TestSequenceId)
                            .unique(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(TestCase::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum TestCase {
    Table,
    Id,
    Name,
    Description,
    Precondition,
    TestSequenceIndex,
    CreatedAt,
    TestSequenceId,
}
