use cache::AppCache;
use std::{process, thread, time::Duration};

use crate::config::load_app_config;
use actix_cors::Cors;
use actix_web::{middleware, web::Data, App, HttpServer};
use sea_orm::{Database, DatabaseConnection};
use sea_orm_migration::MigratorTrait;

use crate::migration::Migrator;
use crate::route::init_services;

use config::AppConfig;

use log::{error, info};

use lazy_static::lazy_static;

mod cache;
#[cfg(test)]
mod cache_test;
mod config;
mod dto;
mod email;
mod entity;
mod migration;
mod route;
mod service;
mod util;

#[derive(Debug, Clone)]
pub struct AppState {
    db_conn: DatabaseConnection,
    app_config: AppConfig,
}

lazy_static! {
    pub static ref CACHE: AppCache = AppCache::new();
}

fn main() {
    let result = start_server();
    if let Some(err) = result.err() {
        error!("Failed to start application!\nError: {err}");
        process::exit(1);
    }
}

#[actix_web::main]
async fn start_server() -> std::io::Result<()> {
    // set `warn` as default log-level, only logs warnings + errors
    // for actix-web: log-level is set to `info` to enable endpoint logging
    std::env::set_var("RUST_LOG", "info,actix_web=info");
    // initialize env_logger to enable logging
    env_logger::init();
    info!("Logger is set up");

    let app_config = load_app_config();
    info!("Config is loaded");

    // establish connection to database
    let db_conn = Database::connect(&app_config.db_url)
        .await
        .expect("Failed to connect to database. Does it exist in the DBMS?");
    // apply migrations
    Migrator::up(&db_conn, None)
        .await
        .expect("Failed to apply migrations.");
    info!("Applied database migrations.");

    // build app state
    let state = AppState {
        db_conn,
        app_config: app_config.clone(),
    };

    // Creating a task which clears cahce everyday.
    tokio::spawn(async {
        loop {
            CACHE.async_clear_disabled_users_cache().await;
            thread::sleep(Duration::from_secs(24 * 60 * 60));
        }
    });

    // create server
    info!("Starting server at {}", &app_config.server_url);
    HttpServer::new(move || {
        App::new()
            .wrap(
                Cors::permissive(), // .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
                                    // .allowed_headers(vec![http::header::ACCEPT, http::header::CONTENT_TYPE]),
            )
            .app_data(Data::new(state.clone()))
            .wrap(middleware::Logger::default())
            .configure(init_services)
    })
    .bind(&app_config.server_url)?
    .run()
    .await?;

    Ok(())
}
