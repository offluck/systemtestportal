use sea_orm::prelude::TimeDateTimeWithTimeZone;
use sha256::digest;

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

pub fn get_current_datetime_with_timezone_utc() -> TimeDateTimeWithTimeZone {
    // If you change the implementation, don't forget to also regenerate the entities with sea-orm
    // cli with the appropriate --date-time-crate argument

    // implementation with chrono lib
    // -> returns data type DateTimeWithTimeZone (sea-orm alias for chrono::DateTime<FixedOffset>)
    // chrono::Utc::now().with_timezone(&FixedOffset::east_opt(0).expect("Offset 0 will never be out of bounds!"))

    // implementation with time lib -> TimeDateTimeWithTimeZone (sea-orm alias for time::OffsetDateTime )
    time::OffsetDateTime::now_utc()
}

pub fn hash_string(string: String, pepper: &str) -> String {
    digest(string + pepper)
}

pub fn get_password_reset_token(length: usize) -> String {
    let random_token: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect();

    random_token
}
