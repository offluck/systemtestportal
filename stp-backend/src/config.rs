use dotenvy::dotenv;
use std::env;

#[derive(Debug, Default, Clone)]
pub struct AppConfig {
    pub db_url: String,
    pub frontend_url: String,
    pub host: String,
    pub port: String,
    pub server_url: String,
    pub pepper: String,
    pub jwt_secret: String,
    pub token_expiration: u32,
}

impl AppConfig {
    fn new(
        db_url: String,
        frontend_url: String,
        host: String,
        port: String,
        pepper: String,
        jwt_secret: String,
        token_expiration: u32,
    ) -> Self {
        Self {
            db_url,
            frontend_url,
            server_url: format!("{}:{}", host, port),
            host,
            port,
            pepper,
            jwt_secret,
            token_expiration,
        }
    }
}

pub fn load_app_config() -> AppConfig {
    // initialize config vars from .env file
    dotenv().ok();

    let db_url = env::var("DATABASE_URL").expect("Failed to read database URL from .env file.");
    let frontend_url =
        env::var("FRONTEND_URL").expect("Failed to read frontend URL from .env file.");
    let host: String = env::var("HOST").expect("Failed to read host from .env file.");
    let port = env::var("PORT").expect("Failed to read port from .env file.");
    let pepper = env::var("PEPPER").expect("Failed to read pepper value from .env file.");
    let jwt_secret =
        env::var("JWT_SECRET").expect("Failed to read jwt_secret value from .env file.");
    let token_expiration = env::var("TOKEN_EXPIRATION_TIME_MINUTES")
        .expect("Failed to read token_expiration_time_minutes value from .env file.")
        .parse::<u32>()
        .unwrap();

    AppConfig::new(
        db_url,
        frontend_url,
        host,
        port,
        pepper,
        jwt_secret,
        token_expiration,
    )
}
