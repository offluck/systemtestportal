use actix_web::{
    post,
    web::{Data, Json},
    HttpResponse,
};
use serde_json::json;

use crate::{
    dto::app_user_activation_dto::AppUserActivationToken,
    service::{app_user_activation_service, app_user_service},
    AppState,
};

#[post("/activate")]
pub async fn activate_user_account(
    app_state: Data<AppState>,
    activation_token: Json<AppUserActivationToken>,
) -> HttpResponse {
    let db_conn: &sea_orm::DatabaseConnection = &app_state.db_conn;
    let token = &activation_token.activation_token;
    let activation_result =
        app_user_activation_service::find_single_activation_token(db_conn, token.clone()).await;

    match activation_result {
        Ok(activation_data) => {
            let update_result =
                app_user_service::activate_app_user(db_conn, activation_data.app_user_id).await;
            match update_result {
                Ok(_) => {
                    let _ = app_user_activation_service::delete_app_user_activation_by_token(
                        db_conn,
                        token.to_string(),
                    )
                    .await;
                    HttpResponse::Ok().json(json!({"status": "success"}))
                }
                Err(_err) => HttpResponse::InternalServerError()
                    .json(json!({"status": "failed", "error": "failed updating user"})),
            }
        }
        Err(_err) => HttpResponse::BadRequest()
            .json(json!({"status": "failed", "error": "provided token does not exist"})),
    }
}
