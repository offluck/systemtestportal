use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::product_version;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::product_version_dto::{
        ProductVersionGetDto, ProductVersionPostDto, ProductVersionUpdateDto,
    },
    service::{jwt_auth_service, product_version_service},
    AppState,
};

#[derive(Deserialize)]
pub struct ProductIdQueryParam {
    product_id: Option<i32>,
}

#[get("/versions")]
pub async fn get_all_product_versions(
    app_state: Data<AppState>,
    product_query_param: Query<ProductIdQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_product_versions = product_version_service::find_all_product_versions(
        &app_state.db_conn,
        product_query_param.product_id,
    )
    .await;

    get_all_response::<product_version::Model, ProductVersionGetDto>(found_product_versions)
}

#[get("/versions/{product_version_id}")]
pub async fn get_single_product_version(
    app_state: Data<AppState>,
    version_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_product_version = product_version_service::find_single_product_version(
        &app_state.db_conn,
        version_id.into_inner(),
    )
    .await;

    get_single_response::<product_version::Model, ProductVersionGetDto>(found_product_version)
}

#[post("/versions")]
pub async fn add_product_version(
    app_state: Data<AppState>,
    new_product_version: Json<ProductVersionPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_product_version = product_version_service::save_product_version(
        &app_state.db_conn,
        new_product_version.into_inner(),
    )
    .await;

    post_response::<product_version::Model, ProductVersionGetDto>(added_product_version)
}

#[put("/versions/{product_version_id}")]
pub async fn update_product_version(
    app_state: Data<AppState>,
    version_id: Path<i32>,
    updated_product_version: Json<ProductVersionUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_product_version = product_version_service::update_product_version(
        &app_state.db_conn,
        version_id.into_inner(),
        updated_product_version.into_inner(),
    )
    .await;

    put_response::<product_version::Model, ProductVersionGetDto>(updated_product_version)
}

#[delete("/versions/{version_id}")]
pub async fn delete_product_version(
    app_state: Data<AppState>,
    version_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result =
        product_version_service::delete_product_version(db_conn, version_id.into_inner()).await;
    delete_response(delete_result)
}
