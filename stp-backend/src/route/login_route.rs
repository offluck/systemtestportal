use actix_web::{
    get, post,
    web::{Data, Json},
    Error, HttpResponse,
};
use log::debug;
use serde_json::json;

use crate::service::jwt_auth_service::{create_cookie, create_jwt_cookie, JWT_COOKIE_NAME};
use crate::{
    dto::{app_user_dto::AppUserGetDto, login_credentials::LoginCredentials},
    util::hash_string,
};
use crate::{service::app_user_service, AppState};

const MAXIMUM_NUMBER_OF_FAILED_ATTEMPTS: u64 = 3;

#[post("auth/login")]
pub async fn login_user(
    app_state: Data<AppState>,
    login_credentials: Json<LoginCredentials>,
) -> Result<HttpResponse, Error> {
    let email = login_credentials.email.to_owned();

    debug!("Before lock");
    let mut disabled_users = crate::CACHE.disabled_users.lock().await;
    debug!("After lock");

    if let Some(attempts) = disabled_users.get(&email) {
        if *attempts > MAXIMUM_NUMBER_OF_FAILED_ATTEMPTS {
            return Err(actix_web::error::ErrorForbidden(
                "Too many bad incorrect password attempts",
            ));
        }
    }

    let password = login_credentials.password.to_owned();

    let db_conn = &app_state.db_conn;
    let config = &app_state.app_config;

    let found_app_user =
        match app_user_service::find_single_app_user_by_email_and_enabled(db_conn, &email).await? {
            Some(user) => user,
            None => {
                return Err(actix_web::error::ErrorUnauthorized(
                    "Cannot find user with given login.",
                ));
            }
        };

    if found_app_user.password_hash != hash_string(password, &config.pepper) {
        let entry = disabled_users.entry(email);
        match entry {
            std::collections::hash_map::Entry::Occupied(mut val) => {
                *val.get_mut() += 1;
            }

            std::collections::hash_map::Entry::Vacant(val) => {
                val.insert(0);
            }
        };

        return Err(actix_web::error::ErrorUnauthorized("Wrong email."));
    }

    let id = found_app_user.id;
    let jwt_secret = &app_state.app_config.jwt_secret;
    let lifetime_hours = 1;

    Ok(HttpResponse::Ok()
        .cookie(create_jwt_cookie(id, jwt_secret, lifetime_hours))
        .json(AppUserGetDto::from(found_app_user)))
}

#[get("/auth/logout")]
pub(crate) async fn logout_user() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok()
        .cookie(create_cookie(JWT_COOKIE_NAME, "".to_string(), -1))
        .json(json!({"status": "success"})))
}

#[cfg(test)]
mod tests {
    use actix_web::{cookie::Cookie, http, test, App};
    use serde_json::json;

    use crate::{route::login_route::logout_user, service::jwt_auth_service::JWT_COOKIE_NAME};

    #[actix_web::test]
    async fn test_logout_user() {
        let mut app = test::init_service(App::new().service(logout_user)).await;

        let req = test::TestRequest::get().uri("/auth/logout").to_request();
        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), http::StatusCode::OK);

        let cookies: Vec<Cookie<'_>> = resp.response().cookies().collect();
        assert_eq!(cookies.len(), 1);

        let cookie = cookies.get(0).unwrap();
        assert_eq!(cookie.name(), JWT_COOKIE_NAME);
        assert_eq!(cookie.value(), "");
        // Can't assert cookie.max_age(), because it returns a Duration of 0 even though the cookie string contains "Max-Age=-3600"
        assert!(cookie.http_only().unwrap());

        let body = test::read_body(resp).await;
        let json_data = serde_json::from_slice::<serde_json::Value>(&body).unwrap();
        assert_eq!(json_data, json!({"status": "success"}));
    }
}
