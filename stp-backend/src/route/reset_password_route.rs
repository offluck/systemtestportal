use actix_web::{
    post,
    web::{Data, Json, Query},
    Error, HttpResponse,
};
use chrono::prelude::*;
use log::debug;
use serde_json::json;

use crate::{
    cache::Token,
    dto::app_user_dto::AppUserUpdateDto,
    dto::{email::Email, password_token::PasswordToken},
    email::email_type::PasswordResetData,
    email::{email_service::send, email_type::EmailType},
    util::{get_password_reset_token, hash_string},
};
use crate::{service::app_user_service, AppState};

#[post("/pwd_reset_request")]
pub async fn send_email(
    app_state: Data<AppState>,
    email_parameter: Query<Email>,
) -> Result<HttpResponse, Error> {
    let email = email_parameter.email.clone();

    if email.is_empty() {
        return Err(actix_web::error::ErrorBadRequest(
            "Email parameter is empty.",
        ));
    }

    debug!("Before lock");
    let mut users_tokens = crate::CACHE.users_tokens.lock().await;
    debug!("After lock");

    let db_conn = &app_state.db_conn;
    let config = &app_state.app_config;

    let token_validity_min = config.token_expiration as i64;

    let found_app_user =
        match app_user_service::find_single_app_user_by_email_and_enabled(db_conn, &email).await? {
            Some(user) => user,
            None => {
                return Err(actix_web::error::ErrorNotFound(
                    "Cannot find user with given e-mail.",
                ));
            }
        };

    let now = Utc::now().timestamp();
    let reset_token: String = get_password_reset_token(64);
    let hashed_token: String = hash_string(reset_token.clone(), &config.pepper);

    let token_to_store: Token = Token {
        user_id: found_app_user.id,
        validity: (now + (token_validity_min * 60)),
    };

    let entry = users_tokens.entry(hashed_token.clone());
    match entry {
        std::collections::hash_map::Entry::Occupied(_val) => {
            // should not happen if idea of hashing is correct :)
        }
        std::collections::hash_map::Entry::Vacant(val) => {
            val.insert(token_to_store);
        }
    };

    let mut reset_link = config.frontend_url.clone();
    reset_link.push_str("/reset-password?token=");
    reset_link.push_str(&reset_token);

    let token_expiration_h: i32 = token_validity_min as i32 / 60;
    let password_reset_data = PasswordResetData {
        reset_url: reset_link,
        hours_expiration: token_expiration_h,
    };

    send(
        EmailType::PasswordReset(password_reset_data),
        email,
        db_conn,
    )
    .await;

    Ok(HttpResponse::Ok().json(json!({"status": "success"})))
}

#[post("/set_password")]
pub async fn set_password(
    app_state: Data<AppState>,
    password_token: Json<PasswordToken>,
) -> Result<HttpResponse, Error> {
    let db_conn = &app_state.db_conn;
    let config = &app_state.app_config;

    // check password for strength ?
    if password_token.password.is_empty() {
        return Err(actix_web::error::ErrorBadRequest(
            "Password parameter is not set.",
        ));
    }

    debug!("Before lock");
    let mut users_tokens = crate::CACHE.users_tokens.lock().await;
    debug!("After lock");

    let input_token = password_token.token.to_owned();
    let hashed_input_token: String = hash_string(input_token.clone(), &config.pepper);

    let token_data = match users_tokens.get(&hashed_input_token) {
        Some(token) => token,
        None => {
            return Err(actix_web::error::ErrorNotFound("Token is not valid."));
        }
    };

    let now = Utc::now().timestamp();
    let token_lifetime = token_data.validity;

    if token_lifetime < now {
        let delete_token = users_tokens.remove(&hashed_input_token);
        match delete_token {
            Some(_token) => debug!("Token successfully removed."),
            None => debug!("Is not possible to delete token."),
        }

        return Err(actix_web::error::ErrorNotFound("Token is not valid."));
    }

    let found_app_user =
        match app_user_service::find_single_app_user(db_conn, token_data.user_id).await {
            Ok(user) => user,
            Err(_e) => {
                return Err(actix_web::error::ErrorNotFound(
                    "User with this id is not in the database.",
                ));
            }
        };

    let updated_app_user: AppUserUpdateDto = AppUserUpdateDto {
        app_role_id: found_app_user.app_role_id,
        password: "".to_string(),
        new_password: Some(password_token.password.clone()),
        biography: found_app_user.biography,
        email: found_app_user.email,
        image_path: found_app_user.image_path,
        name: found_app_user.name,
    };

    match app_user_service::update_app_user(
        db_conn,
        token_data.user_id,
        updated_app_user,
        &config.pepper,
    )
    .await
    {
        Ok(_user) => {
            let delete_token = users_tokens.remove(&hashed_input_token);
            match delete_token {
                Some(_token) => debug!("Token successfully removed."),
                None => debug!("Is not possible to delete token."),
            };
            Ok(HttpResponse::Ok().finish())
        }
        Err(_e) => Err(actix_web::error::ErrorInternalServerError(
            "User's password was not set.",
        )),
    }
}
