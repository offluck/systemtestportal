use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path},
    HttpResponse,
};

use crate::entity::execution_status;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::execution_status_dto::{
        ExecutionStatusGetDto, ExecutionStatusPostDto, ExecutionStatusUpdateDto,
    },
    service::{execution_status_service, jwt_auth_service},
    AppState,
};

#[get("/execution-statuses")]
pub async fn get_all_execution_statuses(app_state: Data<AppState>) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = execution_status_service::find_all_execution_statuses(db_conn).await;
    get_all_response::<execution_status::Model, ExecutionStatusGetDto>(query_result)
}

#[get("/execution-statuses/{id}")]
pub async fn get_single_execution_status(
    app_state: Data<AppState>,
    execution_status_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = execution_status_service::find_single_execution_status(
        db_conn,
        execution_status_id.into_inner(),
    )
    .await;
    get_single_response::<execution_status::Model, ExecutionStatusGetDto>(query_result)
}

#[post("/execution-statuses")]
pub async fn add_execution_status(
    app_state: Data<AppState>,
    new_execution_status: Json<ExecutionStatusPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        execution_status_service::save_execution_status(db_conn, new_execution_status.into_inner())
            .await;
    post_response::<execution_status::Model, ExecutionStatusGetDto>(query_result)
}

#[put("/execution-statuses/{id}")]
pub async fn update_execution_status(
    app_state: Data<AppState>,
    execution_status_id: Path<i32>,
    updated_execution_status: Json<ExecutionStatusUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let updated_execution_status = execution_status_service::update_execution_status(
        db_conn,
        execution_status_id.into_inner(),
        updated_execution_status.into_inner(),
    )
    .await;

    put_response::<execution_status::Model, ExecutionStatusGetDto>(updated_execution_status)
}

#[delete("/execution-statuses/{id}")]
pub async fn delete_execution_status(
    app_state: Data<AppState>,
    execution_status_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result = execution_status_service::delete_execution_status(
        db_conn,
        execution_status_id.into_inner(),
    )
    .await;
    delete_response(delete_result)
}
