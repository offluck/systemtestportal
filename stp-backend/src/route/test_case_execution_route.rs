use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_case_execution;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_case_execution_dto::{
        TestCaseExecutionGetDto, TestCaseExecutionPostDto, TestCaseExecutionUpdateDto,
    },
    service::{jwt_auth_service, test_case_execution_service},
    AppState,
};

#[derive(Deserialize)]
pub struct TestCaseExecutionQueryParams {
    test_case_id: Option<i32>,
    test_sequence_execution_id: Option<i32>,
    execution_status_id: Option<i32>,
}

#[get("/test-case-executions")]
pub async fn get_all_test_case_executions(
    app_state: Data<AppState>,
    test_case_execution_query_params: Query<TestCaseExecutionQueryParams>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_case_executions = test_case_execution_service::find_all_test_case_executions(
        &app_state.db_conn,
        test_case_execution_query_params.test_case_id,
        test_case_execution_query_params.test_sequence_execution_id,
        test_case_execution_query_params.execution_status_id,
    )
    .await;

    get_all_response::<test_case_execution::Model, TestCaseExecutionGetDto>(
        found_test_case_executions,
    )
}

#[get("/test-case-executions/{test_case_execution_id}")]
pub async fn get_single_test_case_execution(
    app_state: Data<AppState>,
    test_case_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_case_execution = test_case_execution_service::find_single_test_case_execution(
        &app_state.db_conn,
        test_case_execution_id.into_inner(),
    )
    .await;

    get_single_response::<test_case_execution::Model, TestCaseExecutionGetDto>(
        found_test_case_execution,
    )
}

#[post("/test-case-executions")]
pub async fn add_test_case_execution(
    app_state: Data<AppState>,
    new_test_case_execution: Json<TestCaseExecutionPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_case_execution = test_case_execution_service::save_test_case_execution(
        &app_state.db_conn,
        new_test_case_execution.into_inner(),
    )
    .await;

    post_response::<test_case_execution::Model, TestCaseExecutionGetDto>(added_test_case_execution)
}

#[put("/test-case-executions/{test_case_execution_id}")]
pub async fn update_test_case_execution(
    app_state: Data<AppState>,
    test_case_execution_id: Path<i32>,
    updated_test_case_execution: Json<TestCaseExecutionUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_case_execution = test_case_execution_service::update_test_case_execution(
        &app_state.db_conn,
        test_case_execution_id.into_inner(),
        updated_test_case_execution.into_inner(),
    )
    .await;

    put_response::<test_case_execution::Model, TestCaseExecutionGetDto>(updated_test_case_execution)
}

#[delete("/test-case-executions/{test_case_execution_id}")]
pub async fn delete_test_case_execution(
    app_state: Data<AppState>,
    test_case_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result = test_case_execution_service::delete_test_case_execution(
        &app_state.db_conn,
        test_case_execution_id.into_inner(),
    )
    .await;
    delete_response(delete_result)
}
