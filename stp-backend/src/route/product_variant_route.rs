use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::product_variant;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::product_variant_dto::{
        ProductVariantGetDto, ProductVariantPostDto, ProductVariantUpdateDto,
    },
    service::{jwt_auth_service, product_variant_service},
    AppState,
};

#[derive(Deserialize)]
pub struct ProductVersionIdQueryParam {
    product_version_id: Option<i32>,
}

#[get("/variants")]
pub async fn get_all_product_variants(
    app_state: Data<AppState>,
    product_version_query_param: Query<ProductVersionIdQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let found_product_variants = product_variant_service::find_all_product_variants(
        db_conn,
        product_version_query_param.product_version_id,
    )
    .await;

    get_all_response::<product_variant::Model, ProductVariantGetDto>(found_product_variants)
}

#[get("/variants/{variant_id}")]
pub async fn get_single_product_variant(
    app_state: Data<AppState>,
    variant_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let found_product_variant =
        product_variant_service::find_single_product_variant(db_conn, variant_id.into_inner())
            .await;

    get_single_response::<product_variant::Model, ProductVariantGetDto>(found_product_variant)
}

#[post("/variants")]
pub async fn add_product_variant(
    app_state: Data<AppState>,
    new_product_variant: Json<ProductVariantPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let added_product_variant =
        product_variant_service::save_product_variant(db_conn, new_product_variant.into_inner())
            .await;

    post_response::<product_variant::Model, ProductVariantGetDto>(added_product_variant)
}

#[put("/variants/{variant_id}")]
pub async fn update_product_variant(
    app_state: Data<AppState>,
    variant_id: Path<i32>,
    updated_product_variant: Json<ProductVariantUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let updated_product_variant = product_variant_service::update_product_variant(
        db_conn,
        variant_id.into_inner(),
        updated_product_variant.into_inner(),
    )
    .await;

    put_response::<product_variant::Model, ProductVariantGetDto>(updated_product_variant)
}

#[delete("/variants/{variant_id}")]
pub async fn delete_product_variant(
    app_state: Data<AppState>,
    variant_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result =
        product_variant_service::delete_product_variant(db_conn, variant_id.into_inner()).await;
    delete_response(delete_result)
}
