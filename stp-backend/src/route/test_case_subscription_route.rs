use actix_web::{
    delete, get, post,
    web::{Data, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::dto::app_user_dto::SubscribedUserGetDto;
use crate::dto::test_case_subscription_dto::{
    TestCaseSubscriptionGetDto, TestCaseSubscriptionPostDto,
};
use crate::entity::{app_user, test_case_subscription};
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::service::{jwt_auth_service, test_case_subscription_service};
use crate::AppState;

#[derive(Deserialize)]
pub struct TestCaseSubscriptionQueryParam {
    app_user_id: Option<i32>,
}

#[get("/test-cases/{test_case_id}/subscribed-users/{user_id}")]
pub async fn get_single_test_case_subscription(
    app_state: Data<AppState>,
    param: Path<(i32, i32)>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let (test_case_id, user_id) = param.into_inner();

    let found_subscribed_user = test_case_subscription_service::find_single_test_case_subscription(
        db_conn,
        test_case_id,
        user_id,
    )
    .await;

    get_single_response::<app_user::Model, SubscribedUserGetDto>(found_subscribed_user)
}

#[get("/test-cases/{test_case_id}/subscribed-users")]
pub async fn get_all_test_case_subscriptions(
    app_state: Data<AppState>,
    test_case_id: Path<i32>,
    test_case_subscription_query_param: Query<TestCaseSubscriptionQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let found_subscribed_users = test_case_subscription_service::find_all_test_case_subscriptions(
        db_conn,
        test_case_id.into_inner(),
        test_case_subscription_query_param.app_user_id,
    )
    .await;

    get_all_response::<app_user::Model, SubscribedUserGetDto>(found_subscribed_users)
}

#[post("/test-cases/{test_case_id}/subscribed-users/{user_id}")]
pub async fn add_test_case_subscription(
    app_state: Data<AppState>,
    param: Path<(i32, i32)>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let (test_case_id, user_id) = param.into_inner();

    let new_test_case_subscription = TestCaseSubscriptionPostDto {
        app_user_id: user_id,
        test_case_id,
    };

    let added_test_case_subscription = test_case_subscription_service::save_test_case_subscription(
        db_conn,
        new_test_case_subscription,
    )
    .await;

    post_response::<test_case_subscription::Model, TestCaseSubscriptionGetDto>(
        added_test_case_subscription,
    )
}

#[delete("/test-cases/{test_case_id}/subscribed-users/{user_id}")]
pub async fn delete_test_case_subscription(
    app_state: Data<AppState>,
    param: Path<(i32, i32)>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let (test_case_id, user_id) = param.into_inner();

    let result = test_case_subscription_service::delete_test_case_subscription(
        db_conn,
        test_case_id,
        user_id,
    )
    .await;
    delete_response(result)
}
