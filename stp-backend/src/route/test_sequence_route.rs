use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_sequence;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_sequence_dto::{TestSequenceGetDto, TestSequencePostDto, TestSequenceUpdateDto},
    service::{jwt_auth_service, test_sequence_service},
    AppState,
};

#[derive(Deserialize)]
pub struct ProductVariantIdQueryParam {
    product_variant_id: Option<i32>,
}

#[get("/test-sequences")]
pub async fn get_all_test_sequences(
    app_state: Data<AppState>,
    product_variant_query_param: Query<ProductVariantIdQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_sequences = test_sequence_service::find_all_test_sequences(
        &app_state.db_conn,
        product_variant_query_param.product_variant_id,
    )
    .await;

    get_all_response::<test_sequence::Model, TestSequenceGetDto>(found_test_sequences)
}

#[get("/test-sequences/{test_sequence_id}")]
pub async fn get_single_test_sequence(
    app_state: Data<AppState>,
    test_sequence_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_sequence = test_sequence_service::find_single_test_sequence(
        &app_state.db_conn,
        test_sequence_id.into_inner(),
    )
    .await;

    get_single_response::<test_sequence::Model, TestSequenceGetDto>(found_test_sequence)
}

#[post("/test-sequences")]
pub async fn add_test_sequence(
    app_state: Data<AppState>,
    new_test_sequence: Json<TestSequencePostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_sequence = test_sequence_service::save_test_sequence(
        &app_state.db_conn,
        new_test_sequence.into_inner(),
    )
    .await;

    post_response::<test_sequence::Model, TestSequenceGetDto>(added_test_sequence)
}

#[put("/test-sequences/{test_sequence_id}")]
pub async fn update_test_sequence(
    app_state: Data<AppState>,
    test_sequence_id: Path<i32>,
    updated_test_sequence: Json<TestSequenceUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_sequence = test_sequence_service::update_test_sequence(
        &app_state.db_conn,
        test_sequence_id.into_inner(),
        updated_test_sequence.into_inner(),
    )
    .await;

    put_response::<test_sequence::Model, TestSequenceGetDto>(updated_test_sequence)
}

#[delete("/test-sequences/{test_sequence_id}")]
pub async fn delete_test_sequence(
    app_state: Data<AppState>,
    test_sequence_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result = test_sequence_service::delete_test_sequence(
        &app_state.db_conn,
        test_sequence_id.into_inner(),
    )
    .await;
    delete_response(delete_result)
}
