use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_step_execution;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_step_execution_dto::{
        TestStepExecutionGetDto, TestStepExecutionPostDto, TestStepExecutionUpdateDto,
    },
    service::{jwt_auth_service, test_step_execution_service},
    AppState,
};

#[derive(Deserialize)]
pub struct TestStepExecutionQueryParams {
    test_step_id: Option<i32>,
    test_case_execution_id: Option<i32>,
    execution_status_id: Option<i32>,
}

#[get("/test-step-executions")]
pub async fn get_all_test_step_executions(
    app_state: Data<AppState>,
    test_step_execution_query_params: Query<TestStepExecutionQueryParams>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_step_executions = test_step_execution_service::find_all_test_step_executions(
        &app_state.db_conn,
        test_step_execution_query_params.test_step_id,
        test_step_execution_query_params.test_case_execution_id,
        test_step_execution_query_params.execution_status_id,
    )
    .await;

    get_all_response::<test_step_execution::Model, TestStepExecutionGetDto>(
        found_test_step_executions,
    )
}

#[get("/test-step-executions/{test_step_execution_id}")]
pub async fn get_single_test_step_execution(
    app_state: Data<AppState>,
    test_step_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_step_execution = test_step_execution_service::find_single_test_step_execution(
        &app_state.db_conn,
        test_step_execution_id.into_inner(),
    )
    .await;

    get_single_response::<test_step_execution::Model, TestStepExecutionGetDto>(
        found_test_step_execution,
    )
}

#[post("/test-step-executions")]
pub async fn add_test_step_execution(
    app_state: Data<AppState>,
    new_test_step_execution: Json<TestStepExecutionPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_step_execution = test_step_execution_service::save_test_step_execution(
        &app_state.db_conn,
        new_test_step_execution.into_inner(),
    )
    .await;

    post_response::<test_step_execution::Model, TestStepExecutionGetDto>(added_test_step_execution)
}

#[put("/test-step-executions/{test_step_execution_id}")]
pub async fn update_test_step_execution(
    app_state: Data<AppState>,
    test_step_execution_id: Path<i32>,
    updated_test_step_execution: Json<TestStepExecutionUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_step_execution = test_step_execution_service::update_test_step_execution(
        &app_state.db_conn,
        test_step_execution_id.into_inner(),
        updated_test_step_execution.into_inner(),
    )
    .await;

    put_response::<test_step_execution::Model, TestStepExecutionGetDto>(updated_test_step_execution)
}

#[delete("/test-step-executions/{test_step_execution_id}")]
pub async fn delete_test_step_execution(
    app_state: Data<AppState>,
    test_step_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result = test_step_execution_service::delete_test_step_execution(
        &app_state.db_conn,
        test_step_execution_id.into_inner(),
    )
    .await;

    delete_response(delete_result)
}
