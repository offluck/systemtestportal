use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path},
    HttpResponse,
};

use crate::entity::product::Model;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;

use crate::{
    dto::product_dto::{ProductGetDto, ProductPostDto, ProductUpdateDto},
    service::{jwt_auth_service, product_service},
    AppState,
};

#[get("/products")]
pub async fn get_all_products(
    app_state: Data<AppState>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = product_service::find_all_products(db_conn).await;
    get_all_response::<Model, ProductGetDto>(query_result)
}

#[get("/products/{id}")]
pub async fn get_single_product(
    app_state: Data<AppState>,
    product_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = product_service::find_single_product(db_conn, product_id.into_inner()).await;
    get_single_response::<Model, ProductGetDto>(query_result)
}

#[post("/products")]
pub async fn add_product(
    app_state: Data<AppState>,
    new_product: Json<ProductPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = product_service::save_product(db_conn, new_product.into_inner()).await;
    post_response::<Model, ProductGetDto>(query_result)
}

#[put("/products/{id}")]
pub async fn update_product(
    app_state: Data<AppState>,
    product_id: Path<i32>,
    updated_product: Json<ProductUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let updated_product = product_service::update_product(
        db_conn,
        product_id.into_inner(),
        updated_product.into_inner(),
    )
    .await;

    put_response::<Model, ProductGetDto>(updated_product)
}

#[delete("/products/{id}")]
pub async fn delete_product(
    app_state: Data<AppState>,
    product_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result = product_service::delete_product(db_conn, product_id.into_inner()).await;
    delete_response(delete_result)
}
