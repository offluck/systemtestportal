use actix_web::web::{Data, Json};
use actix_web::{post, HttpResponse};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use sea_orm::DatabaseConnection;
use serde_json::json;

use crate::dto::app_user_activation_dto::AppUserActivationPostDto;
use crate::dto::app_user_dto::AppUserPostDto;
use crate::email::email_service;
use crate::email::email_type::{EmailType, RegistrationConfirmationData};
use crate::entity::app_user_activation::Model;
use crate::service::error::create_error::CreateError;
use crate::service::{app_user_activation_service, app_user_service};
use crate::util::hash_string;
use crate::AppState;

#[post("/register")]
pub async fn register_app_user(
    app_state: Data<AppState>,
    new_app_user: Json<AppUserPostDto>,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let pepper = &app_state.app_config.pepper;
    let frontend_url = &app_state.app_config.frontend_url;

    let new_app_user = new_app_user.into_inner();
    let activation_token = create_token(&new_app_user);

    let user_save_result = app_user_service::save_app_user(db_conn, new_app_user, pepper).await;

    match user_save_result {
        Ok(app_user) => {
            let token_save_result =
                save_activation_token(app_user.id, activation_token.clone(), db_conn).await;
            match token_save_result {
                Ok(_) => {
                    send_email(frontend_url, activation_token, app_user.email, db_conn).await;
                    HttpResponse::Ok().json(json!({"status": "success"}))
                }
                Err(_err) => HttpResponse::InternalServerError()
                    .json(json!({"status": "failed", "error": "could not save token"})),
            }
        }
        Err(_err) => {
            HttpResponse::Conflict().json(json!({"status": "failed", "error": "email is taken"}))
        }
    }
}

fn create_token(new_app_user: &AppUserPostDto) -> String {
    let app_user_string = format!("{:?}", new_app_user);
    let pepper: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .map(char::from)
        .take(10)
        .collect();

    hash_string(app_user_string, &pepper)
}

async fn save_activation_token(
    app_user_id: i32,
    activation_token: String,
    db_conn: &DatabaseConnection,
) -> Result<Model, CreateError> {
    let activation_dto = AppUserActivationPostDto {
        app_user_id,
        activation_token,
    };
    app_user_activation_service::save_app_user_activation(db_conn, activation_dto).await
}

async fn send_email(
    frontend_url: &String,
    activation_token: String,
    email: String,
    db_conn: &DatabaseConnection,
) {
    let confirmation_url = format!("{}/activate?token={}", frontend_url, &activation_token);
    let confirmation_data = RegistrationConfirmationData {
        user_recipient: email.clone(),
        confirmation_url,
    };

    email_service::send(
        EmailType::RegistrationConfirmation(confirmation_data),
        email,
        db_conn,
    )
    .await
}
