use actix_web::{http::header::HeaderMap, Error};

#[allow(dead_code)]
pub fn get_info_from_header(header_map: &HeaderMap, key: &str) -> Result<String, Error> {
    match header_map.get(key) {
        Some(header) => Ok(header.to_str().unwrap().to_owned()),
        None => Err(actix_web::error::ErrorBadRequest(format!(
            "{} {}.",
            "Cannot find header", key
        ))),
    }
}
