use crate::dto::app_mail_template_dto::{
    EmailTemplateGetDto, EmailTemplatePostDto, EmailTemplatePutDto,
};
use crate::entity::email_template;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::AppState;
use actix_web::web::Path;
use actix_web::{
    delete, get, post, put,
    web::{Data, Json},
    HttpResponse,
};

use crate::service::{email_template_service, jwt_auth_service};

#[get("/email-templates")]
pub async fn get_all_email_templates(
    app_state: Data<AppState>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = email_template_service::find_all_email_templates(db_conn).await;
    get_all_response::<email_template::Model, EmailTemplateGetDto>(query_result)
}

#[get("/email-templates/{id}")]
pub async fn get_single_email_template(
    app_state: Data<AppState>,
    email_template_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        email_template_service::find_single_email_template(db_conn, email_template_id.into_inner())
            .await;
    get_single_response::<email_template::Model, EmailTemplateGetDto>(query_result)
}

#[post("/email-templates")]
pub async fn add_email_template(
    app_state: Data<AppState>,
    email_template: Json<EmailTemplatePostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let saved_email_template =
        email_template_service::save_email_template(db_conn, email_template.into_inner()).await;
    post_response::<email_template::Model, EmailTemplateGetDto>(saved_email_template)
}

#[put("/email-templates/{id}")]
pub async fn update_email_template(
    app_state: Data<AppState>,
    email_template_id: Path<i32>,
    updated_email_template: Json<EmailTemplatePutDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let email_template_after_update = email_template_service::update_email_template(
        db_conn,
        email_template_id.into_inner(),
        updated_email_template.into_inner(),
    )
    .await;

    put_response::<email_template::Model, EmailTemplateGetDto>(email_template_after_update)
}

#[delete("/email-templates/{id}")]
pub async fn delete_email_template(
    app_state: Data<AppState>,
    email_template_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result =
        email_template_service::delete_email_template(db_conn, email_template_id.into_inner())
            .await;
    delete_response(delete_result)
}
