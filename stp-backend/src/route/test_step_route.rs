use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_step;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_step_dto::{TestStepGetDto, TestStepPostDto, TestStepUpdateDto},
    service::{jwt_auth_service, test_step_service},
    AppState,
};

#[derive(Deserialize)]
pub struct TestCaseIdQueryParam {
    test_case_id: Option<i32>,
}

#[get("/test-steps")]
pub async fn get_all_test_steps(
    app_state: Data<AppState>,
    test_case_query_param: Query<TestCaseIdQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_steps = test_step_service::find_all_test_steps(
        &app_state.db_conn,
        test_case_query_param.test_case_id,
    )
    .await;

    get_all_response::<test_step::Model, TestStepGetDto>(found_test_steps)
}

#[get("/test-steps/{test_step_id}")]
pub async fn get_single_test_step(
    app_state: Data<AppState>,
    test_step_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_step =
        test_step_service::find_single_test_step(&app_state.db_conn, test_step_id.into_inner())
            .await;

    get_single_response::<test_step::Model, TestStepGetDto>(found_test_step)
}

#[post("/test-steps")]
pub async fn add_test_step(
    app_state: Data<AppState>,
    new_test_step: Json<TestStepPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_step =
        test_step_service::save_test_step(&app_state.db_conn, new_test_step.into_inner()).await;

    post_response::<test_step::Model, TestStepGetDto>(added_test_step)
}

#[put("/test-steps/{test_step_id}")]
pub async fn update_test_step(
    app_state: Data<AppState>,
    test_step_id: Path<i32>,
    updated_test_step: Json<TestStepUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_step = test_step_service::update_test_step(
        &app_state.db_conn,
        test_step_id.into_inner(),
        updated_test_step.into_inner(),
    )
    .await;

    put_response::<test_step::Model, TestStepGetDto>(updated_test_step)
}

#[delete("/test-steps/{test_step_id}")]
pub async fn delete_test_step(
    app_state: Data<AppState>,
    test_step_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result =
        test_step_service::delete_test_step(&app_state.db_conn, test_step_id.into_inner()).await;
    delete_response(delete_result)
}
