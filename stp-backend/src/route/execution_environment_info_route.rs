use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::execution_environment_info;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::execution_environment_info_dto::{
        ExecutionEnvironmentInfoGetDto, ExecutionEnvironmentInfoPostDto,
        ExecutionEnvironmentInfoUpdateDto,
    },
    service::{execution_environment_info_service, jwt_auth_service},
    AppState,
};

#[derive(Deserialize)]
pub struct ExecutionEnvironmentInfoQueryParams {
    test_sequence_execution_id: Option<i32>,
}

#[get("/execution-environment-info")]
pub async fn get_all_execution_environment_infos(
    app_state: Data<AppState>,
    execution_environment_info_query_params: Query<ExecutionEnvironmentInfoQueryParams>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = execution_environment_info_service::find_all_execution_environment_infos(
        db_conn,
        execution_environment_info_query_params.test_sequence_execution_id,
    )
    .await;
    get_all_response::<execution_environment_info::Model, ExecutionEnvironmentInfoGetDto>(
        query_result,
    )
}

#[get("/execution-environment-info/{execution_environment_info_id}")]
pub async fn get_single_execution_environment_info(
    app_state: Data<AppState>,
    execution_environment_info_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = execution_environment_info_service::find_single_execution_environment_info(
        db_conn,
        execution_environment_info_id.into_inner(),
    )
    .await;
    get_single_response::<execution_environment_info::Model, ExecutionEnvironmentInfoGetDto>(
        query_result,
    )
}

#[post("/execution-environment-info")]
pub async fn add_execution_environment_info(
    app_state: Data<AppState>,
    new_execution_environment_info: Json<ExecutionEnvironmentInfoPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = execution_environment_info_service::save_execution_environment_info(
        db_conn,
        new_execution_environment_info.into_inner(),
    )
    .await;
    post_response::<execution_environment_info::Model, ExecutionEnvironmentInfoGetDto>(query_result)
}

#[put("/execution-environment-info/{execution_environment_info_id}")]
pub async fn update_execution_environment_info(
    app_state: Data<AppState>,
    execution_environment_info_id: Path<i32>,
    updated_execution_environment_info: Json<ExecutionEnvironmentInfoUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let updated_execution_environment_info =
        execution_environment_info_service::update_execution_environment_info(
            db_conn,
            execution_environment_info_id.into_inner(),
            updated_execution_environment_info.into_inner(),
        )
        .await;

    put_response::<execution_environment_info::Model, ExecutionEnvironmentInfoGetDto>(
        updated_execution_environment_info,
    )
}

#[delete("/execution-environment-info/{execution_environment_info_id}")]
pub async fn delete_execution_environment_info(
    app_state: Data<AppState>,
    execution_environment_info_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result = execution_environment_info_service::delete_execution_environment_info(
        db_conn,
        execution_environment_info_id.into_inner(),
    )
    .await;
    delete_response(delete_result)
}
