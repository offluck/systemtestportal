use crate::dto::app_mail_config_dto::{
    AppEmailConfigGetDto, AppEmailConfigPostDto, AppEmailConfigUpdateDto,
};
use crate::entity::email_config;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::AppState;
use actix_web::web::Path;
use actix_web::{
    delete, get, post, put,
    web::{Data, Json},
    HttpResponse,
};

use crate::service::{email_config_service, jwt_auth_service};

#[get("/email-configs")]
pub async fn get_all_email_configs(
    app_state: Data<AppState>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = email_config_service::find_all_email_configs(db_conn).await;
    get_all_response::<email_config::Model, AppEmailConfigGetDto>(query_result)
}

#[get("/email-configs/{id}")]
pub async fn get_single_email_config(
    app_state: Data<AppState>,
    email_config_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        email_config_service::find_single_email_config(db_conn, email_config_id.into_inner()).await;
    get_single_response::<email_config::Model, AppEmailConfigGetDto>(query_result)
}

#[post("/email-configs")]
pub async fn add_email_config(
    app_state: Data<AppState>,
    email_config: Json<AppEmailConfigPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let saved_email_config =
        email_config_service::save_email_config(db_conn, email_config.into_inner()).await;
    post_response::<email_config::Model, AppEmailConfigGetDto>(saved_email_config)
}

#[put("/email-configs/{id}")]
pub async fn update_email_config(
    app_state: Data<AppState>,
    email_config_id: Path<i32>,
    updated_email_config: Json<AppEmailConfigUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let email_config_after_update = email_config_service::update_email_config(
        db_conn,
        email_config_id.into_inner(),
        updated_email_config.into_inner(),
    )
    .await;

    put_response::<email_config::Model, AppEmailConfigGetDto>(email_config_after_update)
}

#[delete("/email-configs/{id}")]
pub async fn delete_email_config(
    app_state: Data<AppState>,
    app_mail_config_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result =
        email_config_service::delete_email_config(db_conn, app_mail_config_id.into_inner()).await;
    delete_response(delete_result)
}
