use crate::route::email_config_route::{
    add_email_config, delete_email_config, get_all_email_configs, get_single_email_config,
    update_email_config,
};
use crate::route::email_template_route::{
    add_email_template, delete_email_template, get_all_email_templates, get_single_email_template,
    update_email_template,
};
use crate::route::test_case_subscription_route::{
    add_test_case_subscription, delete_test_case_subscription, get_all_test_case_subscriptions,
    get_single_test_case_subscription,
};
use actix_web::web::ServiceConfig;

use self::{
    app_role_route::{
        add_app_role, delete_app_role, get_all_app_roles, get_single_app_role, update_app_role,
    },
    app_user_activation_route::activate_user_account,
    app_user_route::{
        add_app_user, delete_app_user, get_all_app_users, get_single_app_user, update_app_user,
    },
    execution_environment_info_route::{
        add_execution_environment_info, delete_execution_environment_info,
        get_all_execution_environment_infos, get_single_execution_environment_info,
        update_execution_environment_info,
    },
    execution_status_route::{
        add_execution_status, delete_execution_status, get_all_execution_statuses,
        get_single_execution_status, update_execution_status,
    },
    login_route::{login_user, logout_user},
    product_route::{
        add_product, delete_product, get_all_products, get_single_product, update_product,
    },
    product_variant_route::{
        add_product_variant, delete_product_variant, get_all_product_variants,
        get_single_product_variant, update_product_variant,
    },
    product_version_route::{
        add_product_version, delete_product_version, get_all_product_versions,
        get_single_product_version, update_product_version,
    },
    registration_route::register_app_user,
    reset_password_route::{send_email, set_password},
    test_case_execution_route::{
        add_test_case_execution, delete_test_case_execution, get_all_test_case_executions,
        get_single_test_case_execution, update_test_case_execution,
    },
    test_case_route::{
        add_test_case, delete_test_case, get_all_test_cases, get_single_test_case, update_test_case,
    },
    test_sequence_execution_route::{
        add_test_sequence_execution, delete_test_sequence_execution,
        get_all_test_sequence_executions, get_single_test_sequence_execution,
        update_test_sequence_execution,
    },
    test_sequence_route::{
        add_test_sequence, delete_test_sequence, get_all_test_sequences, get_single_test_sequence,
        update_test_sequence,
    },
    test_step_execution_route::{
        add_test_step_execution, delete_test_step_execution, get_all_test_step_executions,
        get_single_test_step_execution, update_test_step_execution,
    },
    test_step_route::{
        add_test_step, delete_test_step, get_all_test_steps, get_single_test_step, update_test_step,
    },
};

pub mod login_route;
pub mod product_route;
pub mod product_variant_route;
pub mod product_version_route;
pub mod reset_password_route;
pub mod test_case_route;
pub mod test_sequence_route;
pub mod test_step_route;

pub mod app_role_route;
pub mod app_user_activation_route;
pub mod app_user_route;
pub mod email_config_route;
pub mod email_template_route;
pub mod execution_environment_info_route;
pub mod execution_status_route;
pub mod registration_route;
pub mod response;
pub mod test_case_execution_route;
pub mod test_case_subscription_route;
pub mod test_sequence_execution_route;
pub mod test_step_execution_route;

// register routes
pub fn init_services(cfg: &mut ServiceConfig) {
    register_product_routes(cfg);
    register_product_version_routes(cfg);
    register_product_variant_routes(cfg);
    register_test_sequence_routes(cfg);
    register_test_case_routes(cfg);
    register_test_step_routes(cfg);
    register_app_role_routes(cfg);
    register_app_user_routes(cfg);
    register_execution_status_routes(cfg);
    register_test_sequence_execution_routes(cfg);
    register_test_case_execution_routes(cfg);
    register_test_case_subscription_routes(cfg);
    register_test_step_execution_routes(cfg);
    register_execution_environment_info_routes(cfg);
    register_login_routes(cfg);
    register_reset_password_routes(cfg);
    register_email_config_service_routes(cfg);
    register_email_template_service_routes(cfg);
    register_activation_route(cfg);
    register_registration_route(cfg);
}

fn register_product_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_products);
    cfg.service(get_single_product);
    cfg.service(add_product);
    cfg.service(update_product);
    cfg.service(delete_product);
}

fn register_product_version_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_product_versions);
    cfg.service(get_single_product_version);
    cfg.service(add_product_version);
    cfg.service(update_product_version);
    cfg.service(delete_product_version);
}

fn register_product_variant_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_product_variants);
    cfg.service(get_single_product_variant);
    cfg.service(add_product_variant);
    cfg.service(update_product_variant);
    cfg.service(delete_product_variant);
}

fn register_test_sequence_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_sequences);
    cfg.service(get_single_test_sequence);
    cfg.service(add_test_sequence);
    cfg.service(update_test_sequence);
    cfg.service(delete_test_sequence);
}

fn register_test_case_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_cases);
    cfg.service(get_single_test_case);
    cfg.service(add_test_case);
    cfg.service(update_test_case);
    cfg.service(delete_test_case);
}

fn register_test_step_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_steps);
    cfg.service(get_single_test_step);
    cfg.service(add_test_step);
    cfg.service(update_test_step);
    cfg.service(delete_test_step);
}

fn register_app_role_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_app_roles);
    cfg.service(get_single_app_role);
    cfg.service(add_app_role);
    cfg.service(update_app_role);
    cfg.service(delete_app_role);
}

fn register_app_user_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_app_users);
    cfg.service(get_single_app_user);
    cfg.service(add_app_user);
    cfg.service(update_app_user);
    cfg.service(delete_app_user);
}

fn register_execution_status_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_execution_statuses);
    cfg.service(get_single_execution_status);
    cfg.service(add_execution_status);
    cfg.service(update_execution_status);
    cfg.service(delete_execution_status);
}

fn register_test_sequence_execution_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_sequence_executions);
    cfg.service(get_single_test_sequence_execution);
    cfg.service(add_test_sequence_execution);
    cfg.service(update_test_sequence_execution);
    cfg.service(delete_test_sequence_execution);
}

fn register_test_case_execution_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_case_executions);
    cfg.service(get_single_test_case_execution);
    cfg.service(add_test_case_execution);
    cfg.service(update_test_case_execution);
    cfg.service(delete_test_case_execution);
}

fn register_test_step_execution_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_test_step_executions);
    cfg.service(get_single_test_step_execution);
    cfg.service(add_test_step_execution);
    cfg.service(update_test_step_execution);
    cfg.service(delete_test_step_execution);
}

fn register_execution_environment_info_routes(cfg: &mut ServiceConfig) {
    cfg.service(get_all_execution_environment_infos);
    cfg.service(get_single_execution_environment_info);
    cfg.service(add_execution_environment_info);
    cfg.service(update_execution_environment_info);
    cfg.service(delete_execution_environment_info);
}

fn register_login_routes(cfg: &mut ServiceConfig) {
    cfg.service(login_user);
    cfg.service(logout_user);
}

fn register_reset_password_routes(cfg: &mut ServiceConfig) {
    cfg.service(send_email);
    cfg.service(set_password);
}

fn register_email_config_service_routes(cfg: &mut ServiceConfig) {
    cfg.service(add_email_config);
    cfg.service(get_all_email_configs);
    cfg.service(get_single_email_config);
    cfg.service(update_email_config);
    cfg.service(delete_email_config);
}

fn register_email_template_service_routes(cfg: &mut ServiceConfig) {
    cfg.service(add_email_template);
    cfg.service(get_all_email_templates);
    cfg.service(get_single_email_template);
    cfg.service(update_email_template);
    cfg.service(delete_email_template);
}

fn register_test_case_subscription_routes(cfg: &mut ServiceConfig) {
    cfg.service(add_test_case_subscription);
    cfg.service(get_single_test_case_subscription);
    cfg.service(get_all_test_case_subscriptions);
    cfg.service(delete_test_case_subscription);
}

fn register_activation_route(cfg: &mut ServiceConfig) {
    cfg.service(activate_user_account);
}

fn register_registration_route(cfg: &mut ServiceConfig) {
    cfg.service(register_app_user);
}
