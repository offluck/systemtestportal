use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_case;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_case_dto::{TestCaseGetDto, TestCasePostDto, TestCaseUpdateDto},
    service::{jwt_auth_service, test_case_service},
    AppState,
};

#[derive(Deserialize)]
pub struct TestSequenceIdQueryParam {
    test_sequence_id: Option<i32>,
}

#[get("/test-cases")]
pub async fn get_all_test_cases(
    app_state: Data<AppState>,
    test_sequence_query_param: Query<TestSequenceIdQueryParam>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_cases = test_case_service::find_all_test_cases(
        &app_state.db_conn,
        test_sequence_query_param.test_sequence_id,
    )
    .await;

    get_all_response::<test_case::Model, TestCaseGetDto>(found_test_cases)
}

#[get("/test-cases/{test_case_id}")]
pub async fn get_single_test_case(
    app_state: Data<AppState>,
    test_case_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_case =
        test_case_service::find_single_test_case(&app_state.db_conn, test_case_id.into_inner())
            .await;

    get_single_response::<test_case::Model, TestCaseGetDto>(found_test_case)
}

#[post("/test-cases")]
pub async fn add_test_case(
    app_state: Data<AppState>,
    new_test_case: Json<TestCasePostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_case =
        test_case_service::save_test_case(&app_state.db_conn, new_test_case.into_inner()).await;

    post_response::<test_case::Model, TestCaseGetDto>(added_test_case)
}

#[put("/test-cases/{test_case_id}")]
pub async fn update_test_case(
    app_state: Data<AppState>,
    test_case_id: Path<i32>,
    updated_test_case: Json<TestCaseUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_case = test_case_service::update_test_case(
        &app_state.db_conn,
        test_case_id.into_inner(),
        updated_test_case.into_inner(),
    )
    .await;

    put_response::<test_case::Model, TestCaseGetDto>(updated_test_case)
}

#[delete("/test-cases/{test_case_id}")]
pub async fn delete_test_case(
    app_state: Data<AppState>,
    test_case_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result =
        test_case_service::delete_test_case(&app_state.db_conn, test_case_id.into_inner()).await;
    delete_response(delete_result)
}
