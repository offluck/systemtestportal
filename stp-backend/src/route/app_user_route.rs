use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use serde_json::json;

use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::service::{app_user_service, jwt_auth_service};
use crate::{
    dto::app_user_dto::{AppUserGetDto, AppUserPostDto, AppUserUpdateDto},
    entity::app_user,
    util::hash_string,
    AppState,
};

#[derive(Deserialize)]
pub struct AppRoleIdQueryParam {
    app_role_id: Option<i32>,
}

#[get("/users")]
pub async fn get_all_app_users(
    app_state: Data<AppState>,
    app_role_query_param: Query<AppRoleIdQueryParam>,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        app_user_service::find_all_app_users(db_conn, app_role_query_param.app_role_id).await;
    get_all_response::<app_user::Model, AppUserGetDto>(query_result)
}

#[get("/users/{id}")]
pub async fn get_single_app_user(
    app_state: Data<AppState>,
    app_user_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        app_user_service::find_single_app_user(db_conn, app_user_id.into_inner()).await;
    get_single_response::<app_user::Model, AppUserGetDto>(query_result)
}

#[post("/users")]
pub async fn add_app_user(
    app_state: Data<AppState>,
    new_app_user: Json<AppUserPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let pepper = &app_state.app_config.pepper;

    let saved_app_user =
        app_user_service::save_app_user(db_conn, new_app_user.into_inner(), pepper).await;
    post_response::<app_user::Model, AppUserGetDto>(saved_app_user)
}

#[put("/users/{id}")]
pub async fn update_app_user(
    app_state: Data<AppState>,
    app_user_id: Path<i32>,
    updated_app_user: Json<AppUserUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let user_id = app_user_id.into_inner();
    let pepper = &app_state.app_config.pepper;

    match app_user_service::find_single_app_user(db_conn, user_id).await {
        Ok(found_user) => {
            if found_user.password_hash != hash_string(updated_app_user.password.clone(), pepper) {
                return HttpResponse::Unauthorized()
                    .json(json!({"status": "failed", "error": "given is wrong"}));
            }
            let updated_app_user = app_user_service::update_app_user(
                db_conn,
                user_id,
                updated_app_user.into_inner(),
                pepper,
            )
            .await;

            put_response::<app_user::Model, AppUserGetDto>(updated_app_user)
        }
        Err(_) => HttpResponse::BadRequest()
            .json(json!({"status": "failed", "error": "given user not found"})),
    }
}

#[delete("/users/{id}")]
pub async fn delete_app_user(
    app_state: Data<AppState>,
    app_user_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result = app_user_service::delete_app_user(db_conn, app_user_id.into_inner()).await;
    delete_response(delete_result)
}
