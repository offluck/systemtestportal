use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path},
    HttpResponse,
};

use crate::entity::app_role;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::app_role_dto::{AppRoleGetDto, AppRolePostDto, AppRoleUpdateDto},
    service::{app_role_service, jwt_auth_service},
    AppState,
};

#[get("/app-roles")]
pub async fn get_all_app_roles(
    app_state: Data<AppState>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = app_role_service::find_all_app_roles(db_conn).await;
    get_all_response::<app_role::Model, AppRoleGetDto>(query_result)
}

#[get("/app-roles/{id}")]
pub async fn get_single_app_role(
    app_state: Data<AppState>,
    app_role_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result =
        app_role_service::find_single_app_role(db_conn, app_role_id.into_inner()).await;
    get_single_response::<app_role::Model, AppRoleGetDto>(query_result)
}

#[post("/app-roles")]
pub async fn add_app_role(
    app_state: Data<AppState>,
    new_app_role: Json<AppRolePostDto>,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let query_result = app_role_service::save_app_role(db_conn, new_app_role.into_inner()).await;
    post_response::<app_role::Model, AppRoleGetDto>(query_result)
}

#[put("/app-roles/{id}")]
pub async fn update_app_role(
    app_state: Data<AppState>,
    app_role_id: Path<i32>,
    updated_app_role: Json<AppRoleUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;

    let updated_app_role = app_role_service::update_app_role(
        db_conn,
        app_role_id.into_inner(),
        updated_app_role.into_inner(),
    )
    .await;

    put_response::<app_role::Model, AppRoleGetDto>(updated_app_role)
}

#[delete("/app-roles/{id}")]
pub async fn delete_app_role(
    app_state: Data<AppState>,
    app_role_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let db_conn = &app_state.db_conn;
    let delete_result = app_role_service::delete_app_role(db_conn, app_role_id.into_inner()).await;
    delete_response(delete_result)
}
