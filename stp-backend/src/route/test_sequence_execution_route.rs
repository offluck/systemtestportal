use actix_web::{
    delete, get, post, put,
    web::{Data, Json, Path, Query},
    HttpResponse,
};
use serde::Deserialize;

use crate::entity::test_sequence_execution;
use crate::route::response::delete_response::delete_response;
use crate::route::response::get_response::{get_all_response, get_single_response};
use crate::route::response::post_response::post_response;
use crate::route::response::put_response::put_response;
use crate::{
    dto::test_sequence_execution_dto::{
        TestSequenceExecutionGetDto, TestSequenceExecutionPostDto, TestSequenceExecutionUpdateDto,
    },
    service::{jwt_auth_service, test_sequence_execution_service},
    AppState,
};

#[derive(Deserialize)]
pub struct TestSequenceExecutionQueryParams {
    test_sequence_id: Option<i32>,
    tester_id: Option<i32>,
    execution_status_id: Option<i32>,
}

#[get("/test-sequence-executions")]
pub async fn get_all_test_sequence_executions(
    app_state: Data<AppState>,
    test_sequence_execution_query_params: Query<TestSequenceExecutionQueryParams>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_sequence_executions =
        test_sequence_execution_service::find_all_test_sequence_executions(
            &app_state.db_conn,
            test_sequence_execution_query_params.test_sequence_id,
            test_sequence_execution_query_params.tester_id,
            test_sequence_execution_query_params.execution_status_id,
        )
        .await;

    get_all_response::<test_sequence_execution::Model, TestSequenceExecutionGetDto>(
        found_test_sequence_executions,
    )
}

#[get("/test-sequence-executions/{test_sequence_execution_id}")]
pub async fn get_single_test_sequence_execution(
    app_state: Data<AppState>,
    test_sequence_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let found_test_sequence_execution =
        test_sequence_execution_service::find_single_test_sequence_execution(
            &app_state.db_conn,
            test_sequence_execution_id.into_inner(),
        )
        .await;

    get_single_response::<test_sequence_execution::Model, TestSequenceExecutionGetDto>(
        found_test_sequence_execution,
    )
}

#[post("/test-sequence-executions")]
pub async fn add_test_sequence_execution(
    app_state: Data<AppState>,
    new_test_sequence_execution: Json<TestSequenceExecutionPostDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let added_test_sequence_execution =
        test_sequence_execution_service::save_test_sequence_execution(
            &app_state.db_conn,
            new_test_sequence_execution.into_inner(),
        )
        .await;

    post_response::<test_sequence_execution::Model, TestSequenceExecutionGetDto>(
        added_test_sequence_execution,
    )
}

#[put("/test-sequence-executions/{test_sequence_execution_id}")]
pub async fn update_test_sequence_execution(
    app_state: Data<AppState>,
    test_sequence_execution_id: Path<i32>,
    updated_test_sequence_execution: Json<TestSequenceExecutionUpdateDto>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let updated_test_sequence_execution =
        test_sequence_execution_service::update_test_sequence_execution(
            &app_state.db_conn,
            test_sequence_execution_id.into_inner(),
            updated_test_sequence_execution.into_inner(),
        )
        .await;

    put_response::<test_sequence_execution::Model, TestSequenceExecutionGetDto>(
        updated_test_sequence_execution,
    )
}

#[delete("/test-sequence-executions/{test_sequence_execution_id}")]
pub async fn delete_test_sequence_execution(
    app_state: Data<AppState>,
    test_sequence_execution_id: Path<i32>,
    _: jwt_auth_service::JwtMiddleware,
) -> HttpResponse {
    let delete_result = test_sequence_execution_service::delete_test_sequence_execution(
        &app_state.db_conn,
        test_sequence_execution_id.into_inner(),
    )
    .await;

    delete_response(delete_result)
}
