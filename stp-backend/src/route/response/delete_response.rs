use crate::service::error::delete_error::DeleteError;
use actix_web::HttpResponse;
use sea_orm::DeleteResult;

/// Returns the correct `HttpResponse` for the `Result` of a delete oparation
/// - 4xx in case of an unsuccesful deletion
/// - 2xx in case of a succesful deletion
///
/// # Examples
/// ```
///#[delete("/products/{id}")]
///pub async fn delete_product(
///    app_state: Data<AppState>,
///    product_id: Path<i32>,
///) -> HttpResponse {
///    let delete_result = product_service::delete_product(&app_state.db_conn, product_id.into_inner()).await;
///    delete_response(delete_result)
///}
/// ```
pub fn delete_response(delete_result: Result<DeleteResult, DeleteError>) -> HttpResponse {
    match delete_result {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(error) => HttpResponse::from(error),
    }
}

impl From<DeleteError> for HttpResponse {
    /// Returns the correct 4xx `HttpResponse` for `DeleteError`
    ///
    /// # Examples
    /// ```
    /// HttpResponse::from(DeleteError::NotFoundError)
    /// ```
    fn from(error: DeleteError) -> Self {
        match error {
            DeleteError::NotFoundError => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().finish(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn successful_delete_result_maps_to_no_content() {
        let delete_result = Ok(DeleteResult { rows_affected: 1 });

        assert_eq!(
            HttpResponse::NoContent().finish().status(),
            delete_response(delete_result).status()
        )
    }

    #[test]
    fn delete_error_not_found_maps_to_not_found() {
        let delete_result = Err(DeleteError::NotFoundError);
        assert_eq!(
            HttpResponse::NotFound().finish().status(),
            delete_response(delete_result).status()
        )
    }

    #[test]
    fn delete_error_db_error_maps_internal_server_error() {
        let delete_result = Err(DeleteError::DbError);
        assert_eq!(
            HttpResponse::InternalServerError().finish().status(),
            delete_response(delete_result).status()
        )
    }
}
