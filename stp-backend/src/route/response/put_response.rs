use crate::service::error::update_error::UpdateError;
use actix_web::HttpResponse;
use sea_orm::ModelTrait;
use serde::Serialize;

/// Returns the correct `HttpResponse` for the `Result` of an update oparation
/// - 4xx if unsuccesful
/// - 2xx if succesful
///
/// # Examples
/// ```
///#[put("/products/{id}")]
///pub async fn update_product(
///    app_state: Data<AppState>,
///    product_id: Path<i32>,
///    updated_product: Json<ProductUpdateDto>,
///) -> HttpResponse {
///    let updated_product = product_service::update_product(
///        &app_state.db_conn,
///        product_id.into_inner(),
///        updated_product.into_inner(),
///    )
///    .await;
///
///    put_response::<product::Model, ProductGetDto>(updated_product)
///}
/// ```
pub fn put_response<M: ModelTrait, D: From<M> + Serialize>(
    model_update: Result<M, UpdateError>,
) -> HttpResponse {
    match model_update {
        Ok(product) => HttpResponse::Ok().json(D::from(product)),
        Err(error) => HttpResponse::from(error),
    }
}

impl From<UpdateError> for HttpResponse {
    /// Returns the correct 4xx `HttpResponse` for `UpdateError`
    ///
    /// # Examples
    /// ```
    /// HttpResponse::from(UpdateError::NotFoundError)
    /// ```
    fn from(error: UpdateError) -> Self {
        match error {
            UpdateError::DbError => HttpResponse::InternalServerError().finish(),
            UpdateError::NotFound => HttpResponse::NotFound().finish(),
            UpdateError::QueryError => HttpResponse::BadRequest().finish(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dto::product_dto::ProductGetDto;
    use crate::entity::product;
    use sea_orm::prelude::TimeDateTimeWithTimeZone;

    #[test]
    fn successful_update_maps_to_ok() {
        let model_update = Ok(product::Model {
            id: 1,
            name: "Updated Title".to_owned(),
            description: Some("Updated description".to_owned()),
            image_path: Some("Updated image path".to_owned()),
            is_public: false,
            created_at: TimeDateTimeWithTimeZone::now_utc(),
        });

        assert_eq!(
            HttpResponse::Ok().finish().status(),
            put_response::<product::Model, ProductGetDto>(model_update).status(),
        )
    }

    #[test]
    fn query_error_maps_to_bad_request() {
        assert_eq!(
            HttpResponse::BadRequest().finish().status(),
            HttpResponse::from(UpdateError::QueryError).status()
        )
    }

    #[test]
    fn not_found_error_maps_to_not_found() {
        assert_eq!(
            HttpResponse::NotFound().finish().status(),
            HttpResponse::from(UpdateError::NotFound).status()
        )
    }

    #[test]
    fn db_error_maps_to_internal_server_error() {
        assert_eq!(
            HttpResponse::InternalServerError().finish().status(),
            HttpResponse::from(UpdateError::DbError).status()
        )
    }
}
