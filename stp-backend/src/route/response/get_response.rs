use crate::service::error::select_error::SelectError;
use actix_web::HttpResponse;
use sea_orm::ModelTrait;
use serde::Serialize;

/// Returns the correct `HttpResponse` for the `Result` of a select all oparation
/// - 4xx if unsuccesful
/// - 2xx if succesful
///
/// # Examples
/// ```
///#[get("/products")]
///pub async fn get_all_products(
///    app_state: Data<AppState>,
/// ) -> HttpResponse {
///    let query_result = product_service::find_all_products(&app_state.db_conn).await;
///    get_all_response::<product::Model, ProductGetDto>(query_result)
///}
/// ```
pub fn get_all_response<M: ModelTrait, D: From<M> + Serialize>(
    result: Result<Vec<M>, SelectError>,
) -> HttpResponse {
    match result {
        Ok(models) => HttpResponse::Ok().json(models.into_iter().map(D::from).collect::<Vec<_>>()),
        Err(error) => HttpResponse::from(error),
    }
}

/// Returns the correct `HttpResponse` for the `Result` of a select single oparation
/// - 4xx if unsuccesful
/// - 2xx if succesful
///
/// # Examples
/// ```
///#[get("/products/{id}")]
///pub async fn get_single_product(
///    app_state: Data<AppState>,
///    product_id: Path<i32>,
///) -> HttpResponse {
///    let query_result = product_service::find_single_product(&app_state.db_conn, product_id.into_inner()).await;
///    get_single_response::<product::Model, ProductGetDto>(query_result)
///}
/// ```
pub fn get_single_response<M: ModelTrait, D: From<M> + Serialize>(
    result: Result<M, SelectError>,
) -> HttpResponse {
    match result {
        Ok(model) => HttpResponse::Ok().json(D::from(model)),
        Err(error) => HttpResponse::from(error),
    }
}

impl From<SelectError> for HttpResponse {
    /// Returns the correct 4xx `HttpResponse` for a `SelectError`
    ///
    /// # Examples
    /// ```
    /// HttpResponse::from(SelectError::NotFoundError)
    /// ```
    fn from(error: SelectError) -> Self {
        match error {
            SelectError::DbError => HttpResponse::InternalServerError().finish(),
            SelectError::NotFoundError => HttpResponse::NotFound().finish(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dto::product_dto::ProductGetDto;
    use crate::entity::product;
    use sea_orm::prelude::TimeDateTimeWithTimeZone;

    #[test]
    fn successful_select_maps_to_ok() {
        let found_model = Ok(product::Model {
            id: 1,
            name: "Title".to_owned(),
            description: Some("Description".to_owned()),
            image_path: Some("Image path".to_owned()),
            is_public: false,
            created_at: TimeDateTimeWithTimeZone::now_utc(),
        });

        assert_eq!(
            HttpResponse::Ok().finish().status(),
            get_single_response::<product::Model, ProductGetDto>(found_model).status(),
        )
    }

    #[test]
    fn db_error_maps_to_internal_server_error() {
        assert_eq!(
            HttpResponse::InternalServerError().finish().status(),
            HttpResponse::from(SelectError::DbError).status()
        )
    }

    #[test]
    fn not_found_error_maps_to_not_found() {
        assert_eq!(
            HttpResponse::NotFound().finish().status(),
            HttpResponse::from(SelectError::NotFoundError).status()
        )
    }
}
