use crate::service::error::create_error::CreateError;
use actix_web::HttpResponse;
use sea_orm::ModelTrait;
use serde::Serialize;

/// Returns the correct `HttpResponse` for the `Result` of a create oparation
/// - 4xx if unsuccesful
/// - 2xx if succesful
///
/// # Examples
/// ```
///#[post("/products")]
///pub async fn add_product(
///    app_state: Data<AppState>,
///    new_product: Json<ProductPostDto>,
///) -> HttpResponse {
///    let query_result = product_service::save_product(&app_state.db_conn, new_product.into_inner()).await;
///    post_response::<product::Model, ProductGetDto>(query_result)
///}
/// ```
pub fn post_response<M: ModelTrait, D: From<M> + Serialize>(
    query_result: Result<M, CreateError>,
) -> HttpResponse {
    match query_result {
        Ok(model) => HttpResponse::Created().json(D::from(model)),
        Err(error) => HttpResponse::from(error),
    }
}

impl From<CreateError> for HttpResponse {
    /// Returns the correct 4xx `HttpResponse` for `CreateError`
    ///
    /// # Examples
    /// ```
    /// HttpResponse::from(CreateError::QueryError)
    /// ```
    fn from(error: CreateError) -> Self {
        match error {
            CreateError::DbError => HttpResponse::InternalServerError().finish(),
            CreateError::QueryError => HttpResponse::BadRequest().finish(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dto::product_dto::ProductGetDto;
    use crate::entity::product;
    use sea_orm::prelude::TimeDateTimeWithTimeZone;

    #[test]
    fn successful_create_maps_to_created() {
        let created_model = Ok(product::Model {
            id: 1,
            name: "Title".to_owned(),
            description: Some("Description".to_owned()),
            image_path: Some("Image path".to_owned()),
            is_public: false,
            created_at: TimeDateTimeWithTimeZone::now_utc(),
        });

        assert_eq!(
            HttpResponse::Created().finish().status(),
            post_response::<product::Model, ProductGetDto>(created_model).status(),
        )
    }

    #[test]
    fn query_error_maps_to_bad_request() {
        assert_eq!(
            HttpResponse::BadRequest().finish().status(),
            HttpResponse::from(CreateError::QueryError).status()
        )
    }

    #[test]
    fn db_error_maps_to_internal_server_error() {
        assert_eq!(
            HttpResponse::InternalServerError().finish().status(),
            HttpResponse::from(CreateError::DbError).status()
        )
    }
}
