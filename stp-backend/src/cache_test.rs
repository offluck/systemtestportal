use super::cache::AppCache;

#[test]
fn test_new() {
    let cache = AppCache::new();
    assert!(cache.get_disabled_users().is_empty())
}

#[test]
fn test_add() {
    let cache = AppCache::new();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.insert_disable_user("user2".to_string());
    assert!(cache.get_disabled_users().len() == 2);
}

#[test]
fn test_clear() {
    let cache = AppCache::new();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.clear_disabled_users_cache();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.clear_cache();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);
    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.clear_disabled_users_cache();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);
    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);

    cache.clear_cache();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);
    cache.insert_disable_user("user2".to_string());
    assert!(cache.get_disabled_users().len() == 2);

    cache.clear_disabled_users_cache();
    assert!(cache.get_disabled_users().is_empty());

    cache.insert_disable_user("user1".to_string());
    assert!(cache.get_disabled_users().len() == 1);
    cache.insert_disable_user("user2".to_string());
    assert!(cache.get_disabled_users().len() == 2);

    cache.clear_cache();
    assert!(cache.get_disabled_users().is_empty());
}
