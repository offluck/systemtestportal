 # Run the project

`rust` and `cargo` need to be installed.

Simply execute `cargo run` in the console to start the server.

# Persistence

STP uses [SeaORM](https://crates.io/crates/sea-orm) combined with [PostgreSQL](https://www.postgresql.org/) for persisting data.

## Migrations
If you intent to update the structure of your database, migrations are run when starting the STP backend.

## Entities
In order to generate Entities with SeaORM, just define your DTOs and run [sea-orm-cli](https://www.sea-ql.org/SeaORM/docs/generate-entity/sea-orm-cli/). Do not change generated code as your changes might be overwritten by the next generation.
