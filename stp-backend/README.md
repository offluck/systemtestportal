# Setup

## Sending Emails

STP's backend sends emails on various occasions. For example, the user receives their first email upon registration. Sending emails requires a SMTP server that can be configured via the `/email-configs` endpoints. Some emails might fall victim to spam filters - this is an issue that has to be resolved by the administrators of STP. Whether or not an email is filtered depends on various factors. Additional Research might be needed at this point.

## Parameters Settings

Parameters for System Test Portal can be set in the file `./stp-backend/.env` for common run and in the `./docker/envs` directory for run inside docker containers.

## Parameters

**DATABASE_URL** url for database ( i.e. `postgres://postgres:password@localhost:5432/systemtestportal_db` )

**FRONTEND_URL** frontend's url ( i.e. `http://localhost:9000` )

**HOST** IP address where backend of STP is running ( i.e. `127.0.0.1` )

**PORT** port wherer backend of STP is running ( i.e. `8080` )

**PEPPER** phrase used as a pepper when hashing is used inside STP ( i.e. `some_pepper_string`)<br/>
More information here: https://www.makeuseof.com/what-is-peppering-how-does-it-work/

**JWT_SECRET** phrase used when jwt cookie is created ( i.e. `temp_secret`)

**RUST_LOG** level of logging of Rust application ( i.e. `INFO`)<br/>
Available values are described here: https://docs.rs/log/latest/log/enum.Level.html

**TOKEN_EXPIRATION_TIME_MINUTES** the period in minutes for that password reset token is valid ( i.e. 240 for 4 hours)

## Information about password reset procedure

- The link with specific token can be used only once.
- The link is tied up with user who requested it (via user_id), so only this user can use this link to reset password. Unfortunately at this moment it does not take into account whether the user is enabled or not.
- Links are stored in cache in hashed form.
- The link can no longer be used after the expiration date.

## Secure implementation information

- For the hashing of strings inside STP is used SHA-256 algorithm.
- For sending of e-mails inside STP is used TLS connection to SMTP Server (port 587).

## Routes

### Login Route (POST)

To access the `/auth/login` endpoint, you need to provide an `email` and `password` field in the JSON body.
The provided email is used to search for a user with an enabled account.
If a user is found, the password provided in the JSON body is hashed using SHA-256 and a pepper string,
and compared to the hashed password in the user's entry.
In case they don't match we again return a "Unautherized" status code.
Also after a certain amout of tries your ip will be blocked and you can't login if your credentials are correct
But if they match, a new JWT token is created and wrapped in an HTTP-only cookie with a one-hour lifetime.
The HTTP response includes a 200 status code, the user information in the JSON body, and the HTTP-only cookie containing the JWT.

### Logout Route (GET)

Calling the `auth/logout` endpoint returns an HTTP response with a status code of 200 and an empty cookie with negative lifetime.

### Register Route (POST)

The `/register` endpoint expects a JSON body that matches the structure of `AppUserPostDto`.
An activation token is created by hashing a string representation of the Dto using SHA-256 with ten random characters as a salt.
The user is then saved in the `app_user` table, and if successful, the generated token is saved in the `app_user_activation` table.
If the email is already taken, a 400 HTTP status code and an error message are returned in the response.
Otherwise, a 200 status code and a success message are returned.

### Activate Route (POST)

The `/activate` endpoint expects the JSON body to contain only the `activationToken` field.
The endpoint checks if an entry with this token exists in the `app_user_activation` table.
If not, a 400 status code and an error message are returned.
If a matching entry is found, the user account associated with the `app_user_id` is activated, and the token entry is deleted.
Finally, a 200 status code and a success message are returned.

### Update Account Route (PUT)

The `/users` PUT endpoint now includes additional functionality.
In addition to updating the name of the user, it checks if the found password and the given password (after hashing) match.
If not, a 401 HTTP response is returned.
To update the password, the `AppUserPutDto` is extended with a `newPassword` field, which is optional.
If present, the `newPassword` is hashed and used to update the user's password. The response is handled by `transform_update_result()`.

## JWT Auth Service

The JWT Auth Service provides a JWT middleware that can be used to guard endpoints.
Simply add `_: jwt_auth_service::JwtMiddleware` to your function parameters,
and the middleware will be executed before the endpoint is called.
In the middleware, the presence and validity of the required HTTP-only cookie and token are checked.
If valid, the program continues to execute the endpoint. Otherwise, a 401 HTTP status code with an error message is returned.



